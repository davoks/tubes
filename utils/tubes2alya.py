#!/usr/bin/env python
# coding: utf-8
"""
    Converts tubes mesh files (*.domn.txt) to
    Alya's gusano domain files (*.dom.dat).

    Instructions for use:
        1) Run Tubes with nnode/elem = 2 to get *.domn.txt file
        2) python3 tubes2alya.py <input_file> <output_file>
"""

import numpy as np
import sys
import os 


# Conversion parameters 
fnamein = sys.argv[1];     # input filename
fnameout = sys.argv[2];    # output filename
f_unit_conversion = 1.0;   # factor to multiply for length unit conversion
flag_smooth_radii = False  # flag to smooth out radius discontinuities

# Alya domain parameters
mesh                                = {};
mesh['ndime']                       = 3;
mesh['nnodes_per_segment']          = 2;
mesh['bar_elem_type']               = 'BAR3D';
mesh['bar_elem_type_number']        = 52;
mesh['virtual_elem_type_number']    = -100;
mesh['bar_elem_char_number']        = 0;
mesh['virtual_elem_char_number']    = 8;
mesh['max_number_of_nodes']         = 3;
mesh['integration_rule']            = 'OPEN';
mesh['domain_integration_points']   = 0;
mesh['boundary_element']            = 'ON';
mesh['extrapolate']                 = 'ON';
mesh['boundary_type']               = 1;


def rearange_list(list_in, list_cols_per_row):
    """ Rearanges one-dimensional list to 2-dimensional list
        according to the list "list_cols_per_row" which indicates
        the number of columns per row.
    """
    
    list_out = [];
    istart = 0;
    for ncols in list_cols_per_row:
        iend = istart + ncols; 
        list_out.append( list_in[istart:iend] );
        istart = iend;
        
    return list_out


def read_tubes_domn(fname_in, mesh):
    """ Reads data from Tubes' domn file and stores in "mesh" dict. """
    
    mesh['lcps4bounds'] = []
    print('\nreading tubes file..')
    
    with open(fname_in,'r',errors='ignore') as f:
        while True:
            line = f.readline();
            if not line: break
            if line in ['\n','\r\n']: continue
            words = line.split();
            
            # General mesh parameters
            if words[0:3] == ['number','of','cp']:        mesh['ncpoints']     = int(words[4]);
            if words[0:3] == ['number','of','segments']:  mesh['nsegments']    = int(words[4]);
            if words[0:3] == ['number','of','BC']:        mesh['nboundaries']  = int(words[4]);
            if words[0]   == 'nodes/element':             mesh['nnodeperelem'] = int(words[2]);
                
            # Data per control point
            if words[0:2] == ['CP','coordinates']:        mesh['cpcoords']         = np.array(words[3:],dtype=float).reshape((-1,3))*f_unit_conversion;
            if words[0:4] == ['num.','of','in','seg.']:   mesh['ninsegs4cps']  = np.array(words[5:],dtype=int);
            if words[0:4] == ['num.','of','out','seg.']:  mesh['noutsegs4cps'] = np.array(words[5:],dtype=int);
            if words[0:3] == ['in','segments','ind.']:
                mesh['linsegs4cps'] = rearange_list( list(map(int,words[4:])), mesh['ninsegs4cps'] );
            if words[0:3] == ['out','segments','ind.']:
                mesh['loutsegs4cps'] = rearange_list( list(map(int,words[4:])), mesh['noutsegs4cps'] );
            if words[0:3] == ['nodes','at','cp']:
                mesh['nnodes4cps'] = (mesh['ninsegs4cps'] + mesh['noutsegs4cps']).astype(int)
                mesh['lnodes4cps'] = rearange_list( list(map(int,words[4:])), mesh['nnodes4cps'] );
            
            # Data per segment
            if words[0:2] == ['limiting','cp']:           mesh['lcps4segs']    = np.array(words[3:],dtype=int).reshape((-1,2));
            if words[0:3] == ['number','of','elements']:
                if len(words) == 5:                       mesh['nelem']        = int(words[4]);
                else:                                     mesh['nelem4segs']   = np.array(words[4:],dtype=int)
            if words[0:2] == ['limit','elements']:        mesh['limelems4segs']= np.array(words[3:],dtype=int).reshape((-1,2));
            if words[0:2] == ['radius','values']:         mesh['radii4segs']   = np.array(words[3:],dtype=float).reshape((-1,2))*f_unit_conversion;
            if words[0:2] == ['thickness','values']:      mesh['thick4segs']   = np.array(words[3:],dtype=float).reshape((-1,2))*f_unit_conversion;
                
            # Data by element
            if words[0:3] == ['segment','owning','el.']:  mesh['lsegs4elems']  = np.array(words[4:],dtype=int);
            if words[0:2] == ['limit','nodes']:           mesh['limnodes4elems'] = np.array(words[3:],dtype=int).reshape((-1,2));
            
            # Data by node
            if words[0:3] == ['number','of','nodes']:     mesh['nnode']        = int(words[4]);
            if words[0:2] == ['limit','nodes']:
                mesh['lnodes4elems'] = rearange_list( list(map(int,words[3:])), [mesh['nnodeperelem']]*mesh['nelem'] );
            if words[0:3] == ['segment','owning','node']: mesh['lsegs4nodes']  = np.array(words[4:],dtype=int);
            
            # Data by boundary
            if words[0:2] == ['-----','BC']:              mesh['lcps4bounds'].append(int(words[3]));
            
    if mesh['nnodeperelem'] > 2:
        print('WARNING! RE-MESH TUBES SETTING 2 NODES PER ELEMENT IN ORDER TO CONVERT TO ALYA.')
        exit()
        
    mesh['nvirtualelems']    = mesh['ncpoints'] - mesh['nboundaries']
    mesh['lelemtypes']       = mesh['nelem'] * [mesh['bar_elem_type_number']];
    mesh['lcharacteristics'] = mesh['nelem'] * [mesh['bar_elem_char_number']];

    print('completed!')

    return mesh


def get_node_coords(mesh):
    """ Computes coordinates for internal nodes. """

    print('\ncomputing nodal coordinates..')
        
    mesh['coords']          = np.zeros((mesh['nnode'],mesh['ndime']), float)
    mesh['linear_location'] = np.zeros(mesh['nnode'], float)
    iseg = -1
    for ipoin in range(mesh['nnode']):
        if ( mesh['lsegs4nodes'][ipoin]-1 != iseg ):
            ipoin_in_seg = -1
            
        iseg   = mesh['lsegs4nodes'][ipoin]-1
        ipoin_in_seg += 1
        icp0   = mesh['lcps4segs'][iseg,0]-1
        icp1   = mesh['lcps4segs'][iseg,1]-1
        coord0 = mesh['cpcoords'][icp0,:]
        coord1 = mesh['cpcoords'][icp1,:]
        dx     = coord1 - coord0
        nelem_in_seg = mesh['nelem4segs'][iseg]
        linear_location = float(ipoin_in_seg/nelem_in_seg)
        mesh['coords'][ipoin,:] = coord0 + linear_location * dx
        mesh['linear_location'][ipoin] = linear_location

    print('completed!')

    return mesh


def get_virtual_elements(mesh):
    """ Generates gusano's virtual elements. """

    print('\ncreating virtual elements..')
    
    for icp, lnodes in enumerate(mesh['lnodes4cps']):
        nnode = len(lnodes);
        
        # Is a virtual element
        if nnode > 1:
            if nnode == 2:
                lnodes = lnodes + [0]
            elif nnode > 3:
                print('Error! Alya still does not have definition for virtual elements with more than 3 nodes')
                exit()
            mesh['lelemtypes'].append(mesh['virtual_elem_type_number']);
            mesh['lnodes4elems'].append(lnodes);
            mesh['lcharacteristics'].append(mesh['virtual_elem_char_number']);
            mesh['nelem'] += 1
            
    print('completed!')

    return mesh



def smooth_radii(mesh):
    """ Smooth out dicontinuities in radius at control point junctions. """

    if flag_smooth_radii:
    
        print('\nsmoothing out radius discontinuities..')

        for icp in range(mesh['ncpoints']):
            if mesh['nnodes4cps'][icp] < 2: continue

            ## rescale radii at control points so that there is no total-area discontinuity
            # ingoing segments
            lsegs_in  = [x-1 for x in mesh['linsegs4cps'][icp]]    # list of segment indices
            r_in      = mesh['radii4segs'][lsegs_in,1]             # list of radii
            a_in      = np.pi * r_in**2                            # list of areas
            A_in      = np.sum( a_in )                             # sum of areas
            # outgoing segments
            lsegs_out = [x-1 for x in mesh['loutsegs4cps'][icp]]   # list of segment indices
            r_out     = mesh['radii4segs'][lsegs_out,0]            # list of radii
            a_out     = np.pi * r_out**2                           # list of areas
            A_out     = np.sum( a_out )                            # sum of areas
            # average total areas between in- and out-going
            A         = 0.5*(A_in + A_out)
            # scaling factors
            f_in  = A / A_in
            f_out = A / A_out
            # scale single in- and out-going radii to sum up to average total area
            r_in      = np.sqrt( f_in  * a_in  / np.pi )
            r_out     = np.sqrt( f_out * a_out / np.pi )
            # save re-scaled radii 
            mesh['radii4segs'][lsegs_in,1]  = r_in
            mesh['radii4segs'][lsegs_out,0] = r_out

        print('completed!')

    return mesh



def get_nodal_radii_and_thickness(mesh):
    """ Computes radius, thicknes and section area for each
        internal node in each network segment.
    """
    mesh['radii4nodes'] = np.zeros(mesh['nnode'], float)
    mesh['thick4nodes'] = np.zeros(mesh['nnode'], float)
    mesh['area4nodes']  = np.zeros(mesh['nnode'], float)
    for ipoin in range(mesh['nnode']):
        iseg   = mesh['lsegs4nodes'][ipoin]-1
        r0 = mesh['radii4segs'][iseg,0] 
        rf = mesh['radii4segs'][iseg,1] 
        t0 = mesh['thick4segs'][iseg,0] 
        tf = mesh['thick4segs'][iseg,1]
        alpha = mesh['linear_location'][ipoin]
        r = r0 + alpha * (rf-r0)
        t = t0 + alpha * (tf-t0)
        a = np.pi*r**2
        mesh['radii4nodes'][ipoin] = r
        mesh['thick4nodes'][ipoin] = t
        mesh['area4nodes'][ipoin] = a
    return mesh
    
            

def write_alya_dom(fname_out, mesh):
    """ Writes Alya dom.dat file using gusano's format. """

    print('\nwriting alya''s dom.dat file..')

    with open(fname_out,'w') as f:
        
        f.write('$------------------------------------------------------------\n')
        f.write('DIMENSIONS\n')
        f.write('  NODAL_POINTS=              '+'%d' % mesh['nnode']+'\n')
        f.write('  ELEMENTS=                  '+'%d' % mesh['nelem']+'\n')
        f.write('  SPACE_DIMENSIONS=          '+'%d' % mesh['ndime']+'\n')
        f.write('  TYPES_OF_ELEMENTS=         '+       mesh['bar_elem_type']+'\n')
        f.write('  BOUNDARIES=                '+'%d' % mesh['nboundaries']+'\n')
        f.write('  MAXIMUM_NUMBER_NODES=      '+'%d' % mesh['max_number_of_nodes']+'\n')
        f.write('END_DIMENSIONS\n')
        f.write('$------------------------------------------------------------\n')
        f.write('STRATEGY\n')
        f.write('  INTEGRATION_RULE=          '+       mesh['integration_rule']+'\n')
        f.write('  DOMAIN_INTEGRATION_POINTS= '+'%d' % mesh['domain_integration_points']+'\n')
        f.write('  BOUNDARY_ELEMENT=          '+       mesh['boundary_element']+'\n')
        f.write('  EXTRAPOLATE=               '+       mesh['extrapolate']+'\n')
        f.write('END_STRATEGY\n')
        f.write('$------------------------------------------------------------\n')
        f.write('GEOMETRY\n')
        
        f.write('  TYPES\n')
        for ielem in range(mesh['nelem']):
            f.write('    %d %d\n' % (ielem+1, mesh['lelemtypes'][ielem]))
        f.write('  END_TYPES\n')
        
        f.write('  COORDINATES\n')
        for ipoin in range(mesh['nnode']):
            f.write('    %d' % int(ipoin+1))
            for idime in range(mesh['ndime']):
                f.write(' %f' % mesh['coords'][ipoin,idime])
            f.write('\n')
        f.write('  END_COORDINATES\n')
        
        f.write('  CHARACTERISTIC\n')
        for ielem in range(mesh['nelem']):
            f.write('    %d %d\n' % (ielem+1, mesh['lcharacteristics'][ielem]))
        f.write('  END_CHARACTERISTIC\n')
        
        f.write('  ELEMENTS\n')
        for ielem in range(mesh['nelem']):
            f.write('    %d' % int(ielem+1))
            for inode in mesh['lnodes4elems'][ielem]:
                f.write(' %d' % inode)
            f.write('\n')
        f.write('  END_ELEMENTS\n')
        
        f.write('  TYPES, BOUNDARIES\n')
        for iboun in range(mesh['nboundaries']):
            f.write('    %d %d\n' % (iboun+1, mesh['boundary_type']))
        f.write('  END_TYPES\n')
        
        f.write('  BOUNDARIES\n')
        for iboun in range(mesh['nboundaries']):
            icp = mesh['lcps4bounds'][iboun]
            inode = mesh['lnodes4cps'][icp-1][0]
            f.write('    %d %d\n' % (iboun+1, inode))
        f.write('  END_BOUNDARIES\n')
        
        f.write('  LELBO\n')
        for iboun in range(mesh['nboundaries']):
            icp = mesh['lcps4bounds'][iboun]
            if mesh['ninsegs4cps'][icp-1] == 0:
                iside = 0
            elif mesh['noutsegs4cps'][icp-1] == 0:
                iside = 1
            inode = mesh['lnodes4cps'][icp-1][0]
            ielem = np.where(mesh['limnodes4elems'][:,iside] == inode)[0][0]
            f.write('    %d %d\n' % (iboun+1, ielem+1))
        f.write('  END_LELBO\n')
        
        f.write('END_GEOMETRY\n')
        f.write('$------------------------------------------------------------\n')
        f.write('SETS\n')
        f.write('END_SETS\n')        
        f.write('$------------------------------------------------------------\n')
        f.write('BOUNDARY_CONDITIONS\n')
        for iboun in range(mesh['nboundaries']):
            f.write('  %d'%(iboun+1) + ' FILL_IN_BC_TYPE \n')
        f.write('END_BOUNDARY_CONDITIONS\n')
        f.write('$------------------------------------------------------------\n')
        
        print('completed!\n')



def write_radii_and_thick(fname_out, mesh):
    """ Writes Alya dom.dat file using gusano's format. """

    casename = os.path.splitext(os.path.splitext(fname_out)[0])[0]
    fname_radii = casename+'.radii.dat'
    fname_thick = casename+'.thick.dat'
    fname_area  = casename+'.area.dat'

    print('writing radius nodal values...')
    with open(fname_radii,'w') as f:
        for ipoin in range(mesh['nnode']):
            f.write('%d %f\n' % (int(ipoin+1),mesh['radii4nodes'][ipoin]))
        print('completed!\n')

    print('writing thickness nodal values...')
    with open(fname_thick,'w') as f:
        for ipoin in range(mesh['nnode']):
            f.write('%d %f\n' % (int(ipoin+1),mesh['thick4nodes'][ipoin]))
        print('completed!\n')

    print('writing area nodal values...')
    with open(fname_area,'w') as f:
        for ipoin in range(mesh['nnode']):
            f.write('%d %f\n' % (int(ipoin+1),mesh['area4nodes'][ipoin]))
        print('completed!\n')



def check_connectivities(mesh):
    """ Do some checks for connectivities in mesh. """

    print('\nperforming checks on mesh..')

    # Check if all nodes are connected to at least 1 element
    l = np.zeros(mesh['nnode'],int)
    for ielem in range(mesh['nelem']):
        for inode in mesh['lnodes4elems'][ielem]:
            l[inode-1] += 1
    if np.any(l < 1):
        print('Error! Loose node.')
        exit()
    else:
        print('Ok! No loose nodes.')

    # Check if all nodes connected to a single element correspond
    # to a boundary
    l2 = []
    for iboun in range(mesh['nboundaries']):
        icp = mesh['lcps4bounds'][iboun]
        inode = mesh['lnodes4cps'][icp-1][0]
        l2.append(inode)
    for inode in range(mesh['nnode']):
        if l[inode] == 1:
            if not ((inode+1) in l2):
                print('Error! Edge node not defined in boundaries')
    print('Ok! All edge nodes defined in boundaries')

    print('completed!')
        


if __name__ == "__main__":
    """ Executes program """

    print(' ')
    print('|-------------------------------------------------------|')
    print('|   Converting domain file from Tubes to Alya format:   |')
    print('|-------------------------------------------------------|')

    mesh = read_tubes_domn(fnamein, mesh);
    mesh = get_node_coords(mesh);
    mesh = get_virtual_elements(mesh);
    mesh = smooth_radii(mesh);
    mesh = get_nodal_radii_and_thickness(mesh);
    check_connectivities(mesh);
    write_alya_dom(fnameout, mesh);
    write_radii_and_thick(fnameout, mesh);

    print('--->  Completed Conversion! :)\n')
