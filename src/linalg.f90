
!> Module containing linear algebra subroutines.
!> * ludcmp and lubksb are from the LU decomposition routines by J.P. Moreau.
!> * cholesky_d is the Cholesky decomposition routine by Takeshi Nishimatsu.

module linalg

  use nrtype

  implicit none

contains

          !*******************************************************
          !*    lu decomposition routines used by test_lu.f90    *
          !*                                                     *
          !*                 f90 version by j-p moreau, paris    *
          !* --------------------------------------------------- *
          !* reference:                                          *
          !*                                                     *
          !* "numerical recipes by w.h. press, b. p. flannery,   *
          !*  s.a. teukolsky and w.t. vetterling, cambridge      *
          !*  university press, 1986" [bibli 08].                *
          !*                                                     *
          !*******************************************************

  !  ***************************************************************
  !  * given an n x n matrix a, this routine replaces it by the lu *
  !  * decomposition of a rowwise permutation of itself. a and n   *
  !  * are input. indx is an output vector which records the row   *
  !  * permutation effected by the partial pivoting; d is output   *
  !  * as -1 or 1, depending on whether the number of row inter-   *
  !  * changes was even or odd, respectively. this routine is used *
  !  * in combination with lubksb to solve linear equations or to  *
  !  * invert a matrix. return code is 1, if matrix is singular.   *
  !  ***************************************************************
  subroutine ludcmp(a,n,indx,d,code)
    integer(ip), parameter                     :: nmax = 100_ip
    real(rp),    parameter                     :: tiny = 1.5e-16_rp
    integer(ip),                 intent(in)    :: n
    real(rp),    dimension(n,n), intent(inout) :: a
    integer(ip),                 intent(out)   :: d, code
    integer(ip), dimension(n),   intent(out)   :: indx
    !f2py depend(n) a, indx
    real(rp)    :: amax, dum, summ, vv(nmax)
    integer(ip) :: i, j, k, imax

     d=1; code=0

     do i=1,n
       amax=0.0_rp
       do j=1,n
         if (abs(a(i,j)).gt.amax) amax=abs(a(i,j))
       end do ! j loop
       if(amax.lt.tiny) then
         code = 1
         return
       end if
       vv(i) = 1.0_rp / amax
     end do ! i loop

     do j=1,n
       do i=1,j-1
         summ = a(i,j)
         do k=1,i-1
           summ = summ - a(i,k)*a(k,j)
         end do ! k loop
         a(i,j) = summ
       end do ! i loop
       amax = 0.0_rp
       do i=j,n
         summ = a(i,j)
         do k=1,j-1
           summ = summ - a(i,k)*a(k,j)
         end do ! k loop
         a(i,j) = summ
         dum = vv(i)*abs(summ)
         if(dum.ge.amax) then
           imax = i
           amax = dum
         end if
       end do ! i loop

       if(j.ne.imax) then
         do k=1,n
           dum = a(imax,k)
           a(imax,k) = a(j,k)
           a(j,k) = dum
         end do ! k loop
         d = -d
         vv(imax) = vv(j)
       end if

       indx(j) = imax
       if(abs(a(j,j)) < tiny) a(j,j) = tiny

       if(j.ne.n) then
         dum = 1.0_rp / a(j,j)
         do i=j+1,n
           a(i,j) = a(i,j)*dum
         end do ! i loop
       end if
     end do ! j loop

     return
   end subroutine ludcmp


  !  ******************************************************************
  !  * solves the set of n linear equations a . x = b.  here a is     *
  !  * input, not as the matrix a but rather as its lu decomposition, *
  !  * determined by the routine ludcmp. indx is input as the permuta-*
  !  * tion vector returned by ludcmp. b is input as the right-hand   *
  !  * side vector b, and returns with the solution vector x. a, n and*
  !  * indx are not modified by this routine and can be used for suc- *
  !  * cessive calls with different right-hand sides. this routine is *
  !  * also efficient for plain matrix inversion.                     *
  !  ******************************************************************
   subroutine lubksb(a, n, indx, b)
     integer(ip), intent(in) :: n
     real(rp),    intent(in), dimension(n,n)  :: a
     integer(ip), intent(in), dimension(n)    :: indx
     real(rp),    intent(inout), dimension(n) :: b
     integer(ip)                              :: i, j, ii, ll
     !f2py depend(n) a, indx, b

     real(rp)  summ

     ii = 0

     do i=1,n
       ll = indx(i)
       summ = b(ll)
       b(ll) = b(i)
       if(ii.ne.0) then
         do j=ii,i-1
           summ = summ - a(i,j)*b(j)
         end do ! j loop
       else if(summ.ne.0.0_rp) then
         ii = i
       end if
       b(i) = summ
     end do ! i loop

     do i=n,1,-1
       summ = b(i)
       if(i < n) then
         do j=i+1,n
           summ = summ - a(i,j)*b(j)
         end do ! j loop
       end if
       b(i) = summ / a(i,i)
     end do ! i loop

     return
   end subroutine lubksb


   ! cholesky_d.f -*-f90-*-
   ! using cholesky decomposition, cholesky_d.f solve a linear equation ax=b,
   ! where a is a n by n positive definite real symmetric matrix, x and b are
   ! real(rp) vectors length n.
   !
   ! time-stamp: <2015-06-25 18:05:47 takeshi>
   ! author: takeshi nishimatsu
   ! licence: gplv3
   !
   ! [1] a = g tg, where g is a lower triangular matrix and tg is transpose of g.
   ! [2] solve  gy=b with  forward elimination
   ! [3] solve tgx=y with backward elimination
   !
   ! reference: taketomo mitsui: solvers for linear equations [in japanese]
   !            http://www2.math.human.nagoya-u.ac.jp/~mitsui/syllabi/sis/info_math4_chap2.pdf
   !
   ! comment:   this cholesky decomposition is used in src/elastic.f and
   !            src/optimize-inho-strain.f of feram http://loto.sourceforge.net/feram/ .
   !!
   subroutine cholesky_d(n, a, g, b)
     integer(ip), intent(in)    :: n
     real(rp),    intent(in)    :: a(n,n)
     real(rp),    intent(out)   :: g(n,n)
     real(rp),    intent(inout) :: b(n)
     real(rp)                   :: tmp
     integer(ip)                :: i,j

     ! light check of positive definite
     do i = 1, n
          if (a(i,i).le.0.0_rp) then
               b(:) = 0.0_rp
               return
          end if
     end do

     ! [1]
     g(:,:)=0.0_rp
     do j = 1, n
          g(j,j) = sqrt( a(j,j) - dot_product(g(j,1:j-1),g(j,1:j-1)) )
          do i = j+1, n
               g(i,j)  = ( a(i,j) - dot_product(g(i,1:j-1),g(j,1:j-1)) ) / g(j,j)
          end do
     end do
     ! write(6,'(3f10.5)') (g(i,:), i=1,n)

     ! [2]
     do i = 1, n
          tmp = 0.0_rp
          do j = 1, i-1
               tmp = tmp + g(i,j)*b(j)
          end do
          b(i) = (b(i)-tmp)/g(i,i)
     end do

     ! [3]
     do i = n, 1, -1
          tmp = 0.0_rp
          do j = i+1, n
               tmp = tmp + b(j)*g(j,i)
          end do
          b(i) = (b(i)-tmp)/g(i,i)
     end do

  end subroutine cholesky_d

end module linalg
