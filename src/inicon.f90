module inicon

    use nrtype
    use clases
    use femodule
    use physfuncs

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Set initial conditions on variables to solve u (A, Q), p and list of scalar variables
!   Set CP related information for incoming / outgoing segments and nodes on them
subroutine setIniConds()

    integer(ip)                                 :: i
    logical                                     :: dirE

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Memory allocation of variables to solve ---"

    ! allocate array of dyastolic area, beta and area0 at each node
    allocate( nodeList%dyArea   ( nodeList%num ) )
    allocate( nodeList%dyA0     ( nodeList%num ) )          ! Initial dyastolic area backup
    allocate( nodeList%beta     ( nodeList%num ) )
    allocate( nodeList%area0    ( nodeList%num ) )
    ! allocate array of spatial derivatives of dyastolic area and beta
    allocate( nodeList%dA_dx    ( nodeList%num ) )
    allocate( nodeList%dB_dx    ( nodeList%num ) )
    ! allocate memory for dof values and their variation at nodes
    allocate( nodeList%u        ( gVar%dof, nodeList%num) )
    allocate( nodeList%du       ( gVar%dof, nodeList%num) )
    ! allocate memory for pressure values at nodes
    allocate( nodeList%p        ( nodeList%num ) )
    ! allocate values of unknowns at control points
    allocate( cpList%uBV        ( cpList%num ) )
    allocate( cpList%wBV        ( cpList%num ) )
    allocate( cpList%antWBV     ( cpList%num ) )
    do i = 1, cpList%num
        allocate( cpList%uBV(i)%r   ( gVar%dof * size(cpList%nodeL(i)%i) ) )
        allocate( cpList%wBV(i)%r   ( size(cpList%nodeL(i)%i) ) )
        allocate( cpList%antWBV(i)%r( 2 * size(cpList%nodeL(i)%i) ) )
    end do
    ! allocate scalar memory related values if needed
    if (gVar%nadv .ne. 0 ) then
        ! scalar values and their variation at nodes
        allocate( nodeList%scal     ( gVar%nadv, nodeList%num) )
        allocate( nodeList%dSc_dx   ( gVar%nadv, nodeList%num) )
        allocate( nodeList%dscal    ( gVar%nadv, nodeList%num) )
        ! scalar values and scalar flux at control points
        allocate( cpList%scBV       ( gVar%nadv, cpList%num ) )
        allocate( cpList%scFlux     ( gVar%nadv, cpList%num ) )
        ! deposited scalar during transport
        allocate( nodeList%depos    ( gVar%nadv, nodeList%num) )
        allocate( nodeList%depFact  ( gVar%nadv, nodeList%num) )
    end if

    write(fIdx%logfile, *) "---> DONE"

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Setting segment initial values ---"

    ! Initialize conditions on each segment
    do i = 1, segList%num
        call initSegment(i)
    end do
    write(fIdx%logfile, *) "---> DONE"

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Verifying/creating output directory ---"

    write(fIdx%logfile, *) "Creating output directory"
    close(fIdx%logfile)
    call execute_command_line('mkdir ' // trim(gVar%outPath)) ! create directory
    open(fIdx%logfile, file=logFileName, status="old", position="append", action="write")
    write(fIdx%logfile, *) "---> DONE"

    write(fIdx%logfile, *) "--- Setting boundary condition type at CP ---"

    ! Initialize cp boundary condition types as 0 (No external bc, internal nodes) by default
    cpList%bcNum(:) = 0
    ! Modify cp boundary conditions following boundary condition table
    do i = 1, bcList%num
        cpList%bcNum(bcList%cp(i)) = i
    end do

    ! Initialize characteristic values from physical ones on the boundaries
    do i = 1, cpList%num
        cpList%antWBV(i)%r(:) = getWBV(i, cpList%inN(i) + cpList%outN(i))
    end do

    write(fIdx%logfile, *) "---> DONE"

    gVar%dumpN = 0                          ! Initialize output dump files counter

contains
! function to transform from physical to characteristic values in each node of a
!   control point
function getWBV(cp, ndNum) result(w)
    integer(ip),      intent(in)                      :: cp, ndNum
    real(rp),         dimension(2*ndNum)              :: w
    real(rp),         dimension(1)                    :: B, aD, a0
    integer(ip)                                       :: i, nd
    do i = 1, ndNum
        nd = cpList%nodeL(cp)%i(i)
        B(:) = nodeList%beta(nd)
        aD(:) = nodeList%dyArea(nd)
        a0(:) = nodeList%area0(nd)
        w(2*i-1:2*i) = [ phys2Char(nodeList%u(:, nd:nd), B(1:1), aD(1:1), 1) , &
                     &   phys2Char(nodeList%u(:, nd:nd), B(1:1), aD(1:1), 2) ]
    end do
end function getWBV

end subroutine setIniConds
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Initialize nodes in segment [iseg] with values of the unknowns and advected scalars as
!   defined in geom and conf files
subroutine initSegment(iseg)

    integer(ip),  intent(in)                      :: iseg
    integer(ip)                                   :: n1, n2, nN, ic, isc
    real(rp)                                      :: Pd

    n1 = elemList%nodLimit(1, segList%elemLimit(1, iseg))
    n2 = elemList%nodLimit(2, segList%elemLimit(2, iseg))
    nN = n2 - n1 + 1

    ! Look for initial condition specified for segment on geometry file (unknowns and scalars)
    ic = segList%icType(iseg)
    isc = segList%scICType(iseg)

    ! get initial conditions for advected scalars in segment
    if (gVar%nadv .ne. 0) then
        if (isc .eq. 0) then
            nodeList%scal(:, n1:n2) = 0.0_rp
        else
            nodeList%scal(:, n1:n2) = initialSCV(nN, isc)
        end if
        nodeList%depos(:,n1:n2) = 0.0_rp
        nodeList%dscal(:,n1:n2) = 0.0_rp
    end if

    write(fIdx%logfile, *) "Setting Dyast. Area, Beta and Area0 on segment: ", iseg

    ! Interpolate dyastolic area value at nodes from radii defined at geometry file
    ! Calculate time constant spatial derivatives of dyastolic area
    nodeList%dyArea(n1:n2) = interpolateArea(iseg, nN)
    nodeList%dyA0 = nodeList%dyArea                 ! create a backup of initial dyast
    call deriveDyArea(iseg)

    segList%ModDyAr(iseg) = (icList%ipList(ic)%i(3) .eq. 1)
    write(fIdx%logfile, *) "Pulsating dyastolic area flag: ", segList%ModDyAr(iseg)

    ! Interpolate Beta and calculate time constant spatial beta derivatives
    !   Interpolate thickness value at nodes from values defined at geometry file
    !   take physical material parameters from config file
    nodeList%beta(n1:n2) = interpolateBeta(iseg, nN)
    call deriveBeta(iseg)

    ! Get Area0 values from values of dyArea, beta and dyastolic pressure (in config file materials)
    ! a0 = ad * (1 - ad^0.5 * Pd / beta)^2
    Pd = WallMatList(segList%mat(iseg))%rParam(3)
    nodeList%area0(n1:n2) = nodeList%dyArea(n1:n2) * (1.0_rp - nodeList%dyArea(n1:n2)**0.5_rp * &
        & Pd / nodeList%beta(n1:n2))**2.0_rp

    write(fIdx%logfile, *) "Setting initial area (A) and flow (Q) on segment: ", iseg

    ! Initialize values of A and Q first to nominal values and then update as
    !   specified in initialization conditions
    nodeList%u(1, n1:n2) = nodeList%area0(n1:n2)
    nodeList%u(2, n1:n2) = 0.0_rp
    nodeList%u(:, n1:n2) = nodeList%u(:, n1:n2) + initialV(nN, ic)
    nodeList%du(:,n1:n2) = 0.0_rp

    write(fIdx%logfile, *) "Setting initial pressure (P) on segment: ", iseg

    call pressureSegment(iseg)

end subroutine initSegment
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get dyastolic area for the [nN] points in the segment [iseg] calculated by linear interpolation
!   of the radii specified in the geometry file
function interpolateArea(iseg, nN) result(area)

    integer(ip),  intent(in)                      :: iseg, nN
    integer(ip)                                   :: i, j, nI
    real(rp), dimension(nN)                       :: area
    real(rp)                                      :: xN, x1, x2, r1, r2
    logical                                       :: assigned

    nI = segList%nRad(iseg)                 ! number of radii used for interpolation

    j = 1                                   ! get the first interpolation radius value
    do i = 1, nN                            ! fill the segment nodes
        xN = real(i - 1) / real(nN - 1)     ! get fraction of segment for current point
        assigned = .false.
        do while (.not. assigned)
            ! look for the pair of radii surrounding xN starting at j
            x1 = real(j - 1) / real(nI - 1)
            x2 = real(j) / real(nI - 1)
            r1 = segList%rList(iseg)%r(j)               ! radius at j
            if ((j-1)*(nN-1) .eq. (i-1)*(nI-1)) then
                ! i is a node that corresponds exactly to j location
                area(i) = pi * r1**2.0_rp
                assigned = .true.
            else if ((x1 .lt. xN) .and. (x2 .gt. xN)) then
                ! i is a node between j and j+1
                r2 = segList%rList(iseg)%r(j+1)
                area(i) = pi * ( ((r1*(x2 - xN)) + r2*(xN - x1))/(x2 - x1) )**2.0_rp
                assigned = .true.
            else
                ! Advance radii pair in case i is after j+1
                j = j + 1
            end if
        end do
    end do

end function interpolateArea
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get beta for the [nN] points in the segment [iseg] calculated by linear interpolation
!   of the radii specified in the geometry file
function interpolateBeta(iseg, nN) result(beta)

    integer(ip),  intent(in)                      :: iseg, nN
    integer(ip)                                   :: i, j, nI
    real(rp), dimension(nN)                       :: beta
    real(rp)                                      :: young, poiss2
    real(rp)                                      :: xN, x1, x2, t1, t2, t
    logical                                       :: assigned

    nI = segList%nTh(iseg)                  ! number of radii used for interpolation

    young = WallMatList(segList%mat(iseg))%rParam(1)
    poiss2 = WallMatList(segList%mat(iseg))%rParam(2)**2.0_rp

    j = 1                                   ! get the first interpolation thickness value
    do i = 1, nN                            ! run over the segment nodes
        xN = real(i - 1) / real(nN - 1)     ! get fraction of segment for current point
        assigned = .false.
        do while (.not. assigned)
            ! look for the pair of radii surrounding xN starting at j
            x1 = real(j - 1) / real(nI - 1)
            x2 = real(j) / real(nI - 1)
            t1 = segList%tList(iseg)%r(j)               ! thickness at j
            if ((j-1)*(nN-1) .eq. (i-1)*(nI-1)) then
                ! i is a node that corresponds exactly to j location
                beta(i) = young * t1 * pi**0.5_rp / (1.0_rp - poiss2)
                assigned = .true.
            else if ((x1 .lt. xN) .and. (x2 .gt. xN)) then
                ! i is a node between j and j+1
                t2 = segList%tList(iseg)%r(j+1)
                t = ((t1*(x2 - xN)) + t2*(xN - x1))/(x2 - x1)
                beta(i) = young * t * pi**0.5_rp / (1.0_rp - poiss2)
                assigned = .true.
            else
                ! Advance radii pair in case i is after j+1
                j = j + 1
            end if
        end do
    end do

end function interpolateBeta
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Return array of [nN] points with values according to the specification in the config file
!   for the initial condition [ic]
function initialV(nN, ic) result(values)

    integer(ip),  intent(in)                      :: nN, ic
    integer(ip)                                   :: fType, nu, j
    real(rp),     dimension(nN)                   :: x
    real(rp),     dimension(2, nN)                :: values
    real(rp)                                      :: amp, mu, sig

    ! Initialize each unknown variable
    do nu = 1, 2
        ! Look for type of initial condition function as listed on the config file [A, Q]
        fType = icList%ipList(ic)%i(nu)
        select case(fType)
        case(0)             ! Offset initial condition
            values(nu,:) = icList%rpList(ic)%r(nu)
        case(1)             ! Gaussian distribution initial condition
            amp = icList%rpList(ic)%r(nu)
            mu  = icList%rpList(ic)%r(2+nu)
            sig = icList%rpList(ic)%r(4+nu)
            x   = [ ( real(j-1) / real(nN-1) , j = 1, nN) ]
            values(nu,:)  = gaussF(x, mu, sig, amp, 0.0_rp)
        case default        ! Raise error if option is not recognized
            write(fIdx%logfile,*)
            write(fIdx%logfile,*) "ERROR --- Not recognized unknown initialization function: ", fType
            stop
        end select
    end do

end function initialV
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Return array of [nN] points with scalar values according to the specification in the config file
!   for the initial condition [isc]
function initialSCV(nN, isc) result(values)

    integer(ip),  intent(in)                      :: nN, isc
    integer(ip)                                   :: npar, fType, s, j
    real(rp),     dimension(nN)                   :: x
    real(rp),     dimension(gVar%nadv, nN)        :: values
    real(rp)                                      :: amp, mu, sig
    real(rp)                                      :: off, nWave

    npar = 1        ! position counter for the function parameters for each advected variable
    do s = 1, gVar%nadv
        ! get function type from the integer index at initial condition [isc]
        ! There should be as many as the number of advected variables
        fType = scICList%ipList(isc)%i(s)
        select case(fType)
        case(0)             ! Constant value (1 real parameter read)
            values(s,:) = scICList%rpList(isc)%r(npar)
            npar = npar + 1
        case(1)             ! Gaussian distribution (3 real parameters read)
            amp = scICList%rpList(isc)%r(npar)
            mu  = scICList%rpList(isc)%r(npar+1)
            sig = scICList%rpList(isc)%r(npar+2)
            x   = [ ( real(j-1) / real(nN-1) , j = 1, nN) ]
            values(s,:) = gaussF(x, mu, sig, amp, 0.0_rp)
            npar = npar + 3
        case(2)             ! Senoidal distribution (for scalar transport verification)
            ! sc(x,0) = off + amp * cos( 2 * pi / L * x  + pi )
            !   L is the spatial period distance
            off = scICList%rpList(isc)%r(npar)
            amp = scICList%rpList(isc)%r(npar+1)
            nWave = scICList%rpList(isc)%r(npar+2)                  ! NWave = Ltotal / Lperiod
            x   = [ ( real(j-1,8) / real(nN-1,8) , j = 1, nN) ]
            values(s,:) = off - amp * cos(nWave * 2.0_rp * pi * x)
            npar = npar + 3
        case default        ! Raise error if option is not recognized
            write(fIdx%logfile,*)
            write(fIdx%logfile,*) "ERROR --- Not recognized scalar initialization function: ", fType
            stop
        end select
    end do

end function initialSCV
!----------------------------------------------------------------------------------------------

end module inicon
