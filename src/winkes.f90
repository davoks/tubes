
!> Run Windkessel 0-D models used for boundary conditions of
!! tube terminals.

module winkes

  use nrtype
  use clases
  use physfuncs

  implicit none

contains

!----------------------------------------------------------------------------------------------
!   System to solve:
!       Pc = Pi - R1 Qi
!       Qo = Qi - C dPi/dt
!       Pi - R1 Qi - R2 Qo - L dQo/dt - Po = 0
!
!   For a time step Dt of the inner loop, the values of Pi and Qi at times Dt and Dt/2 are
!   used to build an iterative scheme that solves the value of the Area at those times. The value
!   of the outgoing interpolated characteristic is also interpolated at each subtime.
!       Pi(A0, 0), Qi(A0, W0, 0) ->
!       -> Pi(A2, Dt), Pi(A1, Dt/2), Qi(A2, W2, Dt), Qi(A1, W1, Dt/2) ->
!       -> A0 = A2 (reloop)
!
!   Function to converge: (Newton - Rapson)
!   F(A1, A2, W1, W2) = CP2 Pi(A2) + CQ2 Qi(A2, W2) + CP1 Pi(A1) + CQ1 Qi(A1, W1) + CP0 Pi(A0) + CQ0 Qi(A0, W0) + Pout = 0
!   CP2 = 1 + C R2 / Dt + 4 L C / Dt^2
!   CQ2 = - (R1 + R2) - (C R1 R2 + L) / Dt - 4 L C R1 / Dt^2
!   CP1 = - 8 L C / Dt^2
!   CQ1 = 8 L C R1 / Dt^2
!   CP0 = - C R2 / Dt + 4 L C / Dt^2
!   CQ0 = (C R1 R2 + L) / Dt - 4 L C R1 / Dt^2
!
function windkessAL(param, nd, inSeg, WoutF) result(Win)

    real(rp),         dimension(:)                    :: param
    integer(ip)                                       :: nd
    real(rp)                                          :: WoutF, Win
    logical                                           :: inSeg
    integer(ip)                                       :: tdiv, it, totcount
    real(rp)                                          :: Dt, sgn, B, c0, Pdy
    real(rp),         dimension(1)                    :: Wout0
    real(rp)                                          :: DW, W0, W1, W2
    real(rp)                                          :: R1, R2, C, L, Pout, CP0, CP1, CP2, CQ0, CQ1, CQ2
    real(rp)                                          :: A0, A1, A2, Q0, Q2, P0, P2
    real(rp)                                          :: Palpha, Fnew, Amod, dFMod, Aerr, Ferr, Err
    real(rp),         dimension(2)                    :: gradF

    ! Tubes time step can be divided when calculating the N-R convergence if needed
    tdiv = 1
    Dt = gVar%dt / real(tdiv,8)
    select case (inSeg)         ! inSeg == True -> Segment enters the CP (w1 into it and w2 out of it)
    case (.true.)
        sgn = 1.0_rp
        ! The characteristic going out of the segment into the CP is the 1
        Wout0 = phys2char(nodeList%u(:,nd:nd), nodeList%beta(nd:nd), nodeList%dyArea(nd:nd), 1)
    case (.false.)
        sgn = -1.0_rp
        ! in that case is the 2
        Wout0 = phys2char(nodeList%u(:,nd:nd), nodeList%beta(nd:nd), nodeList%dyArea(nd:nd), 2)
    end select
    W0 = Wout0(1)
    ! Interpolation of outgoing characteristic (each DW is the variation on a Dt)
    DW = (WoutF - W0) / real(tdiv,8)
    ! Dyastolic pressure value
    Pdy = WallMatList( segList%mat( nodeList%seg(nd) ) )%rparam(3)

    ! speed propagation for characteristics
    ! cw(A) = B * A^{1/4}
    B = (0.5_rp * nodeList%beta(nd) / (FlPar%rho * nodeList%dyArea(nd)))**0.5_rp
    ! c0 = cw(Ad)
    c0 = B * nodeList%dyArea(nd)**0.25_rp

    ! Parameters of dumped model
    R1   = param(1)
    R2   = param(2)
    C    = param(3)
    L    = param(4)
    Pout = param(5)

    ! Translation to target function coefficients
    CP2 = ( 1.0_rp + C * R2 / Dt ) + ( 4.0_rp * L * C / Dt**2.0_rp )
    CQ2 = - (R1 + R2) - (C * R1 * R2 + L) / Dt - (4.0_rp * L * C * R1) / Dt**2.0_rp
    CP1 = - 8.0_rp * L * C / Dt**2.0_rp
    CQ1 = 8.0_rp * L * C * R1 / Dt**2.0_rp
    CP0 = - C * R2 / Dt + 4.0_rp * L * C / Dt**2.0_rp
    CQ0 = (C * R1 * R2 + L) / Dt - 4.0_rp * L *C * R1 / Dt**2.0_rp

    ! Flux related variables on the input of the Windkessel.
    ! Initialization at current tubes time values.
    Q2 = nodeList%u(2,nd)
    A2  = nodeList%u(1,nd)
    P2 = P(A2)

    ! Evolve till next tubes time value
    Err = 1.0e-3                         ! Convergence tolerance
    do it = 1, tdiv
        ! Sub-time loop initialization
        Q0 = Q2 ; P0 = P2
        A0 = A2 ; A1 = A2

        ! Parameter dependent on initial values
        Palpha = CP0 * P0 + CQ0 * Q0 - Pout

        ! Characteristic values
        W1 = W0 + 0.5_rp * DW
        W2 = W0 + DW

        ! Convergence check parameters
        Ferr = 1.0_rp
        Aerr = 1.0_rp
        totcount = 0
        ! N-R loop to find values of A1 (at Dt/2) and A2 (Dt)
        do while( (Aerr .gt. Err) .or. (Ferr .gt. Err) )
            Fnew = F(A1, A2, W1, W2)
            gradF = derF_A(A1, A2, W1, W2)
            dFMod = sqrt(dot_product(gradF, gradF))
            Amod = Fnew / dFMod
            Aerr = abs( Amod / A0 )
            A1 = A1 - Amod * gradF(1) / dFMod
            A2 = A2 - Amod * gradF(2) / dFMod
            Ferr = abs( Fnew )
            totcount = totcount + 1
            if (totcount .gt. 100) stop
        end do
        Q2 = Q(A2, W2)
        P2 = P(A2)
    end do

    Win = Q2 / A2 - sgn * 4.0_rp * (B * A2**0.25_rp - c0)

contains
function Q(A, W) result(qaw)
    real(rp),     intent(in)                          :: A, W
    real(rp)                                          :: qaw
    qaw = A * (W - sgn * 4.0_rp * (B * A**0.25_rp - c0))
end function Q
function P(A) result(pa)
    real(rp),     intent(in)                          :: A
    real(rp)                                          :: pa
    pa = FlPar%Pext + Pdy + nodeList%beta(nd) / nodeList%dyArea(nd) * &
       & (A**0.5_rp - nodeList%dyArea(nd)**0.5_rp)
end function P
function F(Af1, Af2, Wf1, Wf2) result(faw)
    real(rp),     intent(in)                          :: Af1, Af2, Wf1, Wf2
    real(rp)                                          :: faw
    faw = CP2 * P(Af2) + CQ2 * Q(Af2, Wf2) + CP1 * P(Af1) + CQ1 * Q(Af1, Wf1) + Palpha
end function F
function derF_A(Af1, Af2, Wf1, Wf2) result(dfaw)
    real(rp),     intent(in)                          :: Af1, Af2, Wf1, Wf2
    real(rp),     dimension(2)                        :: derP, derQ, dfaw
    derP = [ 0.5_rp * nodeList%beta(nd) / ( nodeList%dyArea(nd) * Af1**0.5_rp ) , &
         &   0.5_rp * nodeList%beta(nd) / ( nodeList%dyArea(nd) * Af2**0.5_rp ) ]
    derQ = [ Wf1 - sgn * ( 5.0_rp * B * Af1**0.25_rp - 4.0_rp * c0 ) , &
         &   Wf2 - sgn * ( 5.0_rp * B * Af2**0.25_rp - 4.0_rp * c0 ) ]
    dfaw = [ CP1 * derP(1) + CQ1 * derQ(1) , CP2 * derP(2) + CQ2 * derQ(2) ]
end function derF_A

end function windkessAl

end module winkes
