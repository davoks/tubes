program main

    use mesher
    use femodule
    use inicon
    use checks 
    use io
    use galtub

    implicit none

!-----
! Try to read an stored domain file with the data for the problem (domain.txt). If the file does not exist,
! parse the file conf.txt with simulation parameters related to fluid and wall materials and file geom.txt with geometric definition
! of the network and the boundary conditions definitions.
!-----

    call readCasename()

    open(fIdx%logfile, file=logFileName)

    ! Set input files revision number to read
    gVar%confRev = 1
    gVar%geomRev = 1

    write(*,*)
    write(*,*) ">>> MESH GENERATION <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> MESH GENERATION <<<"
    call getMesh()

    write(*,*)
    write(*,*) ">>> FINITE ELEMENTS INITIALIZATION <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> FINITE ELEMENTS INITIALIZATION <<<"
    call initFE()

    write(*,*)
    write(*,*) ">>> SET INITIAL CONDITIONS <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> SET INITIAL CONDITIONS <<<"
    call setIniConds()

    write(*,*)
    write(*,*) ">>> CHECK PROBLEM DEFINITION <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> CHECK PROBLEM DEFINITION <<<"
    call checkMesh()

    write(*,*)
    write(*,*) ">>> INITIAL DUMP WRITE <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> INITIAL DUMP WRITE <<<"
    call writeDump(.true.)

    write(*,*)
    write(*,*) ">>> RUN SIMULATION <<<"
    write(fIdx%logfile,*)
    write(fIdx%logfile,*) ">>> RUN SIMULATION <<<"
    call runSim()

    close(fIdx%logfile)

end program main
