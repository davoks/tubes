module physfuncs

    use nrtype
    use clases

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Returns the euclidean distance between points a and b in 3D space
function dist(a, b) result(l)

    integer(ip)                           :: i
    real(rp), dimension(3), intent(in)    :: a, b
    real(rp)                              :: l

    l = 0.0_rp
    do i = 1, 3
        l = l + (a(i) - b(i))**2
    end do
    l = sqrt(l)

end function dist
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Returns the value of the integral of f(x) in the interval x in [-1, 1] calculated by means of
!   Gaussian quadrature using np integration points.
!   The method is exact for polynomials of order (2*np - 1)
function integralGQ(np, f) result(integral)

    real(rp)                              :: integral
    integer(ip),  intent(in)              :: np           ! number of integration points
    real(rp),     dimension(np)           :: pts, wts     ! list of points locations and weights
    real(rp)                              :: pt0, pt1, pt2, wt0, wt1, wt2
    integer(ip)                           :: i

    interface
        function f(x)
            use nrtype
            implicit none
            real(rp)                      :: f
            real(rp), intent(in)          :: x
        end function
    end interface

    ! gauss integration points and weights for successive orders of integration [1...4]
    select case(np)
    case(1)
        pts = [ 0.0_rp ]
        wts = [ 2.0_rp ]
    case(2)                                                                     ! 1st order
        pts = (/ -(1._rp/3._rp)**0.5_rp , (1._rp/3._rp)**0.5_rp /)
        wts = (/ 1._rp          , 1._rp      /)
    case(3)                                                                     ! 2nd order
        pt0 = 0._rp
        pt1 =  (3._rp/5._rp)**0.5_rp
        wt0 = 8._rp/9._rp
        wt1 = 5._rp/9._rp
        pts = (/ -pt1 , pt0 , pt1 /)
        wts = (/  wt1 , wt0 , wt1 /)
    case(4)                                                                     ! 3rd order
        pt0 = ( 3._rp/7._rp - 2._rp/7._rp * (6._rp/5._rp)**.5_rp )**.5_rp
        pt1 = ( 3._rp/7._rp + 2._rp/7._rp * (6._rp/5._rp)**.5_rp )**.5_rp
        wt0 = (18 + 30**.5_rp) / 36._rp
        wt1 = (18 - 30**.5_rp) / 36._rp
        pts = (/ -pt1 , -pt0 , pt0 , pt1 /)
        wts = (/  wt1 ,  wt0 , wt0 , wt1 /)
    case(5)                                                                     ! 4th order
        pt0 = 0._rp
        pt1 = ( 5._rp - 2._rp * (10._rp/7._rp)**.5_rp )**.5_rp / 3._rp
        pt2 = ( 5._rp + 2._rp * (10._rp/7._rp)**.5_rp )**.5_rp / 3._rp
        wt0 = 128._rp/225._rp
        wt1 = (322._rp + 13._rp*70**.5_rp) / 900._rp
        wt2 = (322._rp - 13._rp*70**.5_rp) / 900._rp
        pts = (/ -pt2 , -pt1 , pt0 , pt1 , pt2 /)
        wts = (/  wt2 ,  wt1 , wt0 , wt1 , wt2 /)
    case default
        write(fIdx%logfile,*)
        write(fIdx%logfile,*) "ERROR --- Number of quadrature points not implemented: ", np
        stop
    end select

    integral = 0.0_rp           ! initialize integration value
    do i = 1, np                ! summatory over integration points
        integral = integral + wts(i) * f(pts(i))
    end do

end function integralGQ
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Vector multiplication with matrix LM assebled from elementary matrices M
!   w = LM * v
function elemMatMul(M, v) result (w)

    real(rp), dimension(:,:),   intent(in)                :: M
    real(rp), dimension(:),     intent(in)                :: v
    real(rp), dimension(size(v))                          :: w
    integer(ip)                                           :: nod_elem, nPoints
    integer(ip)                                           :: i

    nod_elem = size(M,1)
    nPoints = size(v)

    w = 0.0_rp
    do i = 1, (nPoints - 1), (nod_elem - 1)
        w(i:i+nod_elem-1) = w(i:i+nod_elem-1) + matmul(M, v(i:i+nod_elem-1))
    end do

end function elemMatMul
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Pressure computation based on the values of beta [Bt], nodal area [ar],
!   dyastolic area [dyA], exterior pressure [extP] and dyastolic pressure [dyP]
elemental function pressure(Bt, dyA, extP, dyP, ar) result (p)

    real(rp), intent(in)                                  :: Bt, dyA, extP, dyP, ar
    real(rp)                                              :: p

    p = extP + dyP + Bt / dyA * ( ar**0.5_rp - dyA**0.5_rp )

end function pressure
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Compute the pressure on all the nodes corresponding to the segment [iseg]
subroutine pressureSegment(iseg)

    integer(ip),  intent(in)                      :: iseg
    integer(ip)                                   :: n1, n2
    real(rp)                                      :: dyP

    n1 = elemList%nodLimit(1, segList%elemLimit(1, iseg))
    n2 = elemList%nodLimit(2, segList%elemLimit(2, iseg))

    ! Get dyastolic pressure value from material list
    dyP = WallMatList( segList%mat(iseg) )%rParam(3)

    ! Compute pressure between nodes n1 and n2 based on the nodal values of beta, dyastolic area
    ! dyastolic pressure, exterior pressure and area
    nodeList%p(n1:n2) = pressure(nodeList%beta(n1:n2), nodeList%dyArea(n1:n2), &
        & FlPar%Pext, dyP, nodeList%u(1, n1:n2))

end subroutine pressureSegment
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   compute characteristics eigenvalues  lambda_[1,2] = u +/- c
!                                        c = sqrt(beta / (2 * rho * Ad)) * A^(1/4)
function eigenvals(a, q, beta, ad, ch) result(lambda)

    real(rp),    intent(in) :: a, q, beta, ad
    integer(ip), intent(in) :: ch
    real(rp)                :: lambda

    lambda = 0.0
    select case(ch)
    case(1)         ! characteristic w1
        lambda = q / a + sqrt(0.5_rp * beta / (FlPar%rho * ad)) * a**0.25_rp
    case(2)         ! characteristic w2
        lambda = q / a - sqrt(0.5_rp * beta / (FlPar%rho * ad)) * a**0.25_rp
    end select

end function eigenvals
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Obtain variables w1 or w2 from physical values
!   w1 = Q/A + 4 · (beta/(2 · rho · Ad))^{1/2} · (A^{1/4} - Ad^{1/4})
!   w2 = Q/A - 4 · (beta/(2 · rho · Ad))^{1/2} · (A^{1/4} - Ad^{1/4})
function phys2Char(u, beta, ad, nCh) result(w)

    real(rp),     dimension(:,:), intent(in)          :: u
    real(rp),     dimension(:),   intent(in)          :: beta, ad
    integer(ip),                  intent(in)          :: nCh
    real(rp),     dimension(size(u,2))                :: w

    select case(nCh)
    case(1)
        w(:) = u(2,:) / u(1,:) + 4.0_rp * sqrt(0.5_rp * beta(:) / (FlPar%rho * ad(:))) * &
            & (u(1,:)**0.25_rp - ad(:)**0.25_rp)
    case(2)
        w(:) = u(2,:) / u(1,:) - 4.0_rp * sqrt(0.5_rp * beta(:) / (FlPar%rho * ad(:))) * &
            & (u(1,:)**0.25_rp - ad(:)**0.25_rp)
    end select

end function phys2Char
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Obtain A and Q from values of w1 and w2 and physical parameters
function char2Phys(w1, w2, beta, ad) result(u)

    real(rp),     dimension(:),   intent(in)          :: w1, w2, beta, ad
    real(rp),     dimension(2,size(beta))             :: u

    ! get A from: A = ( ( 2 * rho * Ad / beta)^0.5 * (w1 - w2) / 8 + Ad^(1/4) )^4
    u(1,:) = ( (2.0_rp * FlPar%rho * ad(:) / beta(:))**0.5_rp * &
           & (w1(:) - w2(:)) / 8.0_rp + ad(:)**0.25_rp)**4.0_rp
    ! get Q from value of A and from u: Q = u * A   u = (w1 + w2) / 2
    u(2,:) = u(1,:) * 0.5_rp * (w1(:) + w2(:))

end function char2Phys
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Gaussian distribution single valued function
elemental function gaussF(x, mu, sig, amp, off) result(g)

    real(rp), intent(in)                      :: x, mu, sig, amp, off
    real(rp)                                  :: g

    g = off + amp * exp( - 0.5_rp*( (x - mu)/sig )**2.0_rp)

end function gaussF
!----------------------------------------------------------------------------------------------

end module physfuncs
