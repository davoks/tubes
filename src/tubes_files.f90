subroutine tubes_files(casename_in , in_conf, in_geom, in_domn, in_log, in_segm, in_extfun, in_wit)

  use clases
  
  character(len=*),   intent(in)  :: casename_in
  integer(4),         intent(in)  :: in_conf
  integer(4),         intent(in)  :: in_geom
  integer(4),         intent(in)  :: in_domn
  integer(4),         intent(in)  :: in_log
  integer(4),         intent(in)  :: in_segm
  integer(4),         intent(in)  :: in_extfun
  integer(4),         intent(in)  :: in_wit
  
  fIdx%geometry      = in_geom 
  fIdx%configuration = in_conf
  fIdx%domain        = in_domn
  fIdx%logfile       = in_log
  fIdx%segment       = in_segm
  fIdx%extFunc       = in_extfun
  fIdx%witnessP      = in_wit

  casename     = trim(casename_in)
  ConfFileName = trim(casename) // '.conf.txt'
  GeomFileName = trim(casename) // '.geom.txt'
  DomnFileName = trim(casename) // '.domn.txt'
  LogFileName  = trim(casename) // '.log'
  
  open(fIdx%configuration, file=LogFileName, status='unknown') ! open file for writing
  
end subroutine tubes_files
