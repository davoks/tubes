subroutine advance_timestep_tubes(Q, P)

    use clases
    use nrtype
    use galtub
    use io
    use omp_lib

    implicit none

    real(rp), intent(in)  :: Q
    real(rp), intent(out) :: P
    integer(ip)           :: iwit, n1
    real(rp)              :: progress

    n1 = elemList%nodLimit(1, segList%elemLimit(1, 1))

    !
    ! Thread initialisation
    !
    call omp_set_num_threads( min( segList%num, omp_get_num_procs() ) )
    !$OMP parallel default(firstprivate), &
    !$OMP & shared( bcList,         icList,     scICList,   scSrcList,&
    !$OMP &         WallMatList,    FlPar,      isoMat,     cpList,&
    !$OMP &         segList,        elemList,   nodeList,   dom,        gVar)

    !
    ! Update time variable
    !
    !$OMP single
    gVar%t  = gVar%t  + gVar%dt
    gVar%it = gVar%it + 1
    !$OMP end single

    !
    ! Read flowrate from external program to be prescribed at inlet
    !
    gVar%Q_external = Q

    !
    ! Advance timestep
    !
    !$OMP barrier
    call timeloop()
    !$OMP barrier
    !$OMP flush

    !
    ! Write to file
    !
    !$OMP do schedule(dynamic)
    do iwit = 1, wpList%num
        call writeWP(iwit)
    end do
    !$OMP end do
    !$OMP single
    if ( mod(gVar%it,gVar%ntspd) .eq. 0 ) then
        progress = real(gVar%it * 100)/real(gVar%ntime)
        write(fIdx%logfile,'(a, i6, a, f5.1, a)') 'Time step: ', gVar%it, ': ', progress, '% completed'
        call writeDump(.true.)
    end if
    !$OMP end single

    !$OMP end parallel

    !
    ! Extract pressure from inlet node
    !
    n1 = elemList%nodLimit(1, segList%elemLimit(1, 1))
    P  = nodeList%p(n1)

end subroutine advance_timestep_tubes
