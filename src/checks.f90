module checks

    use nrtype
    use clases 

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
subroutine checkMesh()

   real(rp), allocatable :: B(:), c0(:)
   real(rp)              :: cmax
   integer(ip)           :: iseg, i1, i2

   allocate( B(nodeList%num) )
   allocate( c0(nodeList%num) )

   ! Check if all segments are longer than distance travelled by charactertics in 1 timestep
   B  = (0.5_rp * nodeList%beta(:) / (FlPar%rho * nodeList%dyArea(:)))**0.5_rp
   c0 = B * nodeList%dyArea(:)**0.25_rp
   do iseg = 1, segList%num
      i1 = elemList%nodLimit(1,segList%elemLimit(1,iseg))
      i2 = elemList%nodLimit(2,segList%elemLimit(2,iseg))
      cmax = maxval( c0(i1:i2) )
      if ( (gVar%dt*cmax) > segList%Length(iseg) ) then
         print*,'---ERROR: All segments must be longer than dt*lambda.'
         print*,'dt*c=',gVar%dt*cmax
         print*,'length=',segList%Length(iseg)
         stop
      end if
   end do

   deallocate( B )
   deallocate( c0 )

   ! Check for loose nodes 
   ! Check for loose elements
   ! Check that all edge elements are either at a CP or boundary 
   ! Check consistency in number of nodes, elements, CPs, boundaries and segments 
   ! etc...  

end subroutine checkMesh
!----------------------------------------------------------------------------------------------


end module checks 
