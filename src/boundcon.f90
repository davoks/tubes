module boundcon

    use clases
    use nrtype
    use femodule
    use linalg
    use physfuncs
    use winkes

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Discriminate procedures to set values at control points based on the kind of connection of
!   each one (internal / external)
!   Updates the value of cpList%uBV(cp)%r(:)
subroutine getBouConds(cp)

    integer(ip),      intent(in)                      :: cp

    ! discriminate outer from inner boundary conditions (inner boundaries don't appear in
    !       bcList and are assigned an index of 0 in cpList%bcNum(...)
    if ( cpList%bcNum(cp) .eq. 0 ) then
        call getIntBC(cp)                   ! Inner node boundary condition
    else
        call getExtBC(cp)                   ! External network boundary conditions
    end if

end subroutine getBouConds
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Apply calculated values at the control points to the node list
subroutine applyBouConds(cp)

    integer(ip),      intent(in)                      :: cp
    integer(ip)                                       :: n, k
    type(IArray),     pointer                         :: ndL

    ! get list of nodes at control point cp
    ndL => cpList%nodeL(cp)
    ! loop over nodes that are present in the control point
    do n = 1, size(ndL%i)           ! n is the numbered node at CP
        ! set unknown variables values (Dirichlet condition)
        k = (n - 1) * gVar%dof
        nodeList%u(:,ndL%i(n)) = cpList%uBV(cp)%r(k+1:k+gVar%dof)
    end do

    ! set scalar variables values if it is an internal node
    ! External nodes get currently calculated right when system is solved
    if ( (gVar%nadv .ne. 0) .and. ( cpList%bcNum(cp) .eq. 0) ) call updateIntScalars()
    nullify( ndL )               ! pointer set to NULL

contains
! update scalar values for internal nodes
subroutine updateIntScalars()
    integer(ip)                                       :: i, nd, cpN
    real(rp),     dimension(gVar%nadv)                :: AvgSc
    cpN = size(ndL%i)
    AvgSc = 0.0_rp
    ! Set the scalar value for all the nodes in the internal CP as the average of their value
    do i = 1, cpN
        nd = ndL%i(i)                       ! Global node index
        AvgSc(:) = AvgSc(:) + nodeList%scal(:,nd) ! Sum of scalar values
    end do
    AvgSc(:) = AvgSc(:) / real(cpN,8)       ! Average of scalar values amongst nodes in the CP
    do i = 1, cpN
        nd = ndL%i(i)                       ! Global node index
        nodeList%scal(:,nd) =  AvgSc(:)     ! The average value is assigned
    end do
end subroutine updateIntScalars

end subroutine applyBouConds
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   External boundary conditon setting for the control point [cp]
!   The values of cpList%uBV(cp)%r(:) get updated at the end of the routine for not inernal CP
subroutine getExtBC(cp)

    integer(ip),      intent(in)                      :: cp
    integer(ip)                                       :: bcI, ndNum
    real(rp),         dimension(:),   allocatable     :: a0, aD, aNext, B, qNext
    real(rp),         dimension(:),   allocatable     :: w1Next, w2Next
    real(rp),         dimension(:,:), allocatable     :: uTemp
    integer(ip)                                       :: j, k, nd, m
    integer(ip)                                       :: ipNum, rpNum
    integer(ip),      dimension(:),   allocatable     :: iParBC
    real(rp),         dimension(:),   allocatable     :: rParBC
    integer(ip)                                       :: seg, segEnd, scBCTy

    ! Index for boundary condition applied on [cp] control point
    bcI = cpList%bcNum(cp)

    ndNum = size(cpList%nodeL(cp)%i)            ! number of nodes at CP
    allocate( aD(       ndNum ) )               ! Dyastolic area on CP nodes
    allocate( a0(       ndNum ) )               ! equilibrium area on CP nodes
    allocate( aNext(    ndNum ) )               ! Value of A to be applied on CP nodes
    allocate( B (       ndNum ) )               ! Beta parameter
    allocate( qNext(    ndNum ) )               ! Value of Q to be applied on CP nodes
    allocate( w1Next(   ndNum ) )               ! Characteristic 1 at CP nodes
    allocate( w2Next(   ndNum ) )               !       and characteristic 2
    allocate( uTemp( 2, ndNum ) )               ! temp array to simplify assignment

    rpNum = bcList%nRealP(bcI)                  ! number of real parameters for boundary condition
    allocate( rParBC(   rpNum ) )               ! copy of real parameters
    ! Get real parameters from boundary condition definition
    rParBC(:) = bcList%rpList(bcI)%r(:)
    ipNum = bcList%nIntP(bcI)                   ! number of integer parameters
    allocate( iParBC(   ipNum ) )               ! copy of integer parameters
    iParBC(:) = bcList%ipList(bcI)%i(:)         ! get integer parameters

    ! Set A offset to consistent value for the different nodes at the CP,
    !       order is defined by the array cpList%nodeL(cp)%i.   (aOff == aDy)
    aD(:) = nodeList%dyArea(cpList%nodeL(cp)%i(:))
    a0(:) = nodeList%area0(cpList%nodeL(cp)%i(:))
    ! Set values of beta for the different nodes
    B(:) = nodeList%beta(cpList%nodeL(cp)%i(:))

    select case (bcList%type(bcI))                ! type of boundary selection

    ! Impose A at boundary. Outgoing characteristic is extrapolated and incoming
    !       characteristic and Q are computed
    case (1)
        ! Operations over the nodes present at the CP
        !       (they run in order, first incoming and next outgoing)
        do j = 1, cpList%inN(cp)      ! incoming segment (characteristic entering cp is w1)
            ! Fix value of A as a function of time for the diferent nodes at the CP
            rParBC(1) = a0(j)
            aNext(j) = valueAtBoundary(bcI, rParBC, gVar%t)
            nd = cpList%nodeL(cp)%i(j)
            ! extrapolate characteristic
            w1Next(j) = extChar(nd, .false., 1)        ! false -> node at the end of segment
            ! Calculate w2 using the relation:
            !       w1 - w2 = 8 * sqrt(beta/(2 * rho * Ad)) * (A^(1/4) - A0^(1/4))
            w2Next(j) = w1Next(j) - 8.0_rp * (0.5_rp * B(j) / (FlPar%rho * aD(j)))**0.5_rp * &
                   & (aNext(j)**0.25_rp - aD(j)**0.25_rp)
        end do
        ! outgoing segment (characteristic entering cp is w2)
        do j = cpList%inN(cp) + 1, ndNum
            rParBC(1) = a0(j)
            aNext(j) = valueAtBoundary(bcI, rParBC, gVar%t)
            nd = cpList%nodeL(cp)%i(j)
            w2Next(j) = extChar(nd, .true., 2)          ! true -> node at beginning of segment
            w1Next(j) = w2Next(j) + 8.0_rp * (0.5_rp * B(j) / (FlPar%rho * aD(j)))**0.5_rp * &
                    & (aNext(j)**0.25_rp - aD(j)**0.25_rp)
        end do
        ! Calculate value for Q
        qNext(:) = 0.5_rp * (w1Next(:) + w2Next(:)) * aNext(:)
        ! Change type of boundary condition if temporal conditions require it
        if ( makeNonReflecting() ) bcList%type(bcI) = 4

    ! Impose Q at boundary. Outgoing characteristic is extrapolated and A computed.
    case (2)
        ! Obtain Q as a function of time
        qNext(:) = valueAtBoundary(bcI, rParBC, gVar%t)
        do j = 1, cpList%inN(cp)      ! incoming segment (characteristic entering cp is w1)
            nd = cpList%nodeL(cp)%i(j)
            ! extrapolate characteristic w1
            w1Next(j) = extChar(nd, .false., 1)     ! false -> end of the segment
            ! estimate w2 using the relation:   u = (w1 + w2) / 2
            w2Next(j) = - w1Next(j) + 2.0_rp * qNext(j) / nodeList%u(1, nd)
        end do
        ! outgoing segment (characteristic entering cp is w2)
        do j = cpList%inN(cp) + 1, ndNum
            nd = cpList%nodeL(cp)%i(j)
            w2Next(j) = extChar(nd, .true., 2)       ! true -> beginning of the segment
            w1Next(j) = - w2Next(j) + 2.0_rp * qNext(j) / nodeList%u(1, nd)
        end do
        ! compute A
        !       from: A = ( ( 2 * rho * Ad / beta)^0.5 * (w1 - w2) / 8 + Ad^(1/4) )^4
        aNext(:) = ( (2.0_rp * FlPar%rho * aD(:) / B(:))**0.5_rp * &
                 & (w1Next(:) - w2Next(:))/8.0_rp + aD(:)**0.25_rp)**4.0_rp
        ! Change type of boundary condition if temporal conditions require it
        if ( makeNonReflecting() ) bcList%type(bcI) = 4

    ! Closed end
    case (3)
        ! TODO

    ! Non-reflecting boundary condition
    case (4)
        do j = 1, cpList%inN(cp)      ! incoming segment (characteristic entering cp is w1)
            nd = cpList%nodeL(cp)%i(j)
            w1Next(j) = extChar(nd, .false., 1)     ! node is at the end of the segment
            w2Next(j:j) = max( 0.0_rp, phys2Char(nodeList%u(:,nd:nd), B(j:j), aD(j:j), 2) )
        end do
        ! outgoing segment (characteristic entering cp is w2)
        do j = cpList%inN(cp) + 1, ndNum
            nd = cpList%nodeL(cp)%i(j)
            w2Next(j) = extChar(nd, .true., 2)       ! node is at beginning of the segment
            w1Next(j:j) = min( 0.0_rp, phys2Char(nodeList%u(:,nd:nd), B(j:j), aD(j:j), 1) )
        end do
        ! compute A and Q from values of w1 and w2
        uTemp = char2Phys(w1Next, w2Next, B, aD)
        aNext(:) = uTemp(1,:)
        qNext(:) = uTemp(2,:)

    ! Windkessel boundary condition [ALternative]
    case (6)
        do j = 1,  cpList%inN(cp)           ! incoming segment (w1 into CP, w2 out of CP)
            nd = cpList%nodeL(cp)%i(j)      ! Get global index for current node in cp
            ! extrapolate characteristic exiting segment (w1)
            w1Next(j) = extChar(nd, .false., 1)
            ! Obtain the other characteristic from Windkessel
            w2Next(j) = windkessAL(rParBC, nd, .true., W1Next(j))
        end do
        do j = cpList%inN(cp) + 1, ndNum    ! outgoing segments (w1 out of CP, w2 into CP)
            nd = cpList%nodeL(cp)%i(j)
            ! extrapolate characteristic exiting segment (w2)
            w2Next(j) = extChar(nd, .true., 2)
            ! Obtain the other characteristic from Windkessel
            w1Next(j) = windkessAL(rParBC, nd, .false., W2Next(j))
        end do
        ! compute A and Q from values of w1 and w2   [TODO]: Include in previous loops
        uTemp = char2Phys(w1Next, w2Next, B, aD)
        aNext(:) = uTemp(1,:)
        qNext(:) = uTemp(2,:)
        ! Change type of boundary condition if temporal conditions require it
        if ( makeNonReflecting() ) bcList%type(bcI) = 4

    case default
        write(fIdx%logfile,*)
        write(fIdx%logfile,*) "ERROR --- Not recognised boundary type: ", bcList%type(bcI)
        stop
    end select

    ! update values of unknowns and scalars in the cpList Boundary Values arrays
    do j = 1, ndNum
        k = 2 * (j - 1) + 1
        ! Unknowns update on cpList boundary value
        cpList%uBV(cp)%r(k: k+1) = [ aNext(j), qNext(j) ]
        ! Scalar boundary values calculation and update on cpList
        seg = nodeList%seg( cpList%nodeL(cp)%i(j)  )    ! Find the segment containing j in the CP
        if ( j .le. cpList%inN(cp) ) then               ! Segment enters CP -> node at end of segment
            segEnd = 2
        else                                            ! Segment exits CP -> node at start of segment
            segEnd = 1
        end if
        do m = 1, gVar%nadv
            ! The 1st integer parameter in the BC defines the index of the scalar BC to use at that CP
            if (iParBC(1) .eq. 0) then
                scBCTy = 1                                              ! No BC defined -> scalar value of 0 is impossed
            else
                scBCTy = scBCList%type( m , iParBC(1) )                 ! get the type of BC for the scalar m on the CP
            end if
            select case( scBCTy )                                       ! Select if Dirichlet or Neuman conditions are used
            case(1)                                                     ! Scalar value defined -> Dirichlet
                ! cpList%scBV(m, cp) = scAtBoundary(iParBC(1), m, gVar%t) ! Get the value of the scalar m
                ! Same as above, but updating in the current scalar list directly (solution of system should include it right)
                nodeList%scal(m, cpList%nodeL(cp)%i(j)) = scAtBoundary(iParBC(1), m, gVar%t)
                dom(seg)%scBCType(m, segEnd) = 0                        ! Store in domain definition as Dirichlet
            case(2)                                                     ! [TODO] impose scalar flux -> Neumann
                dom(seg)%scBCType(m, segEnd) = 1                        ! Store in domain definition as Neumann
                ! Strategy is to calculate the flux and assign it on the dom(s)%scFlux array used for internal CP
                write(fIdx%logfile,*) "ERROR --- Boundary condition type for scalar transport not implemented: ", scBCTy
                stop
            end select
        end do
    end do

    deallocate(aD, a0, aNext, B, qNext, w1Next, w2Next, uTemp, iParBC, rParBC)

contains
!   Function that returns true when a boundary condition has to be set to non-reflecting
function makeNonReflecting() result(flag)
    real(rp)                                      :: tCh
    logical                                     :: flag
    ! 6th real parameter of boundary condition is the time at which it becomes
    !       non-reflecting
    tCh = bcList%rpList(bcI)%r(6)
    flag = .false.
    if ( gVar%t .gt. tCh ) then
        print*,'CHANGING BOUNDARY CONDITION TO NON-REFLECTING'
        write(fIdx%logfile,*) 'Boundary condition changed: ', bcI
        write(fIdx%logfile,*) '    t =', gVar%t
        write(fIdx%logfile,*) '    tChange =', tCh
        flag = .true.
    end if
end function makeNonReflecting

end subroutine getExtBC
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Function to calculate the extrapolated value of a characteristic
!   nd:     global index of node where the characteristic is calculated
!   segSt:  .true. if [nd] is the node with the lower index on the segment (start of segment)
!           .false. if [nd] is the node with the higher index (end of segment)
!   nCh:    Number of characteristic being extrapolated
function extChar(nd ,segSt, nCh) result(wExt)

    integer(ip),                  intent(in)          :: nd, nCh
    logical,                      intent(in)          :: segSt    ! true -> node at start of segment
    real(rp)                                          :: wExt
    real(rp)                                          :: xw, sgn, isoX
    real(rp)                                          :: lambdaBound
    integer(ip)                                       :: seg, el, n1, n2, i
    real(rp),     dimension(gVar%n_el)                :: wValues, shF

    seg = nodeList%seg(nd)              ! get index for the segment

    ! Get eigenvalues at boundary
    lambdaBound = eigenvals(nodeList%u(1, nd), nodeList%u(2, nd), nodeList%beta(nd), &
                & nodeList%dyArea(nd), nCh)

    select case(nCh)        ! Sign depending on the characteristic to extrapolate
    case(1)                 ! w1 (along segment direction)
        sgn = 1._rp
    case(2)                 ! w2 (against segment direction)
        sgn = -1._rp
    end select

    ! Obtain location on segment linear coordinates where characteristic is extrapolated
    ! [el] will be the element containing the location
    select case(segSt)
    case(.true.)                    ! characteristic at start of segment
        xw = nodeList%linX(nd) + sgn * lambdaBound * gVar%dt
        el = segList%elemLimit(1, seg)      ! get index for 1st element of segment
        do while (xw .gt. nodeList%linX(elemList%nodLimit(2, el)))
            el = el + 1
        end do          ! The final value of el is the index of the element containing xw location
    case(.false.)                   ! characteristic at end of segment
        xw = nodeList%linX(nd) - sgn * lambdaBound * gVar%dt
        el = segList%elemLimit(2, seg)      ! get index for last element of segment
        do while (xw .lt. nodeList%linX(elemList%nodLimit(1, el)))
            el = el - 1
        end do
    end select

    ! Get values of characteristic at nodal indices of the element
    n1 = elemList%nodLimit(1, el)
    n2 = elemList%nodLimit(2, el)
    wValues(:) = phys2Char(nodeList%u(:, n1:n2), nodeList%beta(n1:n2), &
            & nodeList%dyArea(n1:n2), nCh)

    isoX = -1.0_rp + 2.0_rp * (xw - nodeList%linX(n1)) / segList%dx(seg)
    if ( abs(isoX) .gt. 1.0 ) then
        write(fIdx%logfile,*) "Extrapolation out of element"
    end if
    shF(:) = [ (shapeF(gVar%n_el, i, isoX), i = 1, gVar%n_el) ]
    ! Calculate characteristic as linear combination of wValues and shape functions
    wExt  = dot_product( shF , wValues )

end function extChar
!----------------------------------------------------------------------------------------------



! TEST COMPUTATION FOR NON-REFLECTING CONDITION
! obtain w1 and w2 at boundary points with alternative method:
!       dw1_dt = [- lambda1 · dw1_dx] - Kr · Q / A^2
!       dw2_dt = [- lambda2 · dw2_dx] - Kr · Q / A^2
! [] term appears only on exiting characteristic
! This formula works only for beta and Ady constant in the segment. if they depend on x
!   new terms have to be included in the rhs
subroutine nonRefChar(nd, segSt, w1, w2)
    integer(ip),      intent(in)                      :: nd
    logical,          intent(in)                      :: segSt
    real(rp),         intent(out)                     :: w1, w2
    integer(ip)                                       :: seg, el, i, n1, n2
    real(rp)                                          :: x, dF, dw1, dw2
    real(rp),     dimension(2)                        :: lambda
    real(rp),     dimension(gVar%n_el, 2)             :: wValues  ! [ [w1Values], [w2Values] ]

    seg = nodeList%seg(nd)              ! get index for the segment

    ! get eigenvalues at boundary element node
    lambda(:) = [(eigenvals(nodeList%u(1, nd), nodeList%u(2, nd), &
             & nodeList%beta(nd), nodeList%dyArea(nd), i), i = 1, 2)]

    select case(segSt)                      ! start of segment condition
    case(.true.)                            ! characteristics at start of segment (w1 enters)
        el = segList%elemLimit(1, seg)      ! get element at boundary
        call getWValues()                   ! Read characteristic values at nodes of element
        x = -1.0_rp                         ! Iso-coordinate for start node of element
        dw2 = 0.0_rp
        do i = 1, gVar%n_el                 ! Sum contributions of all shape functions derivatives (in iso-coords)
            dF = dShapeF_dx(gVar%n_el, i, x)
            dw2 = dw2 + wValues(i, 2) * dF
        end do
        wValues(1,1) = wValues(1,1) - gVar%dt * (FlPar%kr * nodeList%u(2,nd) / nodeList%u(1,nd)**2)
        wValues(1,2) = wValues(1,2) - gVar%dt * (lambda(2) * dw2 * 2.0_rp / segList%dx(seg) &
                     & + FlPar%kr * nodeList%u(2,nd) / nodeList%u(1,nd)**2)
        w1 = wValues(1, 1)                  ! entering characteristic
        w2 = wValues(1, 2)                  ! exiting characteristic
    case(.false.)                           ! characteristics at end of segment (w2 enters)
        el = segList%elemLimit(2, seg)      ! get element at boundary
        call getWValues()                   ! Read characteristic values at nodes of element
        x = 1.0_rp                          ! Iso-coordinate for end node of element
        dw1 = 0.0_rp
        do i = 1, gVar%n_el                 ! Sum contributions of all shape functions derivatives (in iso-coords)
            dF = dShapeF_dx(gVar%n_el, i, x)
            dw1 = dw1 + wValues(i, 1) * dF
        end do
        wValues(gVar%n_el, 1) = wValues(gVar%n_el, 1) &
                              & - gVar%dt * (lambda(1) * dw1 * 2.0_rp / segList%dx(seg) &
                              & + FlPar%kr * nodeList%u(2,nd) / nodeList%u(1,nd)**2)
        wValues(gVar%n_el, 2) = wValues(gVar%n_el, 2) &
                              & - gVar%dt * (FlPar%kr * nodeList%u(2,nd) / nodeList%u(1,nd)**2)
        w1 = wValues(gVar%n_el, 1)          ! exiting characteristic
        w2 = wValues(gVar%n_el, 2)          ! entering characteristic
    end select

contains
! Get values of characteristics at nodal indices of the element
subroutine getWValues()
    n1 = elemList%nodLimit(1, el)
    n2 = elemList%nodLimit(2, el)
    wValues(:,1) = phys2Char(nodeList%u(:, n1:n2), nodeList%beta(n1:n2), &
                 & nodeList%dyArea(n1:n2), 1)
    wValues(:,2) = phys2Char(nodeList%u(:, n1:n2), nodeList%beta(n1:n2), &
                 & nodeList%dyArea(n1:n2), 2)
end subroutine getWValues

end subroutine nonRefChar




!----------------------------------------------------------------------------------------------
!
!   Internal boundary conditon setting for the control point [cp]
!   -> obtain values of A and Q through a N-R scheme that solves continuity eqs
!   -> Get scalar fluxes to use as many Neumann to single Dirichlet BC
subroutine getIntBC(cp)

    integer(ip),  intent(in)                          :: cp
    real(rp)                                          :: errC, errTol
    integer(ip)                                       :: nIt, maxIt, i, j, nd, rc, d, cpN
    real(rp),     dimension( size(cpList%uBV(cp)%r) ) :: u0, f
    integer(ip),  dimension( size(cpList%uBV(cp)%r) ) :: indx
    real(rp), dimension(size(cpList%uBV(cp)%r),size(cpList%uBV(cp)%r))    :: df_du

    ! Unknowns -> Run over all the nodes storing unknown and characteristic values on the CP list Boundary Values list.
    ! Scalars -> Get difussive and advective fluxes entering the CP for all the segments. Set the type of BC on domain structure.
    !   dif = - kd · dSc_dx             (difussion flux)
    !   adv = u · Sc                    (advective flux)
    cpN = cpList%inN(cp) + cpList%outN(cp)
    if (gVar%nadv .ne. 0) cpList%scFlux(:, cp) = 0.0_rp         ! Initialize total scalar flux going into the cp
    do i = 1, cpList%inN(cp)                ! incoming segments
        nd = cpList%nodeL(cp)%i(i)          ! Global node index
        cpList%uBV(cp)%r((i-1)*gVar%dof+1:i*gVar%dof) = nodeList%u(:,nd)
        cpList%wBV(cp)%r(i) = extChar(nd, .false., 1)
        ! Arguments passed are the segment number and 2 because it is an incoming segment (cp at end)
        if (gVar%nadv .ne. 0 ) call setScFlux(nodeList%seg(nd), 2)
    end do
    do i = (cpList%inN(cp) + 1), cpN     ! outgoing segments
        nd = cpList%nodeL(cp)%i(i)                                      ! Global node index
        cpList%uBV(cp)%r((i-1)*gVar%dof+1:i*gVar%dof) = nodeList%u(:,nd)
        cpList%wBV(cp)%r(i) = extChar(nd, .true., 2)
        ! Arguments passed are the segment number and 1 because it is an outgoing segment (cp at start)
        if (gVar%nadv .ne. 0 ) call setScFlux(nodeList%seg(nd), 1)
    end do

    ! Newton-Raphson iterative solver scheme for unknowns Q and A
    errC = 1.0_rp               ! error value init
    errTol = 0.001_rp           ! error value tolerance to convergence
    nIt = 0                     ! iteration counter
    maxIt = int(1e5)            ! maximum iterations
    do while ( (errC .gt. errTol) .and. (nIt .lt. maxIt) )
        nIt = nIt + 1
        u0(:) = cpList%uBV(cp)%r(:)         ! Get previous values to compare error evolution
        ! Obtain f and df_du for N-R:  [df_du] · {u-u*} = {f(u)}    with f(u*) = 0
        call contEqs(cp, f, df_du)
        ! solve linear system [df_du] · {u-u*} = {f(u)-f(u*)}
        ! Transform [df_du] in its row-wise LU decomposition.
        !       {indx} stores the permutations.
        !       d = {-1, 1}  =>  number of interchanges = {even, odd}
        !       rc = return code (1 in case of singular matrix)
        call ludcmp(df_du, size(u0), indx, d, rc)
        if (rc.eq.0) then       ! return code != 1 => matrix is not singular
            ! Uses previously transformed [df_du] and {index} to solve the linear system
            !       f is returned as the vector {u - u*} that solves the system
            call lubksb(df_du, size(u0), indx, f)
        else                    ! return code == 1 => matrix is singular
            print*,'ERROR! Matrix trying to be solved is singular!'
            write(fIdx%logfile,*) 'control point index = ',cp
            write(fIdx%logfile,*) 'iterations = ',nIt
            stop
        end if
        ! Update unknown vector values          {u*} ~= {u} - {u - u*}
        cpList%uBV(cp)%r(:) = cpList%uBV(cp)%r(:) - f(:)
        ! Compare with previously stored to obtain error
        errC = maxval( abs(cpList%uBV(cp)%r(:) - u0(:)) / abs(u0(:)) )
    end do

contains
! Sets Neumann conditions on segments on Domain structure and
!   computes the average flow per segment entering the CP.
!   Assigns the flow value to the variable scFlux on CP List structure
subroutine setScFlux(seg, segEnd)
    integer(ip), intent(in)                           :: seg, segEnd
    integer(ip)                                       :: sgn, n1, nder
    real(rp),   dimension( gVar%nadv )                :: deriv
    ! Advected flux calculation depends if segment enters (segEnd = 2) or exits (segEnd = 1) the CP
    !   direction of the flux entering the CP too
    if (segEnd .eq. 1) then                 ! segment exiting the CP
        n1 = nd                             ! element first node is the node at the CP (nd -> global node number)
        nder = 1                            ! local element node where derivative is calculated is the number 1
        sgn = -1                            ! Positive flux at segment start on CP will be exiting the CP
    else                                    ! segment entering the CP
        n1 = nd - gVar%n_el + 1             ! element first node is (n_el - 1) nodes before the one at the CP
        nder = gVar%n_el                    ! local element node where derivative is calculated is the number n_el
        sgn = 1                             ! Positive flux at segment end on CP will be entering the CP
    end if
    ! The flux is stored for each node (segment) on the domain structure
    !   deriv == dsc_dx        scFlux = Q/A * sc - kd * dsc_dx (in segment geometry coordinates)
    deriv = sum([(nodeList%scal(:,n1 - 1 + j) * segList%elMat(seg)%dfij(j, nder), j = 1, gVar%n_el)])
    dom(seg)%scFlux(:,segEnd) = nodeList%u(2,nd) / nodeList%u(1,nd) * nodeList%scal(:,nd) &
                                - gVar%scalKd(:) * deriv(:)
    dom(seg)%ScBCType(:,segEnd) = 1        ! set all segments at an inner CP as Neumann BC
    ! The average flux from the segments into the CP is stored
    cpList%scFlux(:,cp) = cpList%scFlux(:,cp) + sgn * dom(seg)%scFlux(:,segEnd) / cpN
end subroutine setScFlux

end subroutine getIntBC
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Continuity equations for internal control point [cp].
!   Given the system to solve: f_[i](u*) = 0, returns {fi(u0)} and [df_du]|(u0) with value of
!   function vector and its jacobian at testing point {u0}
subroutine contEqs(cp, f, df_du)

    integer(ip),                      intent(in)      :: cp
    real(rp),         dimension(:),   intent(out)     :: f
    real(rp),         dimension(:,:), intent(out)     :: df_du
    integer(ip)                                       :: nd, i, j, k
    integer(ip)                                       :: ndN
    real(rp),         dimension(:),   allocatable     :: aD, a0, B
    real(rp),         dimension(:,:), allocatable     :: u
    real(rp)                                          :: pTotRef, dyP

    ndN = cpList%inN(cp) + cpList%outN(cp)      ! total nodes at cp

    allocate( aD(       ndN ) )                 ! Dyastolic area on nodes
    allocate( a0(       ndN ) )                 ! equilibrium area on nodes
    allocate( B (       ndN ) )                 ! Beta parameter
    allocate( u( gVar%dof,  ndN ) )             ! temp array to simplify assignment

    ! Get values of aD, beta and u for nodes at the cp (ordered in first, out last)
    aD(:) = nodeList%dyArea(cpList%nodeL(cp)%i(:))
    a0(:) = nodeList%area0( cpList%nodeL(cp)%i(:))
    B(:) = nodeList%beta(cpList%nodeL(cp)%i(:))
    do i = 1, ndN
        u(:,i) = cpList%uBV(cp)%r((i-1)*gVar%dof+1:i*gVar%dof)
    end do

    df_du = 0.0_rp

    ! 1st node (incoming) is calculated apart to take the pressure as reference value

    nd = cpList%nodeL(cp)%i(1)                  ! Get global index for 1st node
    ! mass conservation: balace of flow in the cp has to be 0 (1 equation)
    f(1) = u(2,1)                               ! Initialization of flow summatory
    ! Get pressure on first node to compare to every other: (cpN - 1) equations
! [TODO]: Verify consistency of dyastolic pressure initialization and usage
    dyP = WallMatList( segList%mat( nodeList%seg(nd) ) )%rParam(3)
    pTotRef = pFlux(u(:,1), B(1), aD(1), dyP , FlPar%Pext)
    ! Characteristics extrapolation equality: (cpN) equations
! [TODO]: Careful with the assumption that there will always be incoming segments. Verify!
    ! Get characteristic of 1st node (incoming segment => char = 1)
    f(ndN+1:ndN+1) = phys2Char(u(:,1:1), B(1:1), aD(1:1), 1) - cpList%wBV(cp)%r(1)

    ! Jacobian calculation for 1st node
    df_du(1,1:2) = [ 0.0_rp , 1.0_rp ]          ! 1st equation (mass balance)
    do k = 2, ndN                               ! equations 2 to (inN + outN)
        ! 1st node is the reference pressure => df{i}_du{1} = - dFlux{1}_du{1}
        df_du(k, 1:2) = - dpFlux(u(:,k), B(k), aD(k))
    end do
    ! characteristics partial derivative (equations ndN+1 to 2*ndN)
    df_du(ndN+1, 1:2) = dChar(u(:,1), B(1), aD(1), 1)

    ! incoming segments contribution to continuity equations
    do i = 2, cpList%inN(cp)
        j = (i - 1) * gVar%dof
        nd = cpList%nodeL(cp)%i(i)              ! Get global node index

        f(1) = f(1) + u(2,i)                    ! Add entering flow Q to cp
        ! Total pressure equality
        dyP = WallMatList( segList%mat( nodeList%seg(nd) ) )%rParam(3)
        f(i) = pFlux(u(:,i), B(i), aD(i), dyP, FlPar%Pext) - pTotRef
        f(ndN+i:ndN+i) = phys2Char(u(:,i:i), B(i:i), aD(i:i), 1) - cpList%wBV(cp)%r(i)

        df_du(1, j+1:j+2) = [ 0.0_rp , 1.0_rp ] ! incoming nodes 1st equation
        ! incoming nodes: equations 2 to inN    (pressure equalty)
        df_du(i, j+1:j+2) = dpFlux(u(:,i), B(i), aD(i))
        ! incoming nodes: equations cpN+1 to cpN+inN    (characteristics continuity)
        df_du(ndN+i, j+1:j+2) = dChar(u(:,i), B(i), aD(i), 1)
    end do

    ! outgoing segments contribution to continuity equations
    do i = (cpList%inN(cp) + 1), ndN
        j = (i - 1) * gVar%dof
        nd = cpList%nodeL(cp)%i(i)              ! Global node index

        f(1) = f(1) - u(2,i)                    ! subtract exiting flow Q to cp
        ! Total pressure equality
        dyP = WallMatList( segList%mat( nodeList%seg(nd) ) )%rParam(3)
        f(i) = pFlux(u(:,i), B(i), aD(i), dyP, FlPar%Pext) - pTotRef
        f(ndN+i:ndN+i) = phys2Char(u(:,i:i), B(i:i), aD(i:i), 2) - cpList%wBV(cp)%r(i)

        df_du(1, j+1:j+2) = [ 0.0_rp , -1.0_rp ]    ! outgoing nodes 1st equation
        ! outgoing nodes: equations inN+1 to cpN  (pressure equalty)
        df_du(i, j+1:j+2) = dpFlux(u(:,i), B(i), aD(i))
        ! outgoing nodes: equations cpN+inN+1 to 2*cpN  (characteristics continuity)
        df_du(ndN+i, j+1:j+2) = dChar(u(:,i), B(i), aD(i), 2)
    end do

    deallocate( aD, a0, B, u )

contains
! Returns the value of the total pressure (static + cynematic) as function of
!       a, q, beta, dyastolic area and dyastolic pressure
function pFlux(u, beta, ad, pd, pext) result(pF)
    real(rp),     intent(in)                          :: beta, ad, pd, pext
    real(rp),     dimension(:),   intent(in)          :: u
    real(rp)                                          :: pF
    pF = pd + pext + beta / ad * (sqrt(u(1)) - sqrt(ad)) + 0.5_rp * (u(2) / u(1))**2.0_rp
end function pFlux
! Returns an array with [ dpFlux/dA , dpFlux/dQ ]
function dpFlux(u, beta, ad) result(dpF)
    real(rp),     intent(in)                          :: beta, ad
    real(rp),     dimension(:),   intent(in)          :: u
    real(rp),     dimension(2)                        :: dpF
    dpF(:) = [ 0.5_rp * beta / ad * u(1)**(-0.5_rp) - u(2)**2 * u(1)**(-3) &
           & , u(2) * u(1)**(-2) ]
end function dpFlux
! Returns an array with [ dw/dA , dw/dQ ]
function dChar(u, beta, ad, nCh) result(dW)
    real(rp),     intent(in)                          :: beta, ad
    real(rp),     dimension(:),   intent(in)          :: u
    integer(ip),  intent(in)                          :: nCh
    real(rp)                                          :: sgn
    real(rp),     dimension(2)                        :: dW
    sgn = real(3 - 2 * nCh)     ! nCh = {1, 2}  => sgn = {1, -1}
    dW(:) = [ - u(2) / u(1)**2.0_rp + sgn * (beta/(2.0_rp*FlPar%rho*ad))**0.5_rp &
          &   * u(1)**(-0.75_rp) &
          & , 1.0_rp / u(1) ]
end function dChar

end subroutine contEqs
!----------------------------------------------------------------------------------------------


!----------------------------------------------------------------------------------------------
!
!   External boundary conditon value calculation
function valueAtBoundary(bcI, rPar, t) result(f)

    integer(ip),                  intent(in)          :: bcI
    real(rp),     dimension(:),   intent(in)          :: rPar
    real(rp),                     intent(in)          :: t
    real(rp)                                          :: f

    select case(bcList%fType(bcI))
    ! bc = 0  => constant value:   f = C
    !       rPar: ( C, _ , _ , _ , _ , tChange)
    case(0)
        f = rPar(1)

    ! bc = 1  => gaussian pulse in time: f =  C + A * e^(-(t - u)^2 / s^2)
    !       rPar: ( C, A, 1/s^2, u, _ , tChange)
    case(1)
        f = rPar(1) + rPar(2) * napier**(- rPar(3) * (t - rPar(4))**2.0_rp)

    ! bc = 2  => half sine wave pulse:
    !       f = C + K * sin( 2 * Pi * (t - t0) / T) * H(t - t0) * H(T/2 - (t - t0))
    !       rPar: (C, K, T, t0, _ , tChange)
    case(2)
        f = rPar(1)
        if ( (t - rPar(4)) .gt. 0.0_rp ) then
            if ( (rPar(3)*0.5_rp  - (t - rPar(4))) .gt. 0.0_rp ) then
                f = f + rPar(2) * sin(2.0_rp * pi / rPar(3) * (t - rPar(4)))
            end if
        end if

    ! bc = 3  => square pulse: f = C + A * H(t - t0) * H(t1 - t)
    !       rPar: (C, A, t0 , t1 , _ , tChange)
    case(3)
        f = rPar(1)
        if ( t .gt. rPar(3) ) then
            if ( t .lt. rPar(4) ) then
                f = f + rPar(2)
            end if
        end if

    case(4)  ! inlet function for common cartoid artery case from Boileau et al. (2012)
        f = cartoidInletFlowBoileau(t)

    case(5)  ! inlet function for upper thoracic aorta case from Boileau et al. (2012)
        f = aorticInletFlowBoileau(t)

    case(6)  ! inlet function for upper thoracic aorta case from Boileau et al. (2012)
        f = abdominalAorticInletFlowBoileau(t)

    case(7)  ! inlet function for 37-artery network case from Boileau et al. (2012)
        f = Arteries37InletFlowBoileau(t)

    ! bc = 8  => Cubic ramp to constant value
    !       rPar: (C, M, tin, tup, _ , tChange)
    case(8)
        f = rPar(1)
        if (t .lt. rPar(3)) then
            f = f + rPar(2) * (-2.0_rp * (t/rPar(3))**3.0_rp + 3.0_rp * (t/rPar(3))**2.0_rp)
        else if (t .lt. (rPar(3) + rPar(4)) ) then
            f = f + rPar(2)
        else if (t .lt. (2*rPar(3) + rPar(4)) ) then
            f = f + rPar(2) * (2.0_rp * ((t - rPar(3) - rPar(4))/rPar(3))**3.0_rp - &
                & 3.0_rp * ((t - rPar(3) - rPar(4))/rPar(3))**2.0_rp + 1.0_rp)
        end if

    ! bc = 9 => Linear interpolation from input file
    !       rPar: ( _ , _ , _ , _ , _ , tChange )
    case(9)
        f = linearExternal( t, bcList%extF(bcI) )

    ! bc = 10 => Flowrate read from external program (ie: Alya)
    !       rPar: ( _ , _ , _ , _ , _ , tChange )
    case(10)
        f = gVar%Q_external

    case default
        write(fIdx%logfile,*)
        write(fIdx%logfile,*) "ERROR --- Not recognised function type: ", bcList%fType(bcI)
        stop

    end select

end function valueAtBoundary
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!   ORIGINAL IMPLEMENTATION OF CASE FUNCTIONS
!----------------------------------------------------------------------------------------------
  !> Computes flow-rate prescribed at inlet at each instant in time t as done for the
  !! Common Cartoid Artery case Boileau et al. 2012. The inlet function is a superposition
  !! of sinusoidal functions.
  function cartoidInletFlowBoileau(t) result(f)

    real(rp), intent(in) :: t           !< current time
    real(rp)             :: T_period    !< base time period inlet flow-rate function
    real(rp)             :: pi_boileau  !< pi
    real(rp)             :: f           !< flow-rate prescribed at inlet at each instant in time t

    ! values of constants used in paper
    T_period   = 1.100_rp  ! by default
    ! T_period   = 1.155_rp  ! to have minimum flow at t=0
    pi_boileau = 3.141592653589793_rp

    ! compute flow-rate at time t
    f = 6.5_rp + 3.294_rp * sin(2._rp  * pi_boileau * t/T_period - 0.023974_rp)
    f = f + 1.9262_rp     * sin(4._rp  * pi_boileau * t/T_period - 1.1801_rp)
    f = f - 1.4219_rp     * sin(6._rp  * pi_boileau * t/T_period + 0.92701_rp)
    f = f - 0.66627_rp    * sin(8._rp  * pi_boileau * t/T_period - 0.24118_rp)
    f = f - 0.33933_rp    * sin(10._rp * pi_boileau * t/T_period - 0.27471_rp)
    f = f - 0.37914_rp    * sin(12._rp * pi_boileau * t/T_period - 1.0557_rp)
    f = f + 0.22396_rp    * sin(14._rp * pi_boileau * t/T_period + 1.22_rp)
    f = f + 0.1507_rp     * sin(16._rp * pi_boileau * t/T_period + 1.0984_rp)
    f = f + 0.18735_rp    * sin(18._rp * pi_boileau * t/T_period + 0.067483_rp)
    f = f + 0.038625_rp   * sin(20._rp * pi_boileau * t/T_period + 0.22262_rp)
    f = f + 0.012643_rp   * sin(22._rp * pi_boileau * t/T_period - 0.10093_rp)
    f = f - 0.0042453_rp  * sin(24._rp * pi_boileau * t/T_period - 1.1044_rp)
    f = f - 0.012781_rp   * sin(26._rp * pi_boileau * t/T_period - 1.3739_rp)
    f = f + 0.014805_rp   * sin(28._rp * pi_boileau * t/T_period + 1.2797_rp)
    f = f + 0.012249_rp   * sin(30._rp * pi_boileau * t/T_period + 0.80827_rp)
    f = f + 0.0076502_rp  * sin(32._rp * pi_boileau * t/T_period + 0.40757_rp)
    f = f + 0.0030692_rp  * sin(34._rp * pi_boileau * t/T_period + 0.195_rp)
    f = f - 0.0012271_rp  * sin(36._rp * pi_boileau * t/T_period - 1.1371_rp)
    f = f - 0.0042581_rp  * sin(38._rp * pi_boileau * t/T_period - 0.92102_rp)
    f = f - 0.0069785_rp  * sin(40._rp * pi_boileau * t/T_period - 1.2364_rp)
    f = f + 0.0085652_rp  * sin(42._rp * pi_boileau * t/T_period + 1.4539_rp)
    f = f + 0.0081881_rp  * sin(44._rp * pi_boileau * t/T_period + 0.89599_rp)
    f = f + 0.0056549_rp  * sin(46._rp * pi_boileau * t/T_period + 0.17623_rp)
    f = f + 0.0026358_rp  * sin(48._rp * pi_boileau * t/T_period - 1.3003_rp)
    f = f - 0.0050868_rp  * sin(50._rp * pi_boileau * t/T_period - 0.011056_rp)
    f = f - 0.0085829_rp  * sin(52._rp * pi_boileau * t/T_period - 0.86463_rp)

  end function


  !> Computes flow-rate prescribed at inlet at each instant in time t as done for the
  !! Upper Thoracic Aorta case Boileau et al. 2012. The inlet function is a superposition
  !! of sinusoidal functions.
  function aorticInletFlowBoileau(t) result(f)

    real(rp), intent(in) :: t           !< current time
    real(rp)             :: T_period    !< base time period inlet flow-rate function
    real(rp)             :: pi_boileau  !< pi
    real(rp)             :: f           !< flow-rate prescribed at inlet at each instant in time t

    ! values of constants used in paper
    T_period   = 0.9550_rp   ! by default
    ! T_period   = 0.9000_rp   ! to have minimum flow at t=0
    pi_boileau = 3.141592653589793_rp

    ! compute flow-rate at time t
    f = 0.20617_rp
    f = f + 0.37759000_rp * sin(2.0_rp * pi_boileau * t/T_period + 0.5960500_rp)
    f = f + 0.28040000_rp * sin(4.0_rp * pi_boileau * t/T_period - 0.3585900_rp)
    f = f + 0.15337000_rp * sin(6.0_rp * pi_boileau * t/T_period - 1.2509000_rp)
    f = f - 0.04988900_rp * sin(8.0_rp * pi_boileau * t/T_period + 1.3921000_rp)
    f = f + 0.03810700_rp * sin(10._rp * pi_boileau * t/T_period - 1.1068000_rp)
    f = f - 0.04169900_rp * sin(12._rp * pi_boileau * t/T_period + 1.3985000_rp)
    f = f - 0.02075400_rp * sin(14._rp * pi_boileau * t/T_period + 0.7292100_rp)
    f = f + 0.01336700_rp * sin(16._rp * pi_boileau * t/T_period - 1.5394000_rp)
    f = f - 0.02198300_rp * sin(18._rp * pi_boileau * t/T_period + 0.9561700_rp)
    f = f - 0.01307200_rp * sin(20._rp * pi_boileau * t/T_period - 0.0224170_rp)
    f = f + 0.00370280_rp * sin(22._rp * pi_boileau * t/T_period - 1.4146000_rp)
    f = f - 0.01397300_rp * sin(24._rp * pi_boileau * t/T_period + 0.7741600_rp)
    f = f - 0.01242300_rp * sin(26._rp * pi_boileau * t/T_period - 0.4651100_rp)
    f = f + 0.00400980_rp * sin(28._rp * pi_boileau * t/T_period + 0.9514500_rp)
    f = f - 0.00597040_rp * sin(30._rp * pi_boileau * t/T_period + 0.8636900_rp)
    f = f - 0.00734390_rp * sin(32._rp * pi_boileau * t/T_period - 0.6476900_rp)
    f = f + 0.00370060_rp * sin(34._rp * pi_boileau * t/T_period + 0.7466300_rp)
    f = f - 0.00320690_rp * sin(36._rp * pi_boileau * t/T_period + 0.8592600_rp)
    f = f - 0.00481710_rp * sin(38._rp * pi_boileau * t/T_period - 1.0306000_rp)
    f = f + 0.00404030_rp * sin(40._rp * pi_boileau * t/T_period + 0.2800900_rp)
    f = f - 0.00324090_rp * sin(42._rp * pi_boileau * t/T_period + 1.2020000_rp)
    f = f - 0.00325170_rp * sin(44._rp * pi_boileau * t/T_period - 0.9331600_rp)
    f = f + 0.00291120_rp * sin(46._rp * pi_boileau * t/T_period + 0.2140500_rp)
    f = f - 0.00227080_rp * sin(48._rp * pi_boileau * t/T_period + 1.1869000_rp)
    f = f - 0.00215660_rp * sin(50._rp * pi_boileau * t/T_period - 1.1574000_rp)
    f = f + 0.00255110_rp * sin(52._rp * pi_boileau * t/T_period - 0.1291500_rp)
    f = f - 0.00244480_rp * sin(54._rp * pi_boileau * t/T_period + 1.1185000_rp)
    f = f - 0.00190320_rp * sin(56._rp * pi_boileau * t/T_period - 0.9924400_rp)
    f = f + 0.00194760_rp * sin(58._rp * pi_boileau * t/T_period - 0.0598850_rp)
    f = f - 0.00194770_rp * sin(60._rp * pi_boileau * t/T_period + 1.1655000_rp)
    f = f - 0.00145450_rp * sin(62._rp * pi_boileau * t/T_period - 0.8582900_rp)
    f = f + 0.00139790_rp * sin(64._rp * pi_boileau * t/T_period + 0.0429120_rp)
    f = f - 0.00143050_rp * sin(66._rp * pi_boileau * t/T_period + 1.2439000_rp)
    f = f - 0.00107750_rp * sin(68._rp * pi_boileau * t/T_period - 0.7946400_rp)
    f = f + 0.00103680_rp * sin(70._rp * pi_boileau * t/T_period - 0.0043058_rp)
    f = f - 0.00121620_rp * sin(72._rp * pi_boileau * t/T_period + 1.2110000_rp)
    f = f - 0.00095707_rp * sin(74._rp * pi_boileau * t/T_period - 0.6620300_rp)
    f = f + 0.00077733_rp * sin(76._rp * pi_boileau * t/T_period + 0.2564200_rp)
    f = f - 0.00092407_rp * sin(78._rp * pi_boileau * t/T_period + 1.3954000_rp)
    f = f - 0.00079585_rp * sin(80._rp * pi_boileau * t/T_period - 0.4997300_rp)
    f = 500._rp * f

 end function


  !> Computes flow-rate prescribed at inlet at each instant in time t as done for the
  !! Aortic Bifurcation case Boileau et al. 2012. The inlet function is a superposition
  !! of sinusoidal functions.
  function abdominalAorticInletFlowBoileau(t) result(f)

    real(rp), intent(in) :: t           !< current time
    real(rp)             :: T_period    !< base time period inlet flow-rate function
    real(rp)             :: pi_boileau  !< pi
    real(rp)             :: f           !< flow-rate prescribed at inlet at each instant in time t

    ! values of constants used in paper
    T_period   = 1.1_rp   ! by default
    pi_boileau = 3.141592653589793_rp

    ! compute flow-rate at time t
    f = 7.9853e-06_rp
    f = f+2.6617e-05_rp*sin(2.0_rp*pi_boileau*t/T_period+0.29498_rp)
    f = f+2.3616e-05_rp*sin(4.0_rp*pi_boileau*t/T_period-1.14030_rp)
    f = f-1.9016e-05_rp*sin(6.0_rp*pi_boileau*t/T_period+0.40435_rp)
    f = f-8.5899e-06_rp*sin(8.0_rp*pi_boileau*t/T_period-1.18920_rp)
    f = f-2.4360e-06_rp*sin(10._rp*pi_boileau*t/T_period-1.49180_rp)
    f = f+1.4905e-06_rp*sin(12._rp*pi_boileau*t/T_period+1.05360_rp)
    f = f+1.3581e-06_rp*sin(14._rp*pi_boileau*t/T_period-0.47666_rp)
    f = f-6.3031e-07_rp*sin(16._rp*pi_boileau*t/T_period+0.93768_rp)
    f = f-4.5335e-07_rp*sin(18._rp*pi_boileau*t/T_period-0.79472_rp)
    f = f-4.5184e-07_rp*sin(20._rp*pi_boileau*t/T_period-1.40950_rp)
    f = f-5.6583e-07_rp*sin(22._rp*pi_boileau*t/T_period-1.36290_rp)
    f = f+4.9522e-07_rp*sin(24._rp*pi_boileau*t/T_period+0.52495_rp)
    f = f+1.3049e-07_rp*sin(26._rp*pi_boileau*t/T_period-0.97261_rp)
    f = f-4.1072e-08_rp*sin(28._rp*pi_boileau*t/T_period-0.15685_rp)
    f = f-2.4182e-07_rp*sin(30._rp*pi_boileau*t/T_period-1.40520_rp)
    f = f-6.6217e-08_rp*sin(32._rp*pi_boileau*t/T_period-1.37850_rp)
    f = f-1.5511e-07_rp*sin(34._rp*pi_boileau*t/T_period-1.29270_rp)
    f = f+2.2149e-07_rp*sin(36._rp*pi_boileau*t/T_period+0.68178_rp)
    f = f+6.7621e-08_rp*sin(38._rp*pi_boileau*t/T_period-0.98825_rp)
    f = f+1.0973e-07_rp*sin(40._rp*pi_boileau*t/T_period+1.43270_rp)
    f = f-2.5559e-08_rp*sin(42._rp*pi_boileau*t/T_period-1.23720_rp)
    f = f-3.5079e-08_rp*sin(44._rp*pi_boileau*t/T_period+0.23280_rp)
    f = 1.0E+06_rp*f

 end function


  !> Computes flow-rate prescribed at inlet at each instant in time t as done for the
  !! 37-Artery Network case Boileau et al. 2012. The inlet function is a superposition
  !! of sinusoidal functions.
  function Arteries37InletFlowBoileau(t) result(f)

    real(rp), intent(in) :: t           !< current time
    real(rp)             :: T_period    !< base time period inlet flow-rate function
    real(rp)             :: pi_boileau  !< pi
    real(rp)             :: f           !< flow-rate prescribed at inlet at each instant in time t

    ! values of constants used in paper
    T_period   = 0.827_rp   ! by default
    pi_boileau = 3.141592653589793_rp

    ! compute flow-rate at time t
    f = 3.1199_rp
    f = f + 7.7982_rp*sin(2.0_rp*pi_boileau*t/T_period+0.5769_rp)
    f = f + 4.1228_rp*sin(4.0_rp*pi_boileau*t/T_period-0.8738_rp)
    f = f - 1.0611_rp*sin(6.0_rp*pi_boileau*t/T_period+0.7240_rp)
    f = f + 0.7605_rp*sin(8.0_rp*pi_boileau*t/T_period-0.6387_rp)
    f = f - 0.9148_rp*sin(10._rp*pi_boileau*t/T_period+1.1598_rp)
    f = f + 0.4924_rp*sin(12._rp*pi_boileau*t/T_period-1.0905_rp)
    f = f - 0.5580_rp*sin(14._rp*pi_boileau*t/T_period+1.0420_rp)
    f = f + 0.3280_rp*sin(16._rp*pi_boileau*t/T_period-0.5570_rp)
    f = f - 0.3941_rp*sin(18._rp*pi_boileau*t/T_period+1.2685_rp)
    f = f + 0.2833_rp*sin(20._rp*pi_boileau*t/T_period+0.6702_rp)
    f = f + 0.2272_rp*sin(22._rp*pi_boileau*t/T_period-1.4983_rp)
    f = f + 0.2249_rp*sin(24._rp*pi_boileau*t/T_period+0.9924_rp)
    f = f + 0.2589_rp*sin(26._rp*pi_boileau*t/T_period-1.5616_rp)
    f = f - 0.1460_rp*sin(28._rp*pi_boileau*t/T_period-1.3106_rp)
    f = f + 0.2141_rp*sin(30._rp*pi_boileau*t/T_period-1.1306_rp)
    f = f - 0.1253_rp*sin(32._rp*pi_boileau*t/T_period+0.1552_rp)
    f = f + 0.1321_rp*sin(34._rp*pi_boileau*t/T_period-1.5595_rp)
    f = f - 0.1399_rp*sin(36._rp*pi_boileau*t/T_period+0.4223_rp)
    f = f - 0.0324_rp*sin(38._rp*pi_boileau*t/T_period+0.7811_rp)
    f = f - 0.1211_rp*sin(40._rp*pi_boileau*t/T_period+1.0729_rp)
    f = f * 1.0e+02_rp/6._rp

  end function


! Linear interpolation of external function tabulated values
function linearExternal(t, fInd) result(f)

    real(rp),     intent(in)      :: t
    integer(ip),  intent(in)      :: fInd
    integer(ip)                   :: nP, seg
    real(rp)                      :: T_period, tInt, t1, t2, f1, f2
    real(rp)                      :: f

    nP = extFList(fInd)%numP                        ! total number of interpolation points
    T_period = extFList(fInd)%tList(nP)             ! Period = last time value on the list

    tInt = t - int( t / T_period ) * T_period       ! get value of t to interpolate

    seg = 1                ! find the segment in which to interpolate [1 .. nP-1]
    do while ( extFList(fInd)%tList(seg + 1) .lt. tInt )
        seg = seg + 1
    end do
    t1 = extFList(fInd)%tList(seg)
    t2 = extFList(fInd)%tList(seg+1)
    f1 = extFList(fInd)%fList(seg)
    f2 = extFList(fInd)%fList(seg+1)

    f = (f2 - f1)/(t2 - t1) * (tInt - t1) + f1

end function linearExternal



!----------------------------------------------------------------------------------------------
!
!   External boundary condition scalar calculation
!       bcI = 0 -> Not external scalar BC has been defined at this external CP
!       scType = 1  -> scalar value imposed following a defined function
!           scFunc = 0  -> Constant 0 value
!           scFunc = 1  -> Constant value C read in the conf file real parameters ( C )
!           scFunc = 2  -> dumped senoidal for validation check with parameters ( Amp , Freq , Diff )
!       scType = 2  -> scalar flux imposed [TODO]
function scAtBoundary(bcI, ad, t) result(f)

    integer(ip),                  intent(in)          :: bcI
    real(rp),                     intent(in)          :: t
    real(rp)                                          :: f
    integer(ip)                                       :: ad
    integer(ip)                                       :: scType, scFunc
    real(rp)                                          :: off, amp, Lp, vel, diff

    if (bcI .eq. 0) then                    ! Not scalar BC defined
        scType = 1                          ! Assign a fixed value of 0 for the scalar
        scFunc = 0
    else
        scType = scBCList%type(ad, bcI)     ! Get the type of BC to be applied from conf values
        scFunc = scBCList%fType(ad, bcI)    ! Get the function to be applied from conf values
    end if

    f = 0.0_rp
    select case(scType)             ! Type of BC
    case(1)                         ! Value of scalar at boundary
        select case(scFunc)         ! Function to aply
        case(0)                     ! Constant 0 value
            f = 0.0_rp
        case(1)                     ! Constant value
            f = scBCList%rpList(ad,bcI)%r(1)
        case(2)                     ! dumped senoidal for validation purposes
            ! phi(0,t) = off - amp * exp(-diff * 4 * pi^2 / Lp^2 * t) * cos( - 2 * pi * u / Lp * t)
            off     = scBCList%rpList(ad,bcI)%r(1)
            amp     = scBCList%rpList(ad,bcI)%r(2)
            diff    = gVar%scalKd(ad)
            Lp      = scBCList%rpList(ad,bcI)%r(3)
            vel     = scBCList%rpList(ad,bcI)%r(4)
            f       = off - amp * exp( - diff * 4.0_rp * pi**2 * t / Lp**2) * cos(- 2.0_rp * pi * vel * t / Lp)
        end select
    !case(2)                        ! [TODO]: Value of transported flux at boundary
    case default
        write(fIdx%logfile,*)
        write(fIdx%logfile,*) "ERROR --- Scalar boundary condition Type not implemented: ", scType
        stop
    end select

end function scAtBoundary
!----------------------------------------------------------------------------------------------

end module boundcon
