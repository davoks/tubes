module mesher

    use io
    use nrtype
    use physfuncs

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Fetch elements and nodes on segments from Domain File with the information
!   or create them by parsing the geometry and configuration files
!   in this case, create the Domain File
subroutine getMesh(dt)

    real(rp), intent(in), optional            :: dt
    logical                                   :: isDomFile

    ! Writing of Domain file is not yet completed.
    inquire(FILE=domnFileName, EXIST=isDomFile)
    isDomFile = .false.
    if (isDomFile) then
        ! read mesh and problem data from domain file
        write(fIdx%logfile, *)
        write(fIdx%logfile, *) "--- Getting problem data from Domain File ---"
        call readDomFile()
        call confSummary()
        call geomSummary()
    else
        write(fIdx%logfile, *)
        write(fIdx%logfile, *) "--- Domain File not found ---"
        ! Parse Configuration and geometry files (from io.f90)
        call parseConfFile()
        if( present(dt) ) call forceTimeStep(dt)
        call confSummary()
        call parseGeomFile()
        call geomSummary()
        ! Generate element and node mesh
        call createMesh()
        ! fill witness points geometry-derived parameters
        call locateWPs()
        ! Write Domain File with all problem information
        call writeDomFile()
    end if

end subroutine getMesh
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Generate FE mesh structure.
!   It runs 3 times over the segment list read from the geom.txt and performs the following at each:
!       1 -
subroutine createMesh()

    integer(ip)                               :: i, j, seg, el
    integer(ip)                               :: cp1, cp2
    integer(ip)                               :: el1, el2
    integer(ip)                               :: nd1, nd2
    integer(ip)                               :: nIndx, inod
    real(rp), dimension(3)                    :: p1, p2
    integer(ip)                               :: el_seg
    integer(ip)                               :: TotElem, TotNodes

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Generating Mesh ---"

    ! Allocate number of cp dependent arrays with information on incoming and outgoing segments
    allocate( cpList%inN   ( cpList%num ) )             ! Number of segments IN and OUT of the CP
    allocate( cpList%outN  ( cpList%num ) )
    allocate( cpList%inSeg ( cpList%num ) )             ! List of index for IN and OUT segments
    allocate( cpList%outSeg( cpList%num ) )
    allocate( cpList%nodeL ( cpList%num ) )             ! Allocate array for lists of nodes at each CP
    allocate( cpList%bcNum ( cpList%num ) )             ! Allocate array for boundary conditions type

    allocate( segList%Length   ( segList%num ) )        ! Segment lenghts array
    allocate( segList%dx       ( segList%num ) )        ! Segment discretization size array
    allocate( segList%elemLimit( 2, segList%num ) )     ! Segment limiting elements array

    TotElem = 0                         ! initialize total number of elements

    ! first pass over segments:
    ! - Define number of incoming/ougoing segments at each CP
    ! - Get lenght of segments
    ! - Calculate number of elements per segment (get total number of elements) and
    !   obtain discretization element length. Limiting elements also stored.
    cpList%inN(:) = 0                   ! Initialization of number of segments at cp
    cpList%outN(:) = 0
    do i = 1, segList%num
        ! Get length of every segment with check for minimum length allowed
        cp1 = segList%cpLimit(1, i)                     ! limiting control points
        cp2 = segList%cpLimit(2, i)

        cpList%inN(cp2) = cpList%inN(cp2) + 1           ! increase number of incoming segments and
        cpList%outN(cp1) = cpList%outN(cp1) + 1         !       number of outgoing segments

        p1 = cpList%coords(:, cp1)                      ! coords of limiting cp
        p2 = cpList%coords(:, cp2)

        segList%Length(i) = dist(p1, p2)                ! euclidean distance
        !if (abs(segList%Length(i)) .lt. 1.0e-3) then    ! minimum length check: 0.001 cm
        !    print*, "---ERROR: Segment length too small: ", i, segList%Length(i)
        !    stop
        !end if

        ! Set first element index for the current segment
        segList%elemLimit(1, i) = TotElem + 1

        ! Obtain number of elements in segment based in discretization mode selection...
        select case(gVar%discMode)
        case(0)         ! ...by max element size (hMax)
            el_seg = ceiling(segList%Length(i) / gVar%hMax)
            ! overwrite number of elements to comply with hMax restriction
            if ( el_seg .gt. segList%numElem(i) ) then
                segList%numElem(i) = el_seg
            end if
        case(1)         ! ...by set number of elements
            ! keep value of numElem as read from geometry file --> do nothing
        case default    ! Raise error if option is not recognized
            print*, "---ERROR: Not recognized discretization mode: ", gVar%discMode
            stop
        end select

        segList%dx(i) = segList%Length(i) / segList%numElem(i)      ! compute discretization lenght
        TotElem = TotElem + segList%numElem(i)                      ! update total number of elements
        segList%elemLimit(2, i) = TotElem                           ! Set last element index for the current segment

    end do

    elemList%num = TotElem                          ! update total number of elements in the whole network
    allocate( elemList%seg( TotElem ) )             ! allocate array of index for segments containing element
    allocate( elemList%nodLimit( 2, TotElem ) )     ! allocate matrix of limiting nodes for each element

    do i = 1, cpList%num
        allocate( cpList%inSeg(i)%i( cpList%inN(i) ) )     ! allocate number of incoming and...
        allocate( cpList%outSeg(i)%i( cpList%outN(i) ) )   !    ...outgoing segments at cps
        ! allocate space for indexes of nodes at each cp
        allocate( cpList%nodeL(i)%i( cpList%inN(i) + cpList%outN(i) ) )
    end do

    cpList%inN(:) = 0                   ! Use these arrays as counters now
    cpList%outN(:) = 0

    TotNodes = 0                        ! initialize total number of nodes

    ! Second pass over segments:
    ! - Store indexes of segments that go into / out of each control point
    ! - Store segment index that contains each element in the element list
    ! - Store the limiting nodes indexes for each element in element list.
    !   Obtain the total number of nodes.
    do i = 1, segList%num
        cp1 = segList%cpLimit(1, i)                 ! Get limiting CP indexes
        cp2 = segList%cpLimit(2, i)                 !       (segment goes from cp1 to cp2)

        cpList%outN(cp1) = cpList%outN(cp1) + 1     ! Advance arrays indexes counters
        cpList%inN(cp2) = cpList%inN(cp2) +1

        cpList%outSeg(cp1)%i(cpList%outN(cp1)) = i  ! store index of segment at CP array structure
        cpList%inSeg(cp2)%i(cpList%inN(cp2)) = i

        el1 = segList%elemLimit(1, i)               ! Get limiting elements for segment i
        el2 = segList%elemLimit(2, i)

        nIndx = TotNodes + 1                        ! Disjoint segments: not common nodes between them
        do j = el1, el2
            elemList%seg(j) = i                     ! store indexes of segments containing each element
            ! Store limiting nodes indexes for each element
            elemList%nodLimit(1, j) = nIndx
            nIndx = nIndx + gVar%n_el - 1           ! Elements inside segment share limiting nodes
            elemList%nodLimit(2, j) = nIndx
        end do

        ! compute total of nodes after taking into account the number of elements in segment i
        TotNodes = TotNodes + segList%numElem(i) * (gVar%n_el - 1) + 1
    end do

    ! Create ordered list of nodes on control points first from incoming segments and next from outgoing
    do i = 1, cpList%num
        do j = 1, cpList%inN(i)             ! process nodes at end of segment entering cp
            seg = cpList%inSeg(i)%i(j)
            el = segList%elemLimit(2, seg)
            cpList%nodeL(i)%i(j) = elemList%nodLimit(2, el)
        end do                              ! process nodes at end of segment exiting cp
        do j = cpList%inN(i) + 1, cpList%inN(i) + cpList%outN(i)
            seg = cpList%outSeg(i)%i(j - cpList%inN(i))
            el = segList%elemLimit(1, seg)
            cpList%nodeL(i)%i(j) = elemList%nodLimit(1, el)
        end do
    end do

    nodeList%num = TotNodes                         ! update total number of nodes
    allocate( nodeList%seg( TotNodes ) )            ! allocate array of indexes for segments that own each node
    allocate( nodeList%linX( TotNodes ) )           ! allocate array of linear location on segment

    ! third pass over segments:
    ! - store the segment index that contains each node in the node list
    ! - store the linear position of each node inside the segment [0...Lseg]
    do i = 1, segList%num
        el1 = segList%elemLimit(1, i)               ! get limit elements for segment
        el2 = segList%elemLimit(2, i)
        nd1 = elemList%nodLimit(1, el1)             ! get limit nodes for segment
        nd2 = elemList%nodLimit(2, el2)
        nodeList%seg(nd1:nd2) = i                   ! set segment number for nodes in it
        ! Calculate linear position of node [inod] between [nd1] => 0 and [nd2] => Lseg
        do inod = nd1, nd2
            nodeList%linX(inod) = real(inod - nd1) / real(nd2 - nd1) * segList%Length(i)
        end do
    end do

    write(fIdx%logfile, *) "---> DONE"
    write(fIdx%logfile, *)

    write(fIdx%logfile, *) "Number of elements:          ", elemList%num
    write(fIdx%logfile, *) "Number of nodes:             ", nodeList%num
    write(fIdx%logfile, *) "Number of total dofs:        ", gVar%dof * nodeList%num

end subroutine createMesh
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Generate FE mesh structure
subroutine locateWPs()

    integer(ip)                                       :: wp, seg, e1, e2, cp1, cp2

    allocate( wpList%elem(    wpList%num ) )
    allocate( wpList%linXWP(  wpList%num ) )
    allocate( wpList%coord3D( 3, wpList%num ) )

    do wp = 1, wpList%num
        seg = wpList%seg(wp)                ! Get index of segment containing [wp]
        e1 = segList%elemLimit(1, seg)      ! Get limiting elements for segment
        e2 = segList%elemLimit(2, seg)

        ! Obtain element used to calculate wp values (wp inside or in a frontier node)
        wpList%elem(wp) = min(e1 + int(wpList%fract(wp) * (e2 + 1 - e1)), e2)

        ! Get linear location of wp in segment as fraction of segment lenght
        wpList%linXWP(wp) = wpList%fract(wp) * segList%Length(seg)

        cp1 = segList%cpLimit(1, seg)       ! Get control points for the segment containing
        cp2 = segList%cpLimit(2, seg)       !       the witness point [wp]

        ! Assign 3D coordinates as interpolated from control points coordinates
        wpList%coord3D(:, wp) = cpList%coords(:, cp1) + wpList%fract(wp) * &
                & (cpList%coords(:, cp2) - cpList%coords(:, cp1))

    end do

end subroutine locateWPs
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Set ad-hoc timestep size (optional)
subroutine forceTimeStep(dt)
    real(rp), intent(in) :: dt
    if (dt > 0.0_rp) then
        write(fIdx % logfile,*) 'forcing timestep to be ',dt,' instead of ',gVar%dt
        gVar % dt = dt
    end if
end subroutine
!----------------------------------------------------------------------------------------------


end module mesher
