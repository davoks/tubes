module femodule

    use nrtype
    use clases
    use physfuncs

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Initialize Finite elements related fixed parameters
subroutine initFE()

    integer(ip)                                   :: i

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Creating iso element matrices ---"
    call initIsoMatrices(gVar%n_el)
    write(fIdx%logfile, *) "---> DONE"
    write(fIdx%logfile, *)

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Creating function shape derivative matrix ---"
    call initDFMatrix(gVar%n_el)
    write(fIdx%logfile, *) "---> DONE"

    call printIsoMatrices()

    write(fIdx%logfile, *) "--- Assign element matrices to segments ---"
    allocate( segList%elMat( segList%num ) )
    do i = 1, segList%num
        call initSegMatrices(i)
        write(fIdx%logfile, *) "Segment: ", i
    end do
    write(fIdx%logfile, *) "---> DONE"



end subroutine initFE
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Print Iso-elemental matrices
subroutine printIsoMatrices()

    write(fIdx%logfile, *) "[N N] :"
    call printMat(isoMat%NN)
    write(fIdx%logfile, *)

    write(fIdx%logfile, *) "[N dN] :"
    call printMat(isoMat%NdN)
    write(fIdx%logfile, *)

    write(fIdx%logfile, *) "[dN dN] :"
    call printMat(isoMat%dNdN)
    write(fIdx%logfile, *)

end subroutine printIsoMatrices
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Print matrix
subroutine printMat(M)

    integer(ip)                                       :: i
    real(rp),     dimension(:,:),     intent(in)      :: M

    do i = 1, size(M, 1)
        write(fIdx%logfile, *) "    ", M(i,:)
    end do

end subroutine printMat
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Returns the value of the nfunc-th shape function evaluated at x in [-1, 1] from a set of nnodes shape functions
!   that defines the discrete function space based on the number of nodes per element (nnodes)
function shapeF(nnodes, nfunc, x) result(prod)

    integer(ip)   , intent(in)                :: nnodes
    integer(ip)   , intent(in)                :: nfunc
    real(rp)      , intent(in)                :: x
    real(rp)      , dimension(nnodes)         :: roots
    real(rp)                                  :: prod
    integer(ip)                               :: i

    ! Array with locations where shape functions equal 0 (at nnodes-1 points) or 1 (1 point).
    ! Discretization of [-1, 1] in n_elem points
    roots = (/( -1.0_rp + (2.0_rp *(i - 1) ) / (nnodes - 1) , i = 1, nnodes)/)

    ! Obtain shape function value at location x as product of n_elem-1 elementary contribution from the rest of roots
    prod = 1.0_rp
    do i = 1, (nfunc - 1)
        prod = prod * (x - roots(i)) / (roots(nfunc) - roots(i))
    end do
    do i = (nfunc + 1), nnodes
        prod = prod * (x - roots(i)) / (roots(nfunc) - roots(i))
    end do

end function shapeF
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Returns the value of the derivative of the nfunc-th shape function evaluated at x in [-1, 1] from a set of nnodes
!   shape functions that defines the discrete function space based on the number of nodes per element (nnodes)
function dShapeF_dx(nnodes, nfunc, x) result(sumat)

    integer(ip)   , intent(in)                :: nnodes
    integer(ip)   , intent(in)                :: nfunc
    real(rp)      , intent(in)                :: x
    real(rp)      , dimension(nnodes)         :: roots
    real(rp)      , dimension(nnodes)         :: prod_array
    real(rp)                                  :: prod_den
    real(rp)                                  :: sumat
    integer(ip)                               :: i, j

    ! Array with locations where shape functions equal 0 (at nnodes-1 points) or 1 (1 point).
    ! Discretization of [-1, 1] in nnodes points
    roots = (/( -1.0_rp + (2.0_rp *(i - 1) ) / (nnodes - 1) , i = 1, nnodes)/)

    ! Obtain derivative of shape function value at location x as the sumatory of the product of n_elem-1 contribution from the
    ! rest of the roots
    prod_array = 1.0_rp
    prod_array(nfunc) = 0.0_rp
    prod_den = 1.0_rp
    do i = 1, (nfunc - 1)
        do j = 1, i-1
            prod_array(j) = prod_array(j) * (x - roots(i))
        end do
        do j = i+1, nnodes
            prod_array(j) = prod_array(j) * (x - roots(i))
        end do
        prod_den = prod_den * (roots(nfunc) - roots(i))
    end do
    do i = (nfunc + 1), nnodes
        do j = 1, i-1
            prod_array(j) = prod_array(j) * (x - roots(i))
        end do
        do j = i+1, nnodes
            prod_array(j) = prod_array(j) * (x - roots(i))
        end do
        prod_den = prod_den * (roots(nfunc) - roots(i))
    end do
    sumat = sum(prod_array) / prod_den

end function dShapeF_dx
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Initialize a matrix with values of derivatives of i-th function shape (columns) evaluated at
!   the location of the j-th node.
subroutine initDFMatrix(nnodes)

    integer(ip),  intent(in)                  :: nnodes
    real(rp)                                  :: x
    integer(ip)                               :: i, j

    allocate( isoMat%dfij( nnodes, nnodes ) )

    do j = 1, nnodes
        x = -1.0_rp + 2.0_rp * real(j - 1) / real(nnodes - 1)
        do i = 1, nnodes
            isoMat%dfij(i, j) = dShapeF_dx(nnodes, i, x)
        end do
    end do

end subroutine initDFMatrix
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Initializes the elemental matrices in iso coordinate space based on the selected element
!   order n_el (nodes per element)
subroutine initIsoMatrices(n_el)

    integer(ip),  intent(in)                  :: n_el
    integer(ip)                               :: pq0, pq1, pq2      ! number of quadrature points needed
    integer(ip)                               :: i, j

    allocate( isoMat%NN( n_el, n_el ) )         ! allocate iso-elemental matrices size
    allocate( isoMat%NdN( n_el, n_el ) )
    allocate( isoMat%dNN( n_el, n_el ) )
    allocate( isoMat%dNdN( n_el, n_el ) )

    pq0 = n_el
    pq1 = n_el - 1
    pq2 = n_el - 1

    do i = 1, n_el
        do j = 1, n_el
            isoMat%NN(j, i) = integralGQ(pq0, g0)
            isoMat%NdN(j, i) = integralGQ(pq1, g1)
            isoMat%dNN(i, j) = isoMat%NdN(j, i)
            isoMat%dNdN(j, i) = integralGQ(pq2, g2)
        end do
    end do

contains
! wrapper functions to use the IntegralGQ() for different matrix elements and
! matrix types
function g0(x)              ! product of shape functions Nj(x) * Ni(x)
    real(rp), intent(in)          :: x
    real(rp)                      :: g0
    g0 = shapeF(n_el, j, x)*shapeF(n_el, i, x)
end function
function g1(x)              ! product of shape functions Nj(x) * dNi(x)
    real(rp), intent(in)          :: x
    real(rp)                      :: g1
    g1 = shapeF(n_el, j, x)*dShapeF_dx(n_el, i, x)
end function
function g2(x)              ! product of shape functions dNj(x) * dNi(x)
    real(rp), intent(in)          :: x
    real(rp)                      :: g2
    g2 = dShapeF_dx(n_el, j, x)*dShapeF_dx(n_el, i, x)
end function

end subroutine initIsoMatrices
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Initializes the elemental matrices in x coordinate space based on the selected segment dx
subroutine initSegMatrices(seg)

    integer(ip),  intent(in)                  :: seg
    integer(ip)                               :: nEl
    real(rp)                                  :: Jac

    nEl = gVar%n_el

    Jac = segList%dx(seg) * 0.5_rp

    allocate( segList%elMat(seg)%NN( nEl, nEl ) )
    allocate( segList%elMat(seg)%NdN( nEl, nEl ) )
    allocate( segList%elMat(seg)%dNN( nEl, nEl ) )
    allocate( segList%elMat(seg)%dNdN( nEl, nEl ) )
    allocate( segList%elMat(seg)%dfij( nEl, nEl ) )

    segList%elMat(seg)%NN = isoMat%NN * Jac
    segList%elMat(seg)%NdN = isoMat%NdN
    segList%elMat(seg)%dNN = isoMat%dNN
    segList%elMat(seg)%dNdN = isoMat%dNdN / Jac
    segList%elMat(seg)%dfij = isoMat%dfij / Jac

end subroutine initSegMatrices
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Multiply a vector {v} by a matrix [M] composed of the combination of elemental matrices
!   over a segment.         {w} = [M] . {v}
subroutine multEleMatVec(M, v, w)

    real(rp),     dimension(:,:),     intent(in)      :: v
    real(rp),     dimension(:,:),     intent(in)      :: M
    real(rp),     dimension(:,:),     intent(out)     :: w
    integer(ip)                                       :: i, j, k, nNod, n_el, nVar

    nVar = size(v, 1)
    nNod = size(v, 2)
    n_el = size(M,1)

    w = 0.0_rp
    ! Perform M·v for each variable in order
    do k = 1, nVar
        do i = 1, (nNod - 1), (n_el - 1)
            do j = 1, n_el
                w(k,i:i+n_el-1) = w(k,i:i+n_el-1) + M(:,j) * v(k,i+j-1)
            end do
        end do
    end do

end subroutine multEleMatVec
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Return the derivative of the Dyastolic Area in the segment iseg
!
subroutine deriveDyArea(iseg)

    integer(ip),  intent(in)                          :: iseg
    integer(ip)                                       :: e1, e2, n1, n2
    integer(ip)                                       :: e
    real(rp),     dimension(gVar%n_el)                :: modSt, modMid, modEnd, dA

    ! create modifiers for common nodes contribution
    modSt = 1.0_rp                                      ! First element modifier
    modSt(gVar%n_el) = 0.5_rp
    modMid = 1.0_rp                                     ! Middle element modifier
    modMid(1) = 0.5_rp
    modMid(gVar%n_el) = 0.5_rp
    modEnd = 1.0_rp                                     ! Last element modifier
    modEnd(1) = 0.5_rp

    e1 = segList%elemLimit(1, iseg)                     ! Get limiting elements for the segment
    e2 = segList%elemLimit(2, iseg)
    n1 = elemList%nodLimit(1, e1)                       ! Get initial node of first element
    ! Initialize derivative of dyastolic area on the segment
    nodeList%dA_dx( n1 : elemList%nodLimit(2, e2) ) = 0.0_rp
    n2 = n1 + gVar%n_el - 1                             !   and last node of first element

    call getDDyA()                                      ! Returns dA with the derivative of dyastolic area
    ! derivative of dyastolic area is assembled modified on the common nodes
    nodeList%dA_dx(n1:n2) = nodeList%dA_dx(n1:n2) + modSt * dA

    do e = e1+1, e2-1                                   ! run over inner elements in segment
        n1 = n2                                         ! Get limiting nodes of current element
        n2 = n1 + gVar%n_el - 1
        call getDDyA()
        nodeList%dA_dx(n1:n2) = nodeList%dA_dx(n1:n2) + modMid * dA
    end do

    n1 = n2                                             ! Get limiting nodes of current element
    n2 = n1 + gVar%n_el - 1
    call getDDyA()
    nodeList%dA_dx(n1:n2) = nodeList%dA_dx(n1:n2) + modEnd * dA

contains
subroutine getDDyA()
    integer(ip)                                       :: i, j
    dA = 0.0_rp
    do i = 1, gVar%n_el
        do j = 1, gVar%n_el
            dA(i) = dA(i) + nodeList%dyArea(n1+j-1) * segList%elMat(iseg)%dfij(j, i)
        end do
    end do
end subroutine getDDyA

end subroutine deriveDyArea
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Return the derivative of the advected scalars in the segment iseg
subroutine deriveScalar(iseg)

    integer(ip),  intent(in)                          :: iseg
    integer(ip)                                       :: e, e1, e2, n1, n2
    real(rp),     dimension(gVar%nadv, gVar%n_el)     :: modSt, modMid, modEnd, dsc

    ! create modifier vectors for common nodes contribution
    modSt = 1.0_rp
    modSt(:,gVar%n_el) = 0.5_rp
    modMid = 1.0_rp
    modMid(:,1) = 0.5_rp
    modMid(:, gVar%n_el) = 0.5_rp
    modEnd = 1.0_rp
    modEnd(:,1) = 0.5_rp

    e1 = segList%elemLimit(1, iseg)                     ! Get limiting elements for the segment
    e2 = segList%elemLimit(2, iseg)
    n1 = elemList%nodLimit(1, e1)                       ! Get limiting nodes of first element
    nodeList%dSc_dx( :, n1 : elemList%nodLimit(2, e2) ) = 0.0_rp
    n2 = n1 + gVar%n_el - 1
    call getDsc()
    nodeList%dSc_dx(:,n1:n2) = nodeList%dSc_dx(:,n1:n2) + modSt * dsc

    do e = e1+1, e2-1                                   ! run over inner elements in segment
        n1 = n2                                         ! Get limiting nodes of current element
        n2 = n1 + gVar%n_el - 1
        call getDsc()
        nodeList%dSc_dx(:,n1:n2) = nodeList%dSc_dx(:,n1:n2) + modMid * dsc
    end do

    n1 = n2                                             ! Get limiting nodes of current element
    n2 = n1 + gVar%n_el - 1
    call getDsc()
    nodeList%dSc_dx(:,n1:n2) = nodeList%dSc_dx(:,n1:n2) + modEnd * dsc

contains
subroutine getDsc()
    integer(ip)                                       :: i, j
    do i = 1, gVar%n_el
        dsc(:,i) = 0.0_rp
        do j = 1, gVar%n_el
            dsc(:,i) = dsc(:,i) + nodeList%scal(:,n1+j-1) * segList%elMat(iseg)%dfij(j, i)
        end do
    end do
end subroutine getDsc

end subroutine deriveScalar
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Return array of [nN] points with the values of the derivative of Beta in the segment [is]
subroutine deriveBeta(iseg)

    integer(ip),  intent(in)                          :: iseg
    integer(ip)                                       :: e1, e2, e, n1, n2
    real(rp),     dimension(gVar%n_el)                :: modSt, modMid, modEnd, dB

    ! create modifier vectors for common nodes contribution
    modSt = 1.0_rp
    modSt(gVar%n_el) = 0.5_rp
    modMid = 1.0_rp
    modMid(1) = 0.5_rp
    modMid(gVar%n_el) = 0.5_rp
    modEnd = 1.0_rp
    modEnd(1) = 0.5_rp

    e1 = segList%elemLimit(1, iseg)                     ! Get limiting elements for the segment
    e2 = segList%elemLimit(2, iseg)
    n1 = elemList%nodLimit(1, e1)                       ! Get limiting nodes of first element
    nodeList%dB_dx( n1 : elemList%nodLimit(2, e2) ) = 0.0_rp
    n2 = n1 + gVar%n_el - 1
    ! Obtain the derivative of Beta at each element level of the segment
    call getDB()
    nodeList%dB_dx(n1:n2) = nodeList%dB_dx(n1:n2) + modSt * dB

    do e = e1+1, e2-1                                   ! run over inner elements in segment
        n1 = n2                                         ! Get limiting nodes of current element
        n2 = n1 + gVar%n_el - 1
        call getDB()
        nodeList%dB_dx(n1:n2) = nodeList%dB_dx(n1:n2) + modMid * dB
    end do

    n1 = n2                                             ! Get limiting nodes of current element
    n2 = n1 + gVar%n_el - 1
    call getDB()
    nodeList%dB_dx(n1:n2) = nodeList%dB_dx(n1:n2) + modEnd * dB

contains
subroutine getDB()
    integer(ip)                                       :: i, j
    do i = 1, gVar%n_el
        dB(i) = 0.0_rp
        do j = 1, gVar%n_el
            dB(i) = dB(i) + nodeList%beta(n1+j-1) * segList%elMat(iseg)%dfij(j, i)
        end do
    end do
end subroutine getDB

end subroutine deriveBeta
!----------------------------------------------------------------------------------------------

end module femodule
