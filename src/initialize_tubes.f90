subroutine initialize_tubes(dt)

    use nrtype
    use clases
    use mesher
    use inicon
    use checks 
    use femodule
    use galtub
    use io
    use omp_lib

    implicit none

    real(rp),             intent(in) :: dt
    integer(ip)                      :: iseg, iwit

    !
    ! Generate mesh
    !
    call getMesh(dt)

    !
    ! Build finite element arrays
    !
    call initFE()

    !
    ! Set initial conditions
    !
    call setIniConds()

    !
    ! Check mesh definition 
    !
    call checkMesh()

    !
    ! Write initial state to file
    !
    call writeDump(.true.)

    !
    ! Allocate domain structures
    !
    allocate( dom( segList%num ) )

    !
    ! Initialize omp thread
    !
    call omp_set_num_threads( min( segList%num, omp_get_num_procs() ) )
    !$OMP parallel default(firstprivate), &
    !$OMP & shared( bcList,         icList,     scICList,   scSrcList,&
    !$OMP &         WallMatList,    FlPar,      isoMat,     cpList,&
    !$OMP &         segList,        elemList,   nodeList,   dom,        gVar)

    !
    ! Gather domains
    !
    !$OMP do schedule(dynamic)
    do iseg = 1, segList%num
        call gatherDomain(iseg)
    end do
    !$OMP end do

    !
    ! Write witness points of initial state to file
    !
    !$OMP do schedule(dynamic)
    do iwit = 1, wpList%num
        call openWPFile(iwit, .true.)
        call writeWP(iwit)
    end do
    !$OMP end do

    !$OMP end parallel

end subroutine initialize_tubes
