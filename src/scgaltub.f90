module scgaltub

    use nrtype
    use clases
    use femodule

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Computations to be performed each time step to advance the simulation to the next one.
!   1st order FW Euler or different orders of RK are used to advance the values of the variables
!   to the next step.
!
!   System to solve (unknowns):
!       {dU_dt} + {dF_dx} = {B}
!       {th}·[NN]·{dU_dt} + {th}·[NdN]·{F} = {th}·[NN]·{B}
!
!   System to solve (advected scalars):
!       dsc_dt + u · dsc_dx - d(kd · dsc_dx)_dx = S
!       {th}·[NN]·{dsc_dt} = [th·kd·dsc_dx]|b - [th*u*sc]|b + {th}·(-[dNdN]·{kd·sc} + {u}*[dNN]·{sc} + [NN]·{S})
subroutine compAllD_Dt(is, u, du_dt, sc, dsc_dt)

    integer(ip),                                 intent(in)  :: is
    real(rp),     dimension(:,:),                intent(in)  :: u
    real(rp),     dimension(:,:),                intent(in)  :: sc
    real(rp), dimension(gVar%dof, dom(is)%nod),  intent(out) :: du_dt
    real(rp), dimension(gVar%nadv,dom(is)%nod),  intent(out) :: dsc_dt
    real(rp),     dimension(gVar%dof, dom(is)%nod)           :: conv, stab, uSrc, F
    real(rp),     dimension(gVar%nadv,dom(is)%nod)           :: scSrc, NNscSrc, scA, scDiff
    real(rp),     dimension(gVar%nadv,dom(is)%nod)           :: scStab, scFluxBound, scAdv, modscRHS
    real(rp),     dimension(gVar%nadv, dom(is)%nod)          :: modLNN
    real(rp),     dimension(gVar%dof, gVar%dof, dom(is)%nod) :: H
    integer(ip)                                              :: i, ad

    call deriveScalar(is)               ! Update values of dSc_dx in node list

    ! Unknowns: {B} == uSrc     {F} --> [NdN]·{F} = conv      {H} --> stab
    ! Scalars:  {S} == scSrc    [th·kd·dsc_dx]|b - [th*u*sc]|b == {th}*scFluxBound
    ! [TODO]: Include scalar diffusion boundary flux computation here {scFluxBound}

    call getAllVectors(is, u, uSrc, F, H, sc, scFluxBound, scSrc)

    ! Int{th(x) · dF(x)} = [th(x) · F(x)]_{bound} - Int{dth(x) · F(x)} ~ - {th} · [dNN] · {F}
    call multEleMatVec(segList%elMat(is)%NdN, F, conv)      ! [dNN] · {F} = conv

    ! {1/A}·[dNdN]·{kd·A·sc} == scDiff
    do i = 1, dom(is)%nod
        scA(:,i) = gVar%scalKd(:) * sc(:,i) * u(1,i)        ! Diffusion coefficient product by components
    end do
    call multEleMatVec(segList%elMat(is)%dNdN, scA, scDiff)
    call multEleMatVec(segList%elmat(is)%dNN, sc, scAdv)    ! {u}*[dNN]·{sc} == scAdv
    do i = 1, dom(is)%nod
        scDiff(:,i) = scDiff(:,i) / u(1,i)                  ! Inverse of area factor
        scAdv(:,i) = (u(2,i) / u(1,i)) * scAdv(:,i)         ! Advection velocity product by components
    end do

    ! Get stabilization terms
    call getStab(is, uSrc, F, H, stab, scSrc, scStab)

    ! {lumpedNN} ~ [NN]
    ! [NN]·{dU_dt} = - [NdN]·{F} - {stab} + [NN]·{B}
    du_dt = - (conv + stab ) / dom(is)%lumpedNN + uSrc

    ! [NN]·{dsc_dt} = - {scFluxBound} - {1/A}·[dNdN]·{kd·A·sc} + {u}*[dNN]·{sc} - {scStab} + [NN]·{scSrc}
    !   {scFluxBound} = [u·sc - kd·sc_x]
    call multEleMatVec(segList%elmat(is)%NN, scSrc, NNscSrc)        ! [NN]·{scSrc} == NNscSrc

    ! Modification of RHS and lumped mass matrix depending on Neumann or Dirichlet conditions
    ! possibly modified RHS and lumped mass matrix vector
    modscRHS = - scFluxBound - scDiff + scAdv - scStab + NNscSrc
    modLNN = dom(is)%scLumpNN
    do ad = 1, gVar%nadv
        if (dom(is)%scBCType(ad, 1) .eq. 0) then        ! Dirichlet on segment start
            modscRHS(ad,1) = sc(ad,1)                   ! Impossed in the Dirichlet BC
            modLNN(ad,1) = 1.0_rp                       ! Mass matrix has a single 1 in that column
            ! Dirichlet -> zeroes on columns of mass matrix and RHS needs to be modified
            do i = 2, gVar%n_el
                ! RHS vector is corrected to reflect the presence of zeroes in the mass matrix 1st column
                modscRHS(ad,i) = modscRHS(ad,i) - sc(ad,1) * segList%elMat(is)%NN(i, 1)
                ! The sum of the row is corrected as a 0 is needed on 1st column
                modLNN(ad,i) = modLNN(ad,i) - segList%elMat(is)%NN(i, 1)
            end do
        end if
        if (dom(is)%scBCType(ad,2) .eq. 0) then             ! Dirichlet on segment end
            modscRHS(ad,dom(is)%nod) = sc(ad,dom(is)%nod)   ! Impossed value in the Dirichlet BC
            modLNN(ad,dom(is)%nod) = 1.0_rp                 ! Mass matrix has a single 1 in that column
            do i = 1, gVar%n_el - 1
                ! RHS and mass matrix vector modifications by zeroes in the last column
                modscRHS(ad,dom(is)%nod - gVar%n_el + i) = modscRHS(ad,dom(is)%nod - gVar%n_el + i) - &
                                                          sc(ad,dom(is)%nod) * segList%elMat(is)%NN(i, gVar%n_el)
                modLNN(ad,dom(is)%nod - gVar%n_el + i) = modLNN(ad,dom(is)%nod - gVar%n_el + i) - segList%elMat(is)%NN(i, gVar%n_el)
            end do
        end if
    end do
    dsc_dt = modscRHS / modLNN
    ! G-S solve
    !call gsSolver()

contains
! TESTING PURPOSES
! Solve a system [NN] · {du} = {B} by LU decomposion in a Gauss - Seidel scheme
subroutine gsSolver()
    real(rp), dimension(gVar%dof, dom(is)%nod), target :: du_dt0, du_dt1
    real(rp), dimension(gVar%dof, dom(is)%nod)         :: invD, B, LUdu
    real(rp), dimension(gVar%dof)                      :: maxim
    real(rp), dimension(gVar%n_el, gVar%n_el)          :: LU
    integer(ip)                                        :: i, j, iter
    real(rp)                                           :: er
    real(rp), dimension(:,:), pointer                  :: duPrev, duNew
    logical                                            :: swap

    ! Get {B} = [NN] · {uSrc} - {conv} - {stab}
    call multEleMatVec(segList%elMat(is)%NN, uSrc, B)
    B = B - (conv + stab)
    ! Decompose [NN] matrix in [LU] and [D] == {invD}
    LU = segList%elMat(is)%NN
    do i = 1, gVar%n_el
        LU(i,i) = 0.0_rp
    end do
    invD = 0.0_rp
    do i = 1, dom(is)%nod - 1, gVar%n_el - 1
        do j = 1, gVar%n_el
            invD(:,i-1+j) = invD(:,i-1+j) + segList%elMat(is)%NN(j,j)
        end do
    end do

    ! Diagonal matrix invD is inverted component by component to solve system iteratively
    !   {du}^n = {invD} · ({B} - [LU] · {du}^(n-1))
    invD = 1.0_rp / invD

    er = 1.0_rp
    du_dt0 = 0.0_rp
    swap = .false.
    iter = 0
    do while (er .gt. 1.0e-2)
        if (iter .eq. 1000) then
            write(fIdx%logfile,*) "GS not converging"
            stop
        end if
        select case(swap)
        case (.false.)
            duPrev => du_dt0
            duNew  => du_dt1
        case (.true.)
            duPrev => du_dt1
            duNew  => du_dt0
        end select
        call multEleMatVec(LU, duPrev, LUdu)
        duNew = invD * (B - LUdu)
        maxim(:) = maxval( abs(duNew), dim=2 )
        er = maxval( maxval( abs( duNew - duPrev ), dim=2) / maxim )
        swap = .not. swap
        iter = iter + 1
    end do
    du_dt = duNew
end subroutine gsSolver

end subroutine compAllD_Dt
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get Vectors used to compute src (= B), conv (= dF/dx) and diff terms for unknowns
!   B   = {  0  ,  -kr/rho Q/A - dBeta/dx A/(Ad rho) (2/3 A^0.5 - Ad^0.5)
!                + Beta dAd/dx A/(rho Ad^2) (2/3 A^0.5 - 0.5 Ad^0.5)  }
!   F   = {  Q  ,  alpha Q^2/A + beta/(3 rho Ad) A^{3/2}  }
!   H   = [dF/du] = {                     0                               ,        1           }
!                   {  -alpha (Q/A)^2 + beta/(2 rho Ad) · A^{1/2}   ,   2 alpha Q/A  }
!   scDB =
!   Get vector used to compute scSr (= S)
!   [TODO] S   = - kdep · {sc}
subroutine getAllVectors(is, u, B, F, H, sc, scFB, S)

    integer(ip),  intent(in)                          :: is
    real(rp),     dimension(:,:),     intent(in)      :: u, sc
    real(rp),     dimension(:,:),     intent(out)     :: B, F, S, scFB
    real(rp),     dimension(:,:,:),   intent(out)     :: H
    real(rp)                                          :: Tsc
    integer(ip)                                       :: i, j, n1, n2, cp

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

! Hardcoded timescale value for scalar deposition
    Tsc = 1.0_rp

    j = 1           ! Local nodal indexing counter
    do i = n1, n2   ! Global indices
        ! B vector
        B(1,j) = 0.0_rp
        B(2,j) = -FlPar%kr / FlPar%rho * u(2,j) / u(1,j) &
               & - nodeList%dB_dx(i) * u(1,j) / (FlPar%rho * nodeList%dyArea(i)) * &
               &   (2.0_rp / 3.0_rp * sqrt(u(1,j)) - sqrt(nodeList%dyArea(i))) &
               & + nodeList%beta(i) * nodeList%dA_dx(i) * u(1,j) / &
               &   (FlPar%rho * nodeList%dyArea(i)**2.0_rp) * (2.0_rp/3.0_rp * &
               &   sqrt(u(1,j)) - 0.5_rp * sqrt(nodeList%dyArea(i)))
        ! F vector
        F(1,j) = u(2,j)
        F(2,j) = FlPar%alpha * u(2,j)**2 / u(1,j) &
               & + nodeList%beta(i) / (3.0_rp * FlPar%rho * nodeList%dyArea(i)) * &
               &   u(1,j)**1.5_rp
        ! Matrix components
        H(1,1,j) = 0.0_rp
        H(2,1,j) = -FlPar%alpha * (u(2,j) / u(1,j))**2.0_rp + &
                 & nodeList%beta(i) / (2.0_rp * FlPar%rho * nodeList%dyArea(i)) * &
                 & u(1,j)**0.5_rp
        H(1,2,j) = 1.0_rp
        H(2,2,j) = 2.0_rp * FlPar%alpha * u(2,j) / u(1,j)

        ! Scalar source components (and scalar deposition update)
        ! [TODO]:   currently hardcoded proportionality value
        S = 0.0_rp
        ! S(:,j) = - sc(:,j) / Tsc
        !nodeList%depos(:,i) = nodeList%depos(:,i) + u(1,j) * segList%dx(is) * &
        !                    & sc(:,j) * (1.0_rp - NAPIER**(-gVar%dt / Tsc))
        nodeList%depos(:,i) = 0.0_rp
        ! Update local indexing counter
        j = j + 1
    end do

    ! [th*u*sc]|b - [th·kd·dsc_dx]|b== {th}*scFluxBound
    ! The initial point in the integration domain gets a -1 sign
    scFB = 0.0_rp
    ! start of segment -> scFlux is the average flux per segment that has entered the CP, segment exits the CP. F = scFlux
    ! impossed condition -> the entering average flux exits the CP equally at each node
    cp = segList%cpLimit(1, is)
    scFB(:,1) = - cpList%scFlux(:, cp)  ! Total flux is calculated as (+) on final node and (-) on initial node
    ! end of segment -> scFlux is the average flux per segment that has entered the CP, segment enters the CP. F = - scFlux
    ! impossed condition -> the entering average flux exits the CP equally at each node
    cp = segList%cpLimit(2, is)
    scFB(:,dom(is)%nod) = - cpList%scFlux(:, cp)    ! Total flux is calculated as (+) on final node and (-) on initial node

end subroutine getAllVectors
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get diffusive (stabilization) term for the system to solve
!   R = [tau]·([H]x([dNN]·({Ut}-{B})+[dNdN]·{F}))
!   Rsc = {tauSc}*{u}*([dNN]·({Sct}-{scS})+[dNdN]·({u}*{Sc}-kd*{Sc}_x))
subroutine getStab(is, B, F, H, R, scS, scR)

    integer(ip),  intent(in)                          :: is
    real(rp),     dimension(:,:),     intent(in)      :: B, F
    real(rp),     dimension(:,:,:),   intent(in)      :: H
    real(rp),     dimension(:,:),     intent(in)      :: scS
    real(rp),     dimension(gVar%dof, dom(is)%nod)    :: tmp1, tmp2, tmp3
    real(rp),     dimension(gVar%nadv,dom(is)%nod)    :: scTmp1, scTmp2, scTmp3, scTmp4, tmpVect1, tmpVect2
    real(rp),     dimension(:,:),   intent(out)       :: R, scR
    real(rp),     dimension(dom(is)%nod)              :: tau, v
    real(rp),     dimension(gVar%nadv, dom(is)%nod)   :: tauSc
    integer(ip)                                       :: i, n1, n2
    integer(ip)                                       :: t1, t2, rate

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

    v(:) = nodeList%u(2,n1:n2) / nodeList%u(1,n1:n2)    ! precalculate velocity field

call system_clock(t1, rate)

    call getTau_sc(is, tau, tauSc)      ! Obtain stabilization timescale value

call system_clock(t2, rate)
gTime%part8 = gTime%part8 + real(t2-t1,8)/real(rate,8)

call system_clock(t1, rate)

    ! tmp1 = [dNdN] · {F}
    ! tmp2 = [dNN] · {Ut}
    ! tmp3 = [dNN] · {B}
    call multEleMatVec(segList%elMat(is)%dNdN, F, tmp1)
    call multEleMatVec(segList%elMat(is)%dNN, nodeList%du(:,n1:n2), tmp2)
    call multEleMatVec(segList%elMat(is)%dNN, B, tmp3)

    ! scTmp1 = {Sct - scSrc}
    ! scTmp2 = [dNN] · {Sct - scSrc} = [dNN] · scTmp1
    ! scTmp3 = [dNdN]·{u}*{Sc}
    ! scTmp4 = 1/A*[dNdN]·{-kd*A*{Sc}_x}
    do i = 1, dom(is)%nod
        scTmp1(: , i) = nodeList%dscal(: , n1-1+i) - scS(: , i)
    end do
    call multEleMatVec(segList%elMat(is)%dNN, scTmp1, scTmp2)
    do i = 1, dom(is)%nod
        tmpVect1(: , i) = v(i) * nodeList%scal(: , n1-1+i)
        tmpVect2(: , i) = - gVar%scalKd(:) * nodeList%u(1 , n1-1+i) * nodeList%dSc_dx(: , n1-1+i)
    end do
    call multEleMatVec(segList%elMat(is)%dNdN, tmpVect1, scTmp3)
    call multEleMatVec(segList%elMat(is)%dNdN, tmpVect2, scTmp4)
    do i = 1, dom(is)%nod
        scTmp4(: , i) = scTmp4(: , i) / nodeList%u(1 , n1-1+i)
    end do

call system_clock(t2, rate)
gTime%part10 = gTime%part10 + real(t2-t1,8)/real(rate,8)
call system_clock(t1, rate)

    ! Use tau stabilization only for the Q variable, not for A (over dissipation)
    !   -> Set first component of first array to 0
    do i = 1, dom(is)%nod
        R(:,i) = [ 0.0_rp , tau(i) * ( H(2,1,i)*(tmp1(1,i)+tmp2(1,i)-tmp3(1,i)) &
                                   & + H(2,2,i)*(tmp1(2,i)+tmp2(2,i)-tmp3(2,i)) ) ]
        scR(:,i) = v(i) * tauSc(:,i) * (scTmp2(:,i) + scTmp3(:,i) + scTmp4(:,i))
    end do

call system_clock(t2, rate)
gTime%part11 = gTime%part11 + real(t2-t1,8)/real(rate,8)

end subroutine getStab
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get Tau value for each node in segment [is]
subroutine getTau_sc(is, tau, tauSc)

    integer(ip),      intent(in)                      :: is
    real(rp),         dimension(:),   intent(out)     :: tau
    real(rp),         dimension(:,:), intent(out)     :: tauSc
    integer(ip)                                       :: i, j, k, n1, n2
    real(rp)                                          :: lamb1, lamb2, lamb, invtau
    real(rp),         dimension(gVar%nadv)            :: invtauSc

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

    tau = 0.0_rp
    tauSc = 0.0_rp
    j = 1                       ! Local arrays node counter
    do i = n1, n2               ! Global arrays node counter
        lamb1 = eigenvals(nodeList%u(1,i), nodeList%u(2,i), nodeList%beta(i), &
              & nodeList%dyArea(i), 1)
        lamb2 = eigenvals(nodeList%u(1,i), nodeList%u(2,i), nodeList%beta(i), &
              & nodeList%dyArea(i), 2)
        lamb = maxval( abs( [lamb1, lamb2] ) )
        invtau = 2.0_rp * lamb / segList%dx(is) + FlPar%kr / nodeList%u(1,i)
        invtauSc(:) = 2.0_rp * abs(nodeList%u(2,i) / nodeList%u(1,i)) / segList%dx(is) &
                  & + 4.0_rp * gVar%scalKd(:) / segList%dx(is)**2
        if (invtau .gt. 1.0e-6) then
            tau(j) = gVar%asupg / invtau
        end if
        do k = 1, gVar%nadv
            if ( invtauSc(k) .gt. 1.0e-6) then
                tauSc(k,j) = gVar%asupg / invtauSc(k)
            end if
        end do
        j = j + 1
    end do

end subroutine getTau_sc
!----------------------------------------------------------------------------------------------

end module scgaltub
