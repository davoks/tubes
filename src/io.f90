! Subroutines used in the module
!   parseConfFile():    Parse the contents of file conf.txt in the same directory as the executable
!   confSummary():      Print on screen summary of parsed configuration file
!   parseGeomFile():    Parse the contents of file geom.txt in the same directory as the exectuable
!                           and assign the values to global variables cpList, segList and bcList
!                           storing data related to control points, segments of network and external
!                           boundary conditions
!   geomSummary():      Print on screen summary of parsed geommetry file
!   writeDomFile():     Write Domain File with all the problem information and elements/nodes tables
!   readDomFile():      Read problem geometry and configuration data from stored Domain File
!   writeDump():        Write information on each segment at call time
!                       - Param:    overwrite   -> delete previous run files
!   writeWP():          Write information on each witness point at call time
!                       - Param:    firstWP     -> create files and headers
!                                   overwrite   -> delete previous run files

module io

    use nrtype
    use clases
    use femodule

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
! Parse the contents of file conf.txt in the same directory as the exectuable
subroutine parseConfFile()

    integer(ip)         :: pos, idx, i, j, k, j1, j2, m, n        ! auxiliar variables
    real(rp)            :: r1
    integer(ip)         :: idummy                   ! used to skip read parameters
    real(rp)            :: rdummy
    integer(ip)         :: fh
    integer(ip)         :: ios = 0                  ! return value for file read io operation
    integer(ip)         :: iosE = 0                 ! return value for ext function file read io operation
    integer(ip)         :: fhE
    character(len=64)   :: extFuncFN=''             ! name of file with time tabulated function values
    character(len=256)  :: buffer, label
    integer(ip)         :: frev

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Parsing Configuration File ---"

    fh  = fIdx%configuration                        ! configuration file identifier
    fhE = fIdx%extFunc                              ! external function file identifier

    open(fh, file=confFileName)                     ! open file for reading

    gVar%nadv = -1                                  ! Flag to check for correct order in file

    frev = 0                                        ! Initialization of default file version for checking

    ! ios is negative if an end of record condition is encountered or if
    ! an endfile condition was detected.  It is positive if an error was
    ! detected.  ios is zero otherwise.
    do while (ios == 0)
        read(fh, '(A)', iostat=ios) buffer          ! get line from the file
        if (ios /= 0) exit                          ! avoid evalution if error is detected

        ! Split label and data using rightmost '#' character .
        pos = scan(buffer, '#', .true.)             ! .true. => rightmost appearance
        label = buffer(1:pos)
        buffer = buffer(pos+1:)

        ! Check revision of config file (has to be first line of the file). 0 for version not set
        if (label .eq. '#REV#') read(buffer, *) frev
        if (frev .ne. gVar%confRev) then
            print*, '---ERROR: Wrong config file version. Need version: ', gVar%confRev
            stop
        end if

        ! read parameters from config file depending on label
        select case (label)

        ! Read finite elements general related parameters
        !           gVar%dof:   degrees of freedom to solve
        !           gVar%nadv:  number of advected scalars
        !           gVar%n_el:  number of nodes per element
        !           gVar%stab:  stabilization scheme
        case ('#FEparam#')
            read(buffer, *) gVar%dof, gVar%nadv, gVar%n_el, gVar%stab

        ! Read time stepping general related parameters
        !           gVar%dt:    time step value
        !           gVar%nTime: Total number of time steps
        !           gVar%ordT:  Order of time steping scheme
        case ('#tStep#')
            read(buffer, *) gVar%dt, rdummy, r1, rdummy,  idummy, gVar%ordT, rdummy
            gVar%nTime = int(r1)

        ! Read solution dumping general related parameters
        !           gVar%ntspd:     Number of time steps per dump
        !           gVar%outPath:   Path to outuput directory
        case ('#SolDump#')
            read(buffer, *) r1, rdummy, idummy, idummy, gVar%outPath
            gVar%ntspd = int(r1)

        ! Read initial conditions parameters
        !           icList%num:     number of initial conditions
        !           icList%ipList:  list of arrays with integer parameters
        !           icList%rpList:  list of arrays with real parameters
        case ('#initU#')
            read(buffer, *) icList%num              ! Get number of initial conditions
            allocate( icList%ipList( icLIst%num ))  ! list of integer parameters arrays...
            allocate( icList%rpList( icList%num ))  !... and list of real parameters arrays
            do i = 1, icList%num
                read(fh, '(A)', iostat=ios) buffer
                read(buffer, *) idx, j1, j2
                allocate( icList%ipList(idx)%i(j1) )
                allocate( icList%rplist(idx)%r(j2) )
                read(buffer, *) (idummy, n = 1, 3), (icList%ipList(idx)%i(n), n = 1, j1), &
                    & (icList%rpList(idx)%r(n), n = 1, j2)
            end do

        ! Get Boundary conditions data from geometry file
        !           Look on allocation comments for details
        case('#BCdef#')
            read(buffer, *) bcList%num              ! Get the number of boundary conditions specified

            n = bcList%num
            allocate( bcList%cp(n) )                ! list with control point indexes
            allocate( bcList%type(n) )              ! list with type of boundary conditions for each one
            allocate( bcList%extF(n) )              ! list with indices for external functions (0 if no external file)
            allocate( bcList%fType(n) )             ! list with type function applied to the bc for each one
            allocate( bcList%nIntP(n) )             ! list with number of integers parameters used at this bc
            allocate( bcList%nRealP(n) )            ! list with number of real parameters used at this bc
            allocate( bcList%ipList(n)  )           ! list of pointers to integer arrays for integer parameters...
            allocate( bcList%rpList(n) )            ! ... and real arrays for real parameters

            ! read the boundary conditions
            do i = 1, bcList%num
                read(fh, '(A)', iostat=ios) buffer          ! Update buffered line (no need to remove tag on those)
                ! read only the first integer parameters (the last 2 read are used for allocation of memory)
                !   the first parameter is used as consistency check for the number of BC
                read(buffer, *) idummy, bcList%cp(i), bcList%type(i), bcList%extF(i), bcList%fType(i),&
                                & bcList%nIntP(i), bcList%nRealP(i)
                if ( idummy .ne. i) then
                    print*, '---ERROR: Index for boundary condition out of order: ', idummy
                    stop
                end if
                ! allocate dimension of integer and real arrays being pointed at using the last 2 previous integers
                allocate( bcList%ipList(i)%i(bcList%nIntP(i)) )
                allocate( bcList%rpList(i)%r(bcList%nRealP(i)) )
                ! Discard already read integer variables and store new parameters in dimensioned arrays
                read(buffer,*) (idummy, j = 1, 7), (bcList%ipList(i)%i(j), j = 1, bcList%nIntP(i)), &
                            & (bcList%rpList(i)%r(j), j = 1, bcList%nRealP(i))
            end do

        ! Read scalar variables initial conditions parameters
        !           scICList%num:       number of initial conditions
        !           scICList%ipList:    list of arrays with integer parameters
        !           scICList%rpList:    list of arrays with real parameters
        case('#scalIC#')
            read(buffer, *) scICList%num                ! Get number of initial conditions
            allocate( scICList%ipList( scICList%num ))  ! list of integer parameters arrays...
            allocate( scICList%rpList( scICList%num ))  ! ... and list of real parameters arrays
            do i = 1, scICList%num
                read(fh, '(A)', iostat=ios) buffer
                read(buffer, *) idx, j1, j2
                allocate( scICList%ipList(idx)%i(j1) )
                allocate( scICList%rplist(idx)%r(j2) )
                read(buffer, *) (idummy, n = 1, 3), (scICList%ipList(idx)%i(n), n = 1, j1), &
                    & (scICList%rpList(idx)%r(n), n = 1, j2)
            end do

        ! Get Scalar boundary condition data from geometry file
        !      See allocation comments for details
        case('#SCBCdef#')
            read(buffer, *) scBCList%num            ! get number of scalar boundary definitions
            ! Check for file order consistency
            if (gVar%nadv .eq. -1) then
                print*, '---ERROR: Bad ordered config file. Define first the number of scalar variables'
                stop
            end if

            n = scBCList%num
            allocate( scBCList%type  (gVar%nadv, n) )   ! list with type of boundary conditions for each one
            allocate( scBCList%fType (gVar%nadv, n) )   ! list with type function applied to the bc for each one
            allocate( scBCList%nRealP(gVar%nadv, n) )   ! list with number of real parameters used at this bc
            allocate( scBCList%rpList(gVar%nadv, n) )   ! ... and real arrays for real parameters

            ! read the scalar boundary conditions into data structure
            do i = 1, scBCList%num
                read(fh, '(A)', iostat=ios) buffer          ! Update buffered line (no need to remove tag on those)
                ! first integer parameter is sc bc definition index (1 to "num"), not stored because convention is to have
                !   bc ordered sequentially from 1 [not verification is implemented for this yet]
                ! next, in groups of Nadv are the type of scalar conditions used
                ! next, in groups of Nadv are the type fo functions used
                ! next, in groups of Nadv are the numbers of parameters used by each function
                read(buffer, *) idummy, (scBCList%type(j, i), j = 1, gVar%nadv), &
                            & (scBCList%fType(j, i), j = 1, gVar%nadv), &
                            & (scBCList%nRealP(j, i), j = 1, gVar%nadv)
                if ( idummy .ne. i) then            ! Consistency check on number of scalar boundary conditions defined
                    print*, '---ERROR: Index for scalar boundary condition out of order: ', idummy
                    stop
                end if
                ! allocate dimension of real arrays being pointed at using the last integers
                do j = 1, gVar%nadv
                    allocate( scBCList%rpList(j, i)%r(scBCList%nRealP(j, i)) )
                end do
                ! Discard already read integer variables and store new parameters in dimensioned arrays
                read(buffer,*) (idummy, k = 1, 1+3*gVar%nadv), &
                            & ((scBCList%rpList(j, i)%r(k), k = 1, scBCList%nRealP(j, i)), j = 1, gVar%nadv)
            end do

        ! Read scalar difussion coeficients
        case ('#scDif#')
            allocate( gVar%scalKd( gVar%nadv ) )
            read(buffer, *) (gVar%scalKd(n), n = 1, gVar%nadv)

        ! Get injection points (source) data from geometry file
        case('#SCsrc#')
            read(buffer, *) scSrcList%num

            n = scSrcList%num
            allocate( scSrcList%seg(n) )            ! list with segment indexes where injection is applied
            allocate( scSrcList%loc(n) )            ! list with fraction of segment where injection happens
            allocate( scSrcList%nIPar(n) )          ! number of integer parameters
            allocate( scSrcList%nRPar(n) )          ! number of real parameters
            allocate( scSrcList%ipList(n) )         ! list with arrays of integer paramenters
            allocate( scSrcList%rpList(n) )         ! list with arrays of real parameters

            do i = 1, scSrcList%num
                read(fh, '(A)', iostat=ios) buffer          ! Update buffered line (no need to remove tag on those)
                ! read the first parameters
                read(buffer, *) idummy, scSrcList%seg(i), scSrcList%loc(i), scSrcList%nIPar(i), scSrcList%nRPar(i)
                if ( idummy .ne. i) then            ! Consistency check on number of scalar boundary conditions defined
                    print*, '---ERROR: Index for scalar boundary condition out of order: ', idummy
                    stop
                end if
                ! allocate dimensions of integer and real arrays
                allocate( scSrcList%ipList(i)%i( scSrcList%nIPar(i) ) )
                allocate( scSrcList%rpList(i)%r( scSrcList%nRPar(i) ) )
                read(buffer, *) (idummy, j = 1, 2), rdummy, (idummy, j = 1, 2), &
                            & (scSrcList%ipList(i)%i(j), j = 1, scSrcList%nIPar(i) ), &
                            & (scSrcList%rpList(i)%r(j), j = 1, scSrcList%nRPar(i) )
            end do

        ! Read numerical method related parameters
        !           gVar%asupg:     SUPG alpha stabilization parameter
        case ('#NumPar#')
            read(buffer, *) j1, j2, (idummy, n = 1, j1), gVar%asupg

        ! Read Fluid material parameters
        !           FlPar%rho:      fluid density
        !           FlPar%nu:       fluid viscosity
        !           FlPar%alpha:    momentum-correction parameter
        !           FlPar%kvisc:    viscoelasticity parameter
        !           FlPar%prext:    external pressure
        !           FlPar%zprof:    flow profile constant
        case ('#FlMat#')
            read(buffer, *) (idummy, n = 1, 2), FlPar%rho, FlPar%nu, FlPar%alpha, FlPar%kvisc, &
                    & FlPar%Pext, FlPar%zprof
            FlPar%kr = 2.0_rp * (FlPar%zprof + 2.0_rp) * pi * FlPar%nu



        ! Read Wall material parameters
        !           WallMatList(...)%rParam(1): Young's modulus
        !           WallMatList(...)%rParam(2): Poisson's ratio
        !           WallMatList(...)%rParam(3): Dyastolic pressure
        case ('#WallMat#')
            read(buffer, *) n                       ! Get number of wall materials specified
            allocate( WallMatList(n) )              ! Allocate materials
            do i = 1, n
                read(fh, '(A)', iostat=ios) buffer
                read(buffer, *) idx, j1, j2
                ! Allocate integer and real parameters
                allocate( WallMatList(idx)%iParam(j1) )
                allocate( WallMatList(idx)%rParam(j2) )
                read(buffer, *) (idummy, n = 1, 3), ( WallMatList(idx)%iParam(m) , m = 1, j1), &
                    & (WallMatList(idx)%rParam(m) , m = 1, j2)
            end do

        ! Read External functions list of points
        !           extFList(...)%numP:         Number of points used to define the external function
        !           extFList(...)%tList(...):   list of time values
        !           extFList(...)%fList(...):   list of function values
        case ('#ExtFunc#')
            read(buffer, *) n                       ! Number of files with external functions values
            gVar%nExtF = n
            if (n .ne. 0) allocate( extFList(n) )   ! Allocate external functions
            do i = 1, n
                read(fh, *, iostat=ios) extFuncFN
                open(fhE, file=trim(extFuncFN))
                m = 0                               ! use m to store the number of points in the file
                iosE = 0
                read(fhE, *, iostat=iosE)
                do while (iosE == 0)
                    m = m + 1
                    read(fhE, *, iostat=iosE)
                end do
                rewind(fhE)
                extFlist(i)%numP = m
                allocate( extFlist(i)%tList( m ) )
                allocate( extFlist(i)%fList( m ) )
                do j = 1, m
                    read(fhE, *, iostat=iosE) extFList(i)%tList(j), extFList(i)%fList(j)
                end do
                close(fhE)
            end do

        ! Read Witness Points parameters
        !           wpList%num:     Number of witness points
        !           wpList%seg:     index of segment containing the wp
        !           wpList%fract:   fraction of segment where wp is located
        case ('#WitPt#')
            read(buffer, *) wpList%num              ! Get number of witness points
            allocate( wpList%seg(   wpList%num ) )
            allocate( wpList%fract( wpList%num ) )
            ! Store list of segments with the witness points
            read(buffer, *) idummy, ( wpList%seg(i), i = 1, wpList%num)
            ! Advance to the next line on config file
            read(fh, '(A)', iostat=ios) buffer
            ! read fraction of segment where wp are located
            read(buffer, *) ( wpList%fract(i), i = 1, wpList%num)

        end select

    end do

    close(fh)

    write(fIdx%logfile, *) "---> DONE"
    write(fIdx%logfile, *)

end subroutine parseConfFile
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   show config file read variables
subroutine confSummary()

    integer(ip)                                       :: i

    ! FE parameters
    write(fIdx%logfile, *) "Number of dof per node:       ", gVar%dof
    write(fIdx%logfile, *) "Number of advected variables: ", gVar%nadv
    write(fIdx%logfile, *) "      Diffusion coefficients: ", (gVar%scalKd(i), i = 1, gVar%nadv)
    write(fIdx%logfile, *) "Number of nodes per element:  ", gVar%n_el
    select case (gVar%stab)
    case (0)
        write(fIdx%logfile, *) "Stabilization method:      NONE"
    case (1)
        write(fIdx%logfile, *) "Stabilization method:      SUPG"
        write(fIdx%logfile, *) "        alpha = ", gVar%asupg
    case (2)
        write(fIdx%logfile, *) "Stabilization method:      TG"
    end select
    write(fIdx%logfile, *)

    ! Time stepping parameters
    write(fIdx%logfile, *) "Time step value:              ", gVar%dt, " s"
    write(fIdx%logfile, *) "Number of time steps:         ", gVar%nTime
    select case (gVar%stab)
    case (1)
        write(fIdx%logfile, *) "Time stepping scheme:      Euler FW"
    case (2)
        write(fIdx%logfile, *) "Time stepping scheme:      RK 2"
    case (3)
        write(fIdx%logfile, *) "Time stepping scheme:      RK 3"
    case (4)
        write(fIdx%logfile, *) "Time stepping scheme:      RK 4"
    end select
    write(fIdx%logfile, *)

    ! Dumping parameters
    write(fIdx%logfile, *) "Dump interval time steps:     ", gVar%ntspd
    write(fIdx%logfile, *) "Output directory:             ", gVar%outPath
    write(fIdx%logfile, *)

    ! Initial conditions
    write(fIdx%logfile, *) "Number of intial conditions:  ", icList%num
    write(fIdx%logfile, *) "Number of scalar IC:          ", scICList%num
    write(fIdx%logfile, *)

    ! Fluid material parameters
    write(fIdx%logfile, *) "Density:                      ", FlPar%rho
    write(fIdx%logfile, *) "Kinematic viscosity:          ", FlPar%nu
    write(fIdx%logfile, *) "Momentum correction:          ", FlPar%alpha
    write(fIdx%logfile, *) "Viscoelasticity:              ", FlPar%kvisc
    write(fIdx%logfile, *) "External pressure:            ", FlPar%Pext
    write(fIdx%logfile, *) "Flow profile correction:      ", FlPar%zprof
    write(fIdx%logfile, *) "Friction ressistance coeff.:  ", FlPar%kr
    write(fIdx%logfile, *)

    ! Wall materials
    write(fIdx%logfile, *) "Number of wall materials:     ", size(WallMatList)
    write(fIdx%logfile, *) "Material:       Young M.    Poisson     Dy. Press."
    do i = 1, size(WallMatList)
        write(fIdx%logfile, *) i,": ", WallMatList(i)%rParam(1),WallMatList(i)%rParam(2), WallMatList(i)%rParam(3)
    end do
    write(fIdx%logfile, *)

    ! External functions
    write(fIdx%logfile, *) "Number of external functions: ", gVar%nExtF
    if (gVar%nExtF .ne. 0) then
        write(fIdx%logfile, *) "    External function:        ", (i, i = 1, gVar%nExtF)
        write(fIdx%logfile, *) "    Number of points:         ", ( size( extFList(i)%tList ), i = 1, gVar%nExtF)
    end if
    write(fIdx%logfile, *)

    ! Witness points
    write(fIdx%logfile, *) "Number of witness points:     ", wpList%num

end subroutine confSummary
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
! Parse the contents of file geom.txt in the same directory as the exectuable
subroutine parseGeomFile()

    integer(ip)                                       :: pos, idx, i, j, n        ! auxiliar variables
    integer(ip)                                       :: idummy                   ! used to skip read parameters
    integer(ip)                                       :: fh
    integer(ip)                                       :: ios = 0                  ! return value for file read io operation
    character(len=256)                                :: buffer, label
    integer(ip)                                       :: frev                     ! File revision

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Parsing Geometry File ---"

    fh = fIdx%geometry                              ! geometry file identifier

    open(fh, file=geomFileName)                     ! open file for reading

    cpList%num = -1                                 ! Flag values to check that file is correctly ordered
    segList%num = -1
    bcList%num = -1

    frev = 0                                        ! Initialization to file revision 0

    ! ios is negative if an end of record condition is encountered or if
    ! an endfile condition was detected.  It is positive if an error was
    ! detected.  ios is zero otherwise.
    do while (ios == 0)
        read(fh, '(A)', iostat=ios) buffer          ! get line from the file
        if (ios /= 0) exit                          ! avoid evalution if error is detected

        ! Split label and data using rightmost '#' character .
        pos = scan(buffer, '#', .true.)
        label = buffer(1:pos)
        buffer = buffer(pos+1:)

        ! Check revision of geometry file (has to be first line of the file). 0 for version not set
        if (label .eq. '#REV#') read(buffer, *) frev
        if (frev .ne. gVar%geomRev) then
            print*, '---ERROR: Wrong geometry file version. Need version: ', gVar%geomRev
            stop
        end if

        ! read parameters from config file depending on label
        select case (label)

            ! Get Tree structure dimension data
            !           cpList%num: number of control points
            !           segList%num: number of segments
            !           bcList%num:  number of external boundary conditions
            case ('#TreeDef#')
                read(buffer, *) cpList%num, segList%num, bcList%num

            ! Get discretization strategy related parameters
            !           discMode:   Discretization mode
            !           hMax:       Maximum element length
            case ('#Disc#')
                read(buffer, *) gVar%discMode, gVar%hMax

            ! Get cp list coordinates (with check for geometry file order)
            case('#cpdef#')
                if (cpList%num .eq. -1) then
                    print*, '---ERROR: Bad ordered geometry file. Define first number of control points'
                    stop
                end if

                ! allocate matrix array for 3 coordinates X, Y, Z in columns for as many control points as there are
                allocate( cpList%coords(3, cpList%num) )

                ! read lines with coordinates info for control points and store them in a matrix array
                do i = 1, cpList%num
                    read(fh, '(A)', iostat=ios) buffer
                    read(buffer, *) idx, (cpList%coords(j, idx), j = 1, 3)
                end do

            ! Get segments data from geometry file (with check for file order)
            case('#segdef#')
                if (segList%num .eq. -1) then
                    print*, '---ERROR: Bad ordered geometry file. Define first number of Segments'
                    stop
                end if

                n = segList%num
                allocate( segList%cpLimit(2, n) )       ! list with limiting cp per segment
                allocate( segList%mat(n) )              ! list with material index for each segment
                allocate( segList%numElem(n) )          ! list with number of elements at each segment
                allocate( segList%icType(n) )           ! list with initialization condition for each segment
                allocate( segList%scICType(n) )         ! list with initialization index for each segment
                allocate( segList%nRad(n) )             ! list with the number of radius used to interpolate tube inner shape
                allocate( segList%nTh(n)  )             ! list with the number of thicknesses used to interpolate tube wall shape
                allocate( segList%rList(n) )            ! list of pointers to real arrays for radii...
                allocate( segList%tList(n) )            ! ... and thicknesses
                allocate( segList%ModDyAr(n) )          ! list with flag for having pulsating Dyastolic area

                do i = 1, segList%num
                    read(fh, '(A)', iostat=ios) buffer          ! Update buffered line (no need to remove tag on those)
                    ! read only the first integer parameters (the last 2 read are used for allocation of memory)
                    read(buffer, *) idx, (segList%cpLimit(j, idx), j = 1, 2), segList%mat(idx), &
                                & segList%numElem(idx), segList%icType(idx), segList%scICType(idx), &
                                & segList%nRad(idx), segList%nTh(idx)
                    ! allocate dimension of real arrays being pointed at using the last 2 previous integers
                    allocate( segList%rList(idx)%r(segList%nRad(idx)) )
                    allocate( segList%tList(idx)%r(segList%nTh(idx)) )
                    ! Discard already read integer variables and store real parameters in dimensioned arrays
                    read(buffer,*) (idummy, j = 1, 9), (segList%rlist(idx)%r(j), j = 1, segList%nRad(idx)), &
                                & (segList%tList(idx)%r(j), j = 1, segList%nTh(idx))
                end do

        end select
    end do

    close(fh)

    write(fIdx%logfile, *) "---> DONE"
    write(fIdx%logfile, *)

end subroutine parseGeomFile
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   show geometry file read values
subroutine geomSummary()

    integer(ip)                                       :: i

    ! Tree definition
    write(fIdx%logfile, *) "Number of control points:     ", cpList%num
    write(fIdx%logfile, *) "Number of segments:           ", segList%num
    write(fIdx%logfile, *) "Number of boundary conditions:", bcList%num
    write(fIdx%logfile, *) "Number of scalar bound. cond.:", scBCList%num
    write(fIdx%logfile, *)

    ! Spatial discretization
    select case (gVar%discMode)
    case (0)
        write(fIdx%logfile, *) "Discretization strategy:       Max element size"
    case (1)
        write(fIdx%logfile, *) "Discretization strategy:       Number of elements per segment"
    end select
    write(fIdx%logfile, *) "Max element lenght:           ", gVar%hMax
    write(fIdx%logfile, *)

    ! Segments listing
    write(fIdx%logfile, *) "Segment list:   initial CP    final CP    elements"
    do i = 1, segList%num
        write(fIdx%logfile, *) i,": ",segList%cpLimit(1,i),segList%cpLimit(2,i), segList%numElem(i)
    end do
    write(fIdx%logfile, *)

end subroutine geomSummary
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
! Write the Domain File keeping simulation mesh and configuration info
subroutine writeDomFile()

    integer(ip)                                       :: fh
    character(len=256)                                :: frmt
    integer(ip)                                       :: ierr, n, i, j1, j2

    fh = fIdx%domain

    ! Create file to write domain data into
    open(fh, file=domnFileName, status='replace', action='write', iostat=ierr)

    frmt = '(a)'
    write(fh,frmt) '---- Configuration parameters ----'
    ! Parameters regarding FE dof, scalar variables, nodes per element and stabilization method
    frmt = '(a, x, i0)'
    write(fh,frmt) 'dof                 :', gVar%dof
    write(fh,frmt) 'nadv                :', gVar%nadv
    write(fh,frmt) 'nodes/element       :', gVar%n_el
    write(fh,frmt) 'stab                :', gVar%stab

    ! Parameters regarding time discretization
    frmt = '(a, x, e10.5e2)'
    write(fh,frmt) 'dt                  :', gVar%dt
    frmt = '(a, x, i0)'
    write(fh,frmt) 'time steps          :', gVar%nTime
    write(fh,frmt) 'time step order     :', gVar%ordT

    ! Parameters regarding solution dump
    write(fh,frmt) 'time steps / dump   :', gVar%ntspd
    frmt = '(a, x, a)'
    write(fh,frmt) 'output path         :', trim(gVar%outPath)

    ! Parameters regarding initial conditions for dof variables
    frmt = '(a, x, i0)'
    write(fh,frmt) 'number of dof IC    :', icList%num
    do i = 1, icList%num
        j1 = size(icList%ipList(i)%i)
        j2 = size(icList%rplist(i)%r)
        write(frmt,'(a, 2(i0, a))') '(a, x, i0, 2(2x, i0), '
        if (j1 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j1, '(2x, i0), '
        if (j2 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j2, '(2x, e10.3e1)'
        write(frmt, '(a, a)') trim(frmt), ')'
        write(fh,frmt) '----- dof IC        :', i, j1, j2, &
                & (icList%ipList(i)%i(n), n = 1, j1), &
                & (icList%rpList(i)%r(n), n = 1, j2)
    end do

    ! Parameters regarding initial conditions for scalar variables
    frmt = '(a, x, i0)'
    write(fh,frmt) 'number of adv IC    :', scICList%num
    do i = 1, scICList%num
        j1 = size(scICList%ipList(i)%i)
        j2 = size(scICList%rplist(i)%r)
        write(frmt,'(a, 2(i0, a))') '(a, x, i0, 2(2x, i0), '
        if (j1 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j1, '(2x, i0), '
        if (j2 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j2, '(2x, e10.3e1)'
        write(frmt, '(a, a)') trim(frmt), ')'
        write(fh,frmt) '----- adv IC        :', i, j1, j2, &
                & (scICList%ipList(i)%i(n), n = 1, j1), &
                & (scICList%rpList(i)%r(n), n = 1, j2)
    end do

    ! Parameters regarding numerical method used
    frmt = '(a, x, e10.5e2)'
    write(fh,frmt) 'alpha SUPG          :', gVar%asupg

    ! Parameters regarding fluid properties parameters
    write(fh,frmt) 'rho                 :', FlPar%rho
    write(fh,frmt) 'nu                  :', FlPar%nu
    write(fh,frmt) 'alpha               :', FlPar%alpha
    write(fh,frmt) 'kvisc               :', FlPar%kvisc
    write(fh,frmt) 'external pressure   :', FlPar%Pext
    write(fh,frmt) 'profile constant    :', FlPar%zprof
    write(fh,frmt) 'resistance coeff.   :', FlPar%kr

    ! Parameters regarding scalar diffussion coeficients
    frmt = '(a, x, *(e10.3e1))'
    write(fh,frmt) 'scalar dif. coeff.  :', (gVar%scalKd(n), n = 1, gVar%nadv)

    ! Parameters regarding wall material parameters
    frmt = '(a, x, i0)'
    write(fh,frmt) 'wall materials      :', size(WallMatList)
    do i = 1, size(WallMatList)
        j1 = size(WallMatList(i)%iParam)
        j2 = size(WallMatList(i)%rParam)
        write(frmt,'(a, 2(i0, a))') '(a, x, i0, 2(2x, i0), '
        if (j1 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j1, '(2x, i0), '
        if (j2 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j2, '(2x, e10.3e1)'
        write(frmt, '(a, a)') trim(frmt), ')'
        write(fh,frmt) '----- Material      :', i, j1, j2, &
                & (WallMatList(i)%iParam(n) , n = 1, j1), &
                & (WallMatList(i)%rParam(n) , n = 1, j2)
    end do

    ! Parameters regarding witness points locations
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'witness points      :', wpList%num
    write(fh,frmt) '----- segments      :', (wpList%seg(i), i = 1, wpList%num)
    frmt = '(a, *(x, e10.3e1))'
    write(fh,frmt) '----- location      :', (wpList%fract(i), i = 1, wpList%num)

    frmt = '(a)'
    write(fh,frmt) ''
    write(fh,frmt) '---- Geometry parameters ----'
    ! General parameters read from geometry file
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'number of cp        :', cpList%num
    write(fh,frmt) 'number of segments  :', segList%num
    write(fh,frmt) 'number of BC        :', bcList%num
    write(fh,frmt) 'discretization mode :', gVar%discMode
    frmt = '(a, *(x, e21.15e1))'
    write(fh,frmt) 'max element lenght  :', gVar%hMax

    ! CP coordinates list
    write(fh,frmt) 'CP coordinates      :', ( (cpList%coords(i, n), i = 1, 3), n = 1, cpList%num)

    ! CP data for incoming / outgoing segments
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'num. of in seg.     :', (cpList%inN(i), i = 1, cpList%num)
    write(fh,frmt) 'num. of out seg.    :', (cpList%outN(i), i = 1, cpList%num)
    write(fh,frmt) 'in segments ind.    :', ( (cpList%inSeg(i)%i(n), n = 1, cpList%inN(i)), &
                                            & i = 1, cpList%num)
    write(fh,frmt) 'out segments ind.   :', ( (cpList%outSeg(i)%i(n), n = 1, cpList%outN(i)), &
                                            & i = 1, cpList%num)
    write(fh,frmt) 'nodes at cp         :', ( (cpList%nodeL(i)%i(n), n = 1, cpList%inN(i) + cpList%outN(i)), &
                                            & i = 1, cpList%num)

    ! Segment data
    write(fh,frmt) 'segments number     :', segList%num
    write(fh,frmt) 'limiting cp         :', ( (segList%cpLimit(i, n), i = 1, 2), n = 1, segList%num)
    write(fh,frmt) 'material ind.       :', (segList%mat(n), n = 1, segList%num)
    write(fh,frmt) 'number of elements  :', (segList%numElem(n), n = 1, segList%num)
    write(fh,frmt) 'init conditions     :', (segList%icType(n), n = 1, segList%num)
    write(fh,frmt) 'scalar init cond    :', (segList%scICType(n), n = 1, segList%num)
    write(fh,frmt) 'number of radii     :', (segList%nRad(n), n = 1, segList%num)
    write(fh,frmt) 'number of thick.    :', (segList%nTh(n), n = 1, segList%num)
    write(fh,frmt) 'limit elements      :', ( (segList%elemLimit(i, n), i = 1, 2), n = 1, segList%num)
    frmt = '(a, *(x, e15.10e1))'
    write(fh,frmt) 'radius values       :', ( (segList%rList(n)%r(i), i = 1, segList%nRad(n)), &
                                            & n = 1, segList%num)
    write(fh,frmt) 'thickness values    :', ( (segList%tList(n)%r(i), i = 1, segList%nRad(n)), &
                                            & n = 1, segList%num)
    write(fh,frmt) 'lenghts list        :', (segList%Length(n), n = 1, segList%num)
    write(fh,frmt) 'dx sizes            :', (seglist%dx(n), n = 1, segList%num)

    ! Elements data
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'number of elements  :', elemList%num
    write(fh,frmt) 'segment owning el.  :', (elemList%seg(n), n = 1, elemList%num)
    write(fh,frmt) 'limit nodes         :', ( (elemList%nodLimit(i, n), i = 1, 2), n = 1, elemList%num)

    ! Nodes data
    write(fh,frmt) 'number of nodes     :', nodeList%num
    write(fh,frmt) 'segment owning node :', (nodeList%seg(n), n = 1, nodeList%num)
    frmt = '(a, *(x, e15.10e1))'
    write(fh,frmt) 'linear location     :', (nodeList%linX(n), n = 1, nodeList%num)

    ! Boundary conditions data
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'number of BC        :', bcList%num
    do i = 1, bcList%num
        j1 = bcList%nIntP(i)
        j2 = bcList%nRealP(i)
        write(frmt,'(a)') '(a, 6(2x, i0), '
        if (j1 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j1, '(2x, i0), '
        if (j2 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j2, '(2x, e10.3e1)'
        write(frmt, '(a, a)') trim(frmt), ')'
        write(fh,frmt) '----- BC            :', bcList%cp(i), bcList%type(i), 0, bcList%fType(i), &
                & j1, j2, (bcList%ipList(i)%i(n) , n = 1, j1), (bcList%rpList(i)%r(n) , n = 1, j2)
    end do

    ! Scalar injection points data
    frmt = '(a, *(x, i0))'
    write(fh,frmt) 'number of Sc src    :', scSrcList%num
    do i = 1, scSrcList%num
        j1 = scSrcList%nIPar(i)
        j2 = scSrcList%nRPar(i)
        write(frmt,'(a)') '(a, 2(2x, i0), 2x, e10.3e1, 2(2x, i0), '
        if (j1 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j1, '(2x, i0), '
        if (j2 .ne. 0) write(frmt, '(a, i0, a)') trim(frmt), j2, '(2x, e10.3e1)'
        write(frmt, '(a, a)') trim(frmt), ')'
        write(fh,frmt) '----- SC source     :', i, scSrcList%seg(i), scSrcList%loc(i), &
                & j1, j2, (scSrcList%ipList(i)%i(n) , n = 1, j1), (scSrcList%rpList(i)%r(n) , n = 1, j2)
    end do

    close(fh)

end subroutine writeDomFile
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
! Read the Domain File keeping simulation mesh and configuration info
subroutine readDomFile()


end subroutine readDomFile
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Dump segments nodal values at the specified dumping cadence into the specified output directory
subroutine writeDump(overwrite)

    logical,            intent(in)                  :: overwrite
    logical                                         :: flagEx
    character(len=256)                              :: fName, frmt
    integer(ip)                                     :: isg, ierr, e1, e2, n1, n2, in, iadv

    ! Check for existence of output directory
!    inquire(file=gVar%outPath, exist=flagEx)
!    if ( .not. flagEx) then
!        print*, '---ERROR: Output directory missing: ', gVar%outPath
!        stop
!    end if

    do isg = 1, segList%num

        fname = getDumpFilename(isg, gVar%dumpN)      ! Get dump file name

        ! Check for previous run dump files
        inquire(file=fname, exist=flagEx)
        if (flagEx .and. (.not. overwrite)) then     ! avoid overwritting existing files
            print*, '---ERROR: Files already exist in output directory. Overwrite flag = ', overwrite
            stop
        else if (flagEx) then
            call execute_command_line('rm ' // fname)
        end if

        ! Create file to write segment data into
        open(fIdx%segment, file=fname, status='new', action='write', iostat=ierr)

        write(fIdx%segment, '(a, i3.3)') 'segment ', isg
        write(fIdx%segment, '(a, f13.8, a)') 't = ', gVar%t, 's'
        write(fIdx%segment, '(a)')   ' '
        frmt =  '(3x, a, 4x, a, 7x, a, 7x, a, 7x, a, 7x, a)'
        if (gVar%nadv .eq. 0) then
            write(fIdx%segment, frmt) 'x[cm]', 'A[cm^2]', 'Q[cm^3/s]', 'P[dyna/cm^2]', 'NONE', 'NONE'
        else
            write(fIdx%segment, frmt) 'x[cm]', 'A[cm^2]', 'Q[cm^3/s]', 'P[dyna/cm^2]', 'Scal', 'Depos'
        end if

        e1 = segList%elemLimit(1, isg)          ! Get limiting elements for segment
        e2 = segList%elemLimit(2, isg)

        n1 = elemList%nodLimit(1, e1)           ! Get limiting nodes for segment
        n2 = elemList%nodLimit(2, e2)

        ! write data [position, area, flux, pressure, scalar list values]
        frmt = '(f8.3, 1x, f13.8, 1x, f13.8, 1x, f13.2, *(1x, f13.5))'
        if (gVar%nadv .eq. 0) then
            do in = n1, n2
                write(fIdx%segment, frmt) nodeList%linX(in), nodeList%u(1,in), nodeList%u(2,in), &
                    & nodeList%p(in), 0.0, 0.0
            end do
        else
            do in = n1, n2
                write(fIdx%segment, frmt) nodeList%linX(in), nodeList%u(1,in), nodeList%u(2,in), &
                    & nodeList%p(in), [ ( nodeList%scal(iadv, in) , iadv = 1, gVar%nadv) ], &
                    [ (nodeList%depos(iadv, in), iadv = 1, gVar%nadv) ]
            end do
        end if

        close(fIdx%segment)

    end do

    gVar%dumpN = gVar%dumpN + 1

contains
function getDumpFilename(seg, num) result(fn)

    integer(ip),      intent(in)                    :: seg, num
    character(len=5)                                :: fnum
    character(len=3)                                :: fseg
    character(len=256)                              :: fn

    write(fseg, '(i3.3)') seg
    write(fnum, '(i5.5)') num
    fn = trim(gVar%outPath) // trim(casename) // '.seg' // fseg // 'step' // fnum // '.txt'

end function getDumpFilename

end subroutine writeDump
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Open witness points output files
!   Directory to output existence already verified by writeDump function previously
subroutine openWPFile(wp, OWflag)

    integer(ip),      intent(in)                      :: wp
    logical,        intent(in)                        :: OWflag
    logical                                           :: flagEx
    character(len=256)                                :: fName, frmt
    integer(ip)                                       :: fIndex
    integer(ip)                                       :: ierr, i

    fName = getWPFilename(wp)                       ! get dump file name for witness point with index wp

    inquire(file=fName, exist=flagEx)
    if ( flagEx .and. (.not. OWflag)) then          ! avoid overwritting existing files if flag is set
        print*, '---ERROR: Files already exist in output directory: ', fName
        stop
    else if (flagEx) then                           ! remove files if they already exists (overwrite)
        call execute_command_line('rm ' // fName)
    end if
    ! Create new file to write segment data into
    fIndex = fIdx%witnessP + wp                     ! base number for the wp file indices + wp number
    open(fIndex, file=fname, status='new', action='write', iostat=ierr)
    ! Add header data to the newly created file
    write(fIndex, '(a, i9.1)' ) 'WitnessPoint ', wp
    write(fIndex, '(a, i9.1)' ) 'Segment      ', wpList%seg(wp)
    write(fIndex, '(a, i9.1)' ) 'Element      ', wpList%elem(wp)
    write(fIndex, '(a, i9.1)' ) 'Node (dummy) ', 0
    write(fIndex, '(a, f12.6)') 'Coords1D =   ', wpList%linXWP(wp)
    frmt = '(a, f12.6, 2(1x, f12.6))'
    write(fIndex, frmt)         'Coords3D =   ', (wpList%coord3D(i, wp), i = 1, 3)
    write(fIndex, '(a)')        ' '
    frmt = '(4x, a, 10x, a, *(7x, a))'
    if ( gVar%nadv .eq. 0) then
        write(fIndex, frmt ) 't[s]', 'A[cm^2]', 'Q[cm^3/s]', 'P[dyna/cm^2]', 'No-Scalars'
    else
        write(fIndex, frmt ) 't[s]', 'A[cm^2]', 'Q[cm^3/s]', 'P[dyna/cm^2]', 'Scalars', 'Depos'
    end if

contains
function getWPFilename(wp) result(fn)
    integer(ip),      intent(in)                      :: wp
    character(len=3)                                  :: wpnum
    character(len=256)                                :: fn
    write(wpnum, '(i3.3)') wp
    fn = trim(gVar%outPath) // casename //'.witpoint' // wpnum // '.txt'
end function getWPFilename

end subroutine openWPFile

!----------------------------------------------------------------------------------------------
!
!   Write witness points data on output files
subroutine writeWP(wp)

    integer(ip),      intent(in)                      :: wp
    character(len=256)                                :: frmt
    integer(ip)                                       :: fIndex
    real(rp),     dimension(3 + 2*gVar%nadv)          :: vars

    fIndex = fIdx%witnessP + wp                  ! base number for the wp file indices + wp number

    !interpolate values
    call getWPValues()
    frmt = '(f14.8, 2(1x, f13.8), 1x, f13.2, *(1x, f13.5))'
    if ( gVar%nadv .eq. 0 ) then
        write(fIndex, frmt) gVar%t, vars, 0.0_rp, 0.0_rp
    else
        write(fIndex, frmt) gVar%t, vars
    end if

contains
subroutine getWPValues()

    integer(ip)                                       :: el, n1, n2
    real(rp)                                          :: x1, x2, xi
    real(rp),     dimension(gVar%n_el)                :: Nx
    integer(ip)                                       :: i

    el = wpList%elem(wp)                ! Element containing the wp
    n1 = elemList%nodLimit(1, el)       ! Limiting nodes for element
    n2 = elemList%nodLimit(2, el)
    x1 = nodeList%linX(n1)              ! linear location limits on element
    x2 = nodeList%linX(n2)

    ! Isoelemental coordinate for the wp in the containing element
    ! Debug conditional here [TBM]
    xi = -1.0_rp + 2.0_rp * (wpList%linXWP(wp) - x1) / (x2 - x1)

    ! array with values of shape functions evaluated at isocoordinate [xi]
    Nx(:) = [ (shapeF(gVar%n_el, i, xi), i = 1, gVar%n_el) ]
    ! vars array with [A, Q, P, *(Sc)] obtained by dot product of nodal values
    !       and shape functions values
    vars(:) = [ dot_product(nodeList%u(1,n1:n2), Nx(:)), &
            &   dot_product(nodeList%u(2,n1:n2), Nx(:)), &
            &   dot_product(nodeList%p(n1:n2), Nx(:)), &
            &   ( dot_product(nodeList%scal(i,n1:n2), Nx(:)) , i = 1, gVar%nadv), &
            &   ( dot_product(nodeList%depos(i,n1:n2), Nx(:)), i = 1, gVar%nadv) ]

end subroutine getWPValues

end subroutine writeWP
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   close the previously opened witness points files
subroutine closeWPFile(wp)

    integer(ip),      intent(in)                      :: wp
    integer(ip)                                       :: fIndex

    fIndex = fIdx%witnessP + wp                  ! base number for the wp file indices + wp number
    close(fIndex)

end subroutine closeWPFile
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   set casename which can be set as subroutine argument or read from command-line argument
subroutine readCasename(arg_optional)

    character(len=100), intent(in), optional :: arg_optional
    character(len=100)                       :: arg_commandline    

    fIdx%configuration = 15
    fIdx%geometry      = 16
    fIdx%domain        = 17
    fIdx%logfile       = 6
    
    if( present(arg_optional) )then
        casename = trim(arg_optional)
    else
        call get_command_argument(1, arg_commandline)
        casename = trim(arg_commandline)
     endif
     
    confFileName = trim(casename) // '.conf.txt'
    geomFileName = trim(casename) // '.geom.txt'
    domnFileName = trim(casename) // '.domn.txt'
    logFileName  = trim(casename) // '.log'

end subroutine readCasename
!----------------------------------------------------------------------------------------------

end module io
