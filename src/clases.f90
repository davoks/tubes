module clases

    use nrtype

    implicit none

    !--- The types RArray and IArray are used to create double indexable structures where the second array may have
    !    different length for each value of the first array index
    type RArray
        real(rp),     dimension(:),   pointer         :: r    ! allocatable array of reals
    end type RArray
    type IArray
        integer(ip),  dimension(:),   pointer         :: i    ! allocatable array of integers
    end type IArray
    !---

    type ElMatType
        !--- Elemental matrices of integrals of shape and/or derivatives of shape functions products
        real(rp), dimension(:,:)  , allocatable   :: NN
        real(rp), dimension(:,:)  , allocatable   :: NdN
        real(rp), dimension(:,:)  , allocatable   :: dNN
        real(rp), dimension(:,:)  , allocatable   :: dNdN
        real(rp), dimension(:,:)  , allocatable   :: dfij
    end type ElMatType

    !--- This structure holds the information related to geometry Control Points
    !    CP are locations in 3D space where segments have their start / end nodes.
    !    Any CP may have an arbitraty number of nodes associated to it, each belonging to a different segment,
    !    but representing the same point in the real geometry.
    type CPListType
        !--- Set of control points parameters read from geometry file (io/parseGeomFile)
        integer(ip)                                :: num     ! number of control points in network
        real(rp),    dimension(:,:), allocatable   :: coords  ! matrix array with cp coordinates
                                                              !   coords(Xi, n) -> coordinate Xi on n-th CP
        !--- Variables allocated during meshing
        integer(ip), dimension(:),   allocatable   :: inN     ! inN(n) -> Number of segments entering the n-th CP
        integer(ip), dimension(:),   allocatable   :: outN    ! outN(n) -> Number of segments exiting the n-th CP
        type(IArray),dimension(:),   allocatable    :: inSeg   ! Incoming segment indexes (ordered) at listed cp
                                                              !   inSeg(n)%i(m) -> global idx of m-th incoming seg on nth CP
        type(IArray),dimension(:),   allocatable    :: outSeg  ! Outgoing segment indeses (ordered) at listed cp
                                                              !   outSeg(n)%i(m) -> global idx of m-th outgoing seg on nth CP
        type(IArray),dimension(:),   pointer        :: nodeL   ! List of nodes at cp [inseg + outseg] in order
                                                              !   nodeL(n)%i(m) -> golbal index of m-th node on n-th CP
        integer(ip), dimension(:),   allocatable   :: bcNum   ! Index number of boundary condition
        ! Variables allocated on initalization
        type(RArray),dimension(:),   allocatable    :: uBV     ! Array to array of reals with the values of the
                                                              !   unknowns on the nodes at the cp in the shape
                                                              !   uBV(n)%r(dof*(m-1)+d) -> unknown d on m-th node of n-th CP
        real(rp),    dimension(:,:), allocatable   :: scBV    ! Array with scalar boundary values on the CP [nadv, ncp]
        real(rp),    dimension(:,:), allocatable   :: scFlux  ! Avee scalar flux going into the CP at each CP node [nadv, ncp]
        type(RArray),dimension(:),   allocatable   :: wBV     ! extrapolated characteristics, outgoing only
                                                              !   [nodeL(icp)%i(:)]
        type(RArray),dimension(:),   allocatable   :: antWBV  ! characteristics on previous time step, both
                                                              !   [2 * nodL(icp)%i(:)]
    end type CPListType

    type SegListType
        !--- Set of segment parameters read from geometry file (io/parseGeomFile)
        integer(ip)                                   :: num          ! number of segments in network
        ! indexes of initial and final control points defining segment position stored as columns in
        ! cpLimit matrix for as many as segments are [2, seg num]
        integer(ip),  dimension(:,:), allocatable     :: cpLimit
        integer(ip),  dimension(:),   allocatable     :: mat          ! material indexes list ordered by segment index
        integer(ip),  dimension(:),   allocatable     :: numElem      ! number of elements at segment ordered by index
        ! Initial conditions for segment ordered by index (for unknown values and scalar values respectively)
        !   0 ->    Constant offset
        !   1 ->    Gauss distribution
        integer(ip),  dimension(:),   allocatable     :: icType       ! Types of initial condition ordered by segment index
        integer(ip),  dimension(:),   allocatable     :: scICType     ! Types of scalar init. cond. ordered by segment index
        integer(ip),  dimension(:),   allocatable     :: nRad         ! number of radii used for interpolation at indexed segment
        integer(ip),  dimension(:),   allocatable     :: nTh          ! number of thicknesses for interpolation at indexed segment
        ! Lists of radii and thickness for each segment. Stored as arrays of real arrays rList(index)%r(n)
        type(RArray),dimension(:),    allocatable     :: rList        ! array of real arrays with radius values
        type(RArray),dimension(:),    allocatable     :: tList        ! array of real arrays with thickness values
        !--- Set of segment parameters calculated during mesh generation
        real(rp),     dimension(:),   allocatable     :: Length       ! Length of segment
        real(rp),     dimension(:),   allocatable     :: dx           ! discretization size of elements
        ! indexes of initial and final elements in segment stored in elemLimit as columns
        ! for as many as segments are [2, seg num]
        integer(ip),  dimension(:,:), allocatable     :: elemLimit
        logical,      dimension(:),     allocatable   :: ModDyAr      ! Flag to change Dyastolic area during the run
        !--- Set of segment parameters calculated during FE initialization
        type(ElMatType), dimension(:), allocatable  :: elMat          ! elemental matrices
    end type SegListType

    type ElemListType
        !--- Set of element parameters calculated during mesh generation
        integer(ip)                                   :: num          ! Total number of elements
        integer(ip),  dimension(:),   allocatable     :: seg          ! List of segments index that contain each element
        ! indexes of initial and final nodes in element stored in nodLimit as columns
        ! for as many as elements are [2, elem num]
        integer(ip),  dimension(:,:), allocatable     :: nodLimit
    end type ElemListType

    type NodeListType
        !--- Set of node parameters calculated during mesh generation
        integer(ip)                               :: num          ! Total number of nodes
        integer(ip),  dimension(:),   allocatable :: seg          ! List of segments index that contain each node
        real(rp),     dimension(:),   allocatable :: linX         ! linear distance on segment
        !--- Set of node parameters allocated during initialization
        real(rp),     dimension(:),   allocatable :: dyArea       ! Dyastolic area (interpolated from radii definition at geom file)
                                                                  ! Ad = pi . r0^2
        real(rp),     dimension(:),   allocatable :: dyA0         ! [RESEARCH] Initial dyastolic area
        real(rp),     dimension(:),   allocatable :: beta         ! Tube elastic restoration parameter beta
                                                                  ! B = E . th . pi^0.5 / ( 1 - poisson^2 )
        real(rp),     dimension(:),   allocatable :: dB_dx        ! spatial derivative of beta at each node
                                                                  !   calculated once during initialization
        real(rp),     dimension(:),   allocatable :: area0        ! Equilibrium area
                                                                  ! A0 = Ad · (1 - Ad^0.5 · Pd / beta)^2
        real(rp),     dimension(:),   allocatable :: dA_dx        ! spatial derivative of dyastolic area at each node
                                                                  !   calculated once during initialization (for static DyArea)
        real(rp),     dimension(:,:), allocatable :: u            ! node dof to solve [dof, num nodes]
        real(rp),     dimension(:,:), allocatable :: du           ! variations of node dof [dof, num nodes]
        real(rp),     dimension(:),   allocatable :: p            ! pressure at each node [num nodes]
        real(rp),     dimension(:,:), allocatable :: scal         ! nodal values of advected scalars [nuadv, num nodes]
        real(rp),     dimension(:,:), allocatable :: dSc_dx       ! nodal values of scalar spatial derivative [nuadv, num nodes]
        real(rp),     dimension(:,:), allocatable :: dscal        ! variation of transported scalars [nuadv, num nodes]
        real(rp),     dimension(:,:), allocatable :: depos        ! deposition of scalars during transport (negative source)
        real(rp),     dimension(:,:), allocatable :: depFact      ! exponential factor used to calculate deposition
    end type NodeListType

    type DomainType
        integer(ip)                                   :: nod        ! number of nodes in domain
        integer(ip),  dimension(2)                    :: nLim       ! limiting nodes for the domain
        integer(ip),  dimension(2)                    :: BCType     ! Boundary condition type
        ! Memory allocated during domain setting
        integer(ip),  dimension(:,:), allocatable     :: scBCType   !   and scalar boundary condition [nadv, 2]
                                                                    !       0 -> Dirichlet
                                                                    !       1 -> Neumann
        real(rp),     dimension(:,:), allocatable     :: scFlux     ! Scalar flux on the segment extrem in segment coordinates
                                                                    !   [nadv, 2]
        real(rp),     dimension(:,:), allocatable     :: lumpedNN   ! lumped mass matrix
        real(rp),     dimension(:,:), allocatable     :: scLumpNN   ! lumped mass matrix for sc variables
    end type DomainType

    type InitCondType
        integer(ip)                                 :: num          ! number of initial conditions
        type(IArray),dimension(:),  allocatable     :: ipList       ! Array of integer arrays with parameters
        type(RArray),dimension(:),  allocatable     :: rpList       ! Array of real arrays with parameters
    end type InitCondType

    type ScInitCondType
        integer(ip)                                 :: num          ! number of scalar initial conditions
        type(IArray),dimension(:),  allocatable     :: ipList       ! Array of integer arrays with parameters
        type(RArray),dimension(:),  allocatable     :: rpList       ! Array of real arrays with parameters
    end type ScInitCondType

    type BoundCondType
        integer(ip)                                 :: num          ! number of global boundary conditions
        integer(ip),  dimension(:), allocatable     :: cp           ! control point where boundary conditions are applied
        integer(ip),  dimension(:), allocatable     :: type         ! Type of boundary condition
        integer(ip),  dimension(:), allocatable     :: extF         ! index of external file with tabulated function values
        integer(ip),  dimension(:), allocatable     :: fType        ! Type of function applied at the boundary condition
        integer(ip),  dimension(:), allocatable     :: nIntP        ! Number of integer parameters used by the BC
        integer(ip),  dimension(:), allocatable     :: nRealP       ! Number of real parameters used by the BC
        type(IArray), dimension(:), allocatable     :: ipList       ! Array of integer arrays with parameters
        type(RArray), dimension(:), allocatable     :: rpList       ! Array of real arrays with parameters
    end type BoundCondType

    type scBoundCondType
        integer(ip)                               :: num          ! number of scalar boundary condition definitions
        integer(ip), dimension(:,:), allocatable  :: type         ! Type of boundary condition [Nadv num]
        integer(ip), dimension(:,:), allocatable  :: fType        ! Type of function applied at the boundary condition [Nadv num]
        integer(ip), dimension(:,:), allocatable  :: nRealP       ! Number of real parameters used by the BC [Nadv num]
        type(RArray),dimension(:,:), allocatable  :: rpList       ! Array of real arrays with parameters [Nadv num]
    end type scBoundCondType

    type ScSourceType
        integer(ip)                                   :: num          ! number of scalar injection points
        integer(ip),  dimension(:),   allocatable     :: seg          ! indexes of segments where injections are applied
        real(rp),     dimension(:),   allocatable     :: loc          ! fraction of segment where injection is applied
        integer(ip),  dimension(:),   allocatable     :: nIPar        ! number of integer parameters per source definition
        integer(ip),  dimension(:),   allocatable     :: nRPar        ! number of real parameters per source definition
        type(IArray), dimension(:),   allocatable     :: ipList       ! Array of integer parameters arrays
                                                                      !   1st -> advected variable affected
                                                                      !   2nd -> function applied to the injection
        type(RArray), dimension(:),   allocatable     :: rpList       ! Array of real parameters arrays
    end type ScSourceType

    type FluidType
        !--- Parameters for the fluid material to simulate read from conf file
        real(rp)                                      :: rho          ! density
        real(rp)                                      :: nu           ! kinematic viscosity
        real(rp)                                      :: alpha        ! momentum-correction
        real(rp)                                      :: kvisc        ! viscoelasticity
        real(rp)                                      :: Pext         ! external pressure
        real(rp)                                      :: zprof        ! flow profile constant
        !--- Parameters calculated at reading time
        real(rp)                                      :: kr           ! friction resistance coefficient
                                                                      ! kr = 2.(zprof+2).pi.nu
    end type FluidType

    type WallMType
        !--- Definition parameters for the Wall materials in config file
        integer(ip),  dimension(:),   allocatable     :: iParam       ! integer parameters
        real(rp),     dimension(:),   allocatable     :: rParam       ! real parameters [Young Poisson Dyast.Pressure]
    end type WallMType

    type WitPointType
        !--- Witness points to store each time steps allocated during read from conf file
        integer(ip)                                   :: num          ! number of witness points
        integer(ip),  dimension(:),   allocatable     :: seg          ! segment where the wp is
        real(rp),     dimension(:),   allocatable     :: fract        ! fraction of segment where the wp is located
        !--- Data allocated and calculated during mesh generation
        real(rp),     dimension(:),   allocatable     :: linXWP       ! linear location of wp
        real(rp),     dimension(:,:), allocatable     :: coord3D      ! spatial coordinates of point
        integer(ip),  dimension(:),   allocatable     :: elem         ! element containing wp
    end type WitPointType

    type ExtFuncType
        !--- External function tabulated points read from file
        integer(ip)                                   :: numP         ! number of points used to define each function
        real(rp),     dimension(:),   allocatable     :: tList        ! list with tabulated times
        real(rp),     dimension(:),   allocatable     :: fList        ! list with tabulated function values
    end type ExtFuncType

    type GlobalVariables
        !--- Global variables read from geometry file
        integer(ip)                                   :: discMode     ! Selector of discretization mode
                                                                      !   0 ->    Max element size
                                                                      !   1 ->    constant number of elements per segment
        real(rp)                                      :: hMax         ! max element discretization size allowed
        !--- Global variables read from configuration file
        integer(ip)                                   :: dof          ! Degrees of freedom to solve (A and Q)
        integer(ip)                                   :: nadv         ! Number of advected scalars
        integer(ip)                                   :: n_el         ! number of nodes per element
        integer(ip)                                   :: stab         ! Selector of stabilization method
                                                                      !   0 ->    No stabilization
                                                                      !   1 ->    Streamline Upwind Petrov-Galerkin (SUPG)
                                                                      !   2 ->    Taylor-Galerkin
        real(rp)                                      :: dt           ! Time step value
        integer(ip)                                   :: nTime        ! Total number of time steps
        integer(ip)                                   :: ordT         ! Order of time stepping scheme
                                                                      !   1 ->    Euler FW
                                                                      !   2, 3, 4 ->  RK(2), RK(3), RK(4)
        integer(ip)                                   :: ntspd        ! Number of time steps per dump
        character(len=256)                            :: outPath      ! Output directory path
        real(rp)                                      :: asupg        ! SUPG method alpha coefficient
        integer(ip)                                   :: nExtF        ! number of external functions
        !--- currently hardcoded, but should be read from configuration file [TODO]
        real(rp), dimension(:)    , allocatable       :: scalKd       ! diffusion coefficients
        !--- Time variables
        real(rp)                                      :: t            ! time
        integer(ip)                                   :: it=0         ! timestep counter
        !--- Other global variables used
        integer(ip)                                   :: dumpN        ! counter for dump files
        integer(ip)                                   :: confRev = 1  ! Specify the number of revision for the input files that
        integer(ip)                                   :: geomRev = 1  !   the current version of code can read
        !--- External flowrate from external source (ie: Alya)
        real(rp)                                      :: Q_external
    end type GlobalVariables

type GlobalTimings
        real(rp)                                      :: part1 = 0.0_rp
        real(rp)                                      :: part2 = 0.0_rp
        real(rp)                                      :: part3 = 0.0_rp
        real(rp)                                      :: part4 = 0.0_rp
        real(rp)                                      :: part5 = 0.0_rp
        real(rp)                                      :: part6 = 0.0_rp
        real(rp)                                      :: part7 = 0.0_rp
        real(rp)                                      :: part8 = 0.0_rp
        real(rp)                                      :: part9 = 0.0_rp
        real(rp)                                      :: part10 = 0.0_rp
        real(rp)                                      :: part11 = 0.0_rp
end type GlobalTimings


type FileIndex
        !--- Index number for input/output fixed files
        integer(ip)                                   :: geometry         = 10
        integer(ip)                                   :: configuration    = 20
        integer(ip)                                   :: domain           = 30
        integer(ip)                                   :: logfile          = 40
        !--- Initial index number for output variable number files
        integer(ip)                                   :: witnessP         = 1000
        integer(ip)                                   :: segment          = 100
        !--- Index number for external function files
        integer(ip)                                   :: extFunc          = 50
end type FileIndex

    type(FileIndex)                                 :: fIdx
    type(BoundCondType)                             :: bcList
    type(scBoundCondType)                           :: scBCList
    type(InitCondType)                              :: icList
    type(ScInitCondType)                            :: scICList
    type(ScSourceType)                              :: scSrcList
    type(WitPointType)                              :: wpList
    type(WallMType),    dimension(:), allocatable   :: WallMatList
    type(FluidType)                                 :: FlPar
    type(ElMatType)                                 :: isoMat       ! Iso-elemental matrices for element made of n_el nodes
    type(CPListType)                                :: cpList
    type(SegListType)                               :: segList
    type(ElemListType)                              :: elemList
    type(NodeListType)                              :: nodeList
    type(DomainType),   dimension(:), allocatable   :: dom
    type(ExtFuncType),  dimension(:), allocatable   :: extFList
    type(GlobalVariables)                           :: gVar
    type(GlobalTimings)                             :: gTime
    character(len=:), allocatable                   :: casename
    character(len=:), allocatable                   :: confFileName
    character(len=:), allocatable                   :: geomFileName
    character(len=:), allocatable                   :: domnFileName
    character(len=:), allocatable                   :: logFileName

end module clases

