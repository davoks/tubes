subroutine get_timestep_tubes(dt)

    use nrtype
    use clases, only : confFileName
    use clases, only : fIdx
    
    real(rp), intent(out) :: dt
    integer(ip)           :: pos
    integer(ip)           :: idummy
    real(rp)              :: rdummy
    integer(ip)           :: ios = 0       ! return value for file read io operation
    character(len=256)    :: buffer, label

    write(fIdx%logfile, *)
    write(fIdx%logfile, *) "--- Getting original timestep from Tubes ---"


    open(fIdx%configuration,  file=confFileName,status='old') ! open file for reading

    ! ios is negative if an end of record condition is encountered or if
    ! an endfile condition was detected.  It is positive if an error was
    ! detected.  ios is zero otherwise.
    do while (ios == 0)
        read(fIdx%configuration, '(A)', iostat=ios) buffer    ! get line from the file
        if (ios /= 0) exit                                    ! avoid evalution if error is detected

        ! Split label and data using rightmost '#' character .
        pos    = scan(buffer, '#', .true.)                    ! .true. => rightmost appearance
        label  = buffer(1:pos)
        buffer = buffer(pos+1:)

        ! read parameters from config file depending on label
        select case (label)

        ! Read timestep
        case ('#tStep#')
            read(buffer, *) dt, rdummy, rdummy, rdummy,  idummy, idummy, rdummy

        end select

    end do

    close(fIdx%configuration)

    write(fIdx%logfile, *) "---> DONE"
    write(fIdx%logfile, *)

end subroutine get_timestep_tubes 
