module galtub

    use nrtype
    use clases
    use boundcon
    use physfuncs
    use femodule
    use io
    use scgaltub
    use omp_lib

    implicit none

contains

!----------------------------------------------------------------------------------------------
!
!   Time iteration loop is performed here. It calls the computations to be performed at this time
!   step, writes the Witness Points extracted values and gets per segment sampled data
subroutine runSim()

    integer(ip)                                       :: it, seg, i
    integer(ip)                                       :: tt1, tt2, rate
    real(rp)                                          :: progress
    logical                                           :: OW

    call system_clock(tt1, rate)                ! Set initial time mark

    allocate( dom( segList%num ) )              ! create domains

    call omp_set_num_threads( min( segList%num, omp_get_num_procs() ) )
! OMP thread initialization
!$OMP parallel default(firstprivate), &
!$OMP & shared( bcList,         icList,     scICList,   scSrcList,&
!$OMP &         WallMatList,    FlPar,      isoMat,     cpList,&
!$OMP &         segList,        elemList,   nodeList,   dom,        gVar)

! Split the load for this loop
!$OMP do schedule(dynamic)
    do seg = 1, segList%num                     ! fill domains information
        call gatherDomain(seg)                  ! Store limit node information and lumped matrix
    end do
!$OMP end do
!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   Finished gathering domain"

    OW = .true.                                         ! Overwrite flag for dump files

!$OMP do schedule(dynamic)
    do i = 1, wpList%num
        call openWPFile(i, OW)
        call writeWP(i)
    end do
!$OMP end do

    do it = 1, gVar%ntime

!$OMP single
        ! Update time variable (only one thread)
        gVar%t = gVar%t + gVar%dt
!$OMP end single
!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   After time update --- t = ", gVar%t
! time has to be syncronized for all threads
!$OMP barrier
!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   entering timeloop --- t = ", gVar%t
        call timeloop()
!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   exited timeloop --- t = ", gVar%t
! --- Delete when threads write files separately
!$OMP barrier
!$OMP flush
! ---

!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   writing wp files --- it = ", it
        ! Write operations perfomed currently by only one thread [TODO]: Use multiple threads
!$OMP do schedule(dynamic)
        do i = 1, wpList%num
            call writeWP(i)
        end do
!$OMP end do

!$OMP single
!write(fIdx%logfile,*) omp_get_thread_num(),"/",omp_get_num_threads(), "---   writing dump files --- it = ", it
        if ( mod(it,gVar%ntspd) .eq. 0 ) then
            progress = real(it * 100)/real(gVar%ntime)
            write(fIdx%logfile,'(a, i6, a, f5.1, a)') 'Time step: ', it, '  ', progress, '% completed'
            call writeDump(OW)
        end if
!$OMP end single
    end do

![TODO] Remove this writing loop (it's repeated from timeloop last iteration)
!$OMP do schedule(guided)
    do i = 1, wpList%num
        call writeWP(i)
    end do
!$OMP end do
!$OMP end parallel

    call system_clock(tt2)                       ! Get final time mark

    write(fIdx%logfile,*)
    write(fIdx%logfile,*) '*** Total simulation time: ', real(tt2-tt1,8)/real(rate,8), ' s'

    deallocate(dom)

end subroutine runSim
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Prepare variables in array dom(:) to simplify the pass of values to functions that compute
!   partial derivatives over time
subroutine gatherDomain(seg)

    integer(ip),  intent(in)                          :: seg
    integer(ip)                                       :: e1, e2
    real(rp),     dimension(:,:),     allocatable     :: v1


    e1 = segList%elemLimit(1,seg)               ! limiting elements on segment
    e2 = segList%elemLimit(2,seg)

    dom(seg)%nLim(1) = elemList%nodLimit(1, e1) ! limiting nodes on segment
    dom(seg)%nLim(2) = elemList%nodLimit(2, e2)

    ! number of total nodes in the segment
    dom(seg)%nod = dom(seg)%nLim(2) - dom(seg)%nLim(1) + 1

    ! store lumped matrix [NN] vector
    allocate( dom(seg)%lumpedNN(gVar%dof, dom(seg)%nod) )
    allocate( v1(gVar%dof, dom(seg)%nod) )
    v1 = 1.0_rp
    call multEleMatVec(segList%elMat(seg)%NN, v1, dom(seg)%lumpedNN)
    deallocate( v1 )

    if (gVar%nadv .ne. 0) then
        ! create structure to store the type of boundary (0 Dir or 1 Neu )for the scalars
        allocate( dom(seg)%scBCType ( gVar%nadv , 2 ) )
        ! create structure to store the total scalar flux at the ends of the segment
        allocate( dom(seg)%scFlux   ( gVar%nadv , 2 ) )
        ! create a lumped matrix [NN] vector for the scalar computation
        allocate( dom(seg)%scLumpNN ( gVar%nadv , dom(seg)%nod) )
        allocate( v1(gVar%nadv , dom(seg)%nod) )
        v1 = 1.0_rp
        call multEleMatVec(segList%elMat(seg)%NN, v1, dom(seg)%scLumpNN)
        deallocate( v1 )
    end if

end subroutine gatherDomain
!----------------------------------------------------------------------------------------------


!----------------------------------------------------------------------------------------------
!
!   Computations to be performed each time step to advance the simulation to the next one.
subroutine timeloop()

    integer(ip)                                           :: icp, n, k
    integer(ip)                                           :: isg

    ! [RESEARCH]: modification of dyastolic area as function of time
!$OMP do schedule(guided)
    do isg = 1, segList%num
        if ( segList%ModDyAr(isg) ) call updateDyAr(isg)
    end do
!$OMP end do

    ! obtain value of boundary nodes (loop over control points)
    !   and store them in the cpList structure
!$OMP do schedule(guided)
    do icp = 1, cpList%num
      call getBouConds(icp)
    end do
!$OMP end do


    ! Main loop is performed at each segment as a monolityc domain
    ! Get evolution of Q and A in the time step by soving the Galerkin FE equations
!$OMP do schedule(guided)
    do n = 1, segList%num
        call doIter(n)
    end do
!$OMP end do

!$OMP do schedule(guided)
    ! impose boundary conditions at each cp
    do icp = 1, cpList%num
        call applyBouConds(icp)
    end do
!$OMP end do

!$OMP do schedule(guided)
    do n = 1, segList%num
        call pressureSegment(n)
    end do
!$OMP end do

contains
subroutine updateDyAr(s)
    integer(ip), intent(in)                           :: s
    integer(ip)                                       :: nd, n1, n2
    real(rp)                                          :: Tper = 2.0_rp
    real(rp)                                          :: var  = 0.15_rp
    n1 = elemList%nodLimit(1, segList%elemLimit(1,s) )
    n2 = elemList%nodLimit(2, segList%elemLimit(2,s) )
    do nd = n1, n2
        nodeList%dyArea(nd) = nodeList%dyA0(nd) * &
                              ( 1.0_rp + var * sin( real(nd - n1, 8)/real(n2 - n1, 8) * pi ) * &
                                sin(gVar%t * 2.0_rp * pi / Tper) )
    end do
end subroutine

end subroutine timeloop
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Computations to be performed each time step to advance the simulation to the next one.
!   1st order FW Euler or different orders of RK are used to advance the values of the variables
!   to the next step.
!   Computation is performed on the segment [is]
subroutine doIter(is)

    integer(ip),  intent(in)                          :: is
    integer(ip)                                       :: n1, n2
    real(rp), dimension(gVar%dof, dom(is)%nod)        :: up, du_dt0, du_dt1, du_dt2, du_dt3
    real(rp), dimension(gVar%nadv, dom(is)%nod)       :: scp, dsc_dt0, dsc_dt1, dsc_dt2, dsc_dt3

    n1 = dom(is)%nLim(1)
    n2 = dom(is)%nLim(2)

    ! use time stepping order defined in configuration file
    select case(gVar%ordT)
    case(1)             ! 1st order forward Euler
        if (gVar%nadv .eq. 0) then
            ! compute variation of unknown variables as du = dt . du/dt
            call computeDu_Dt(is, nodeList%u(:,n1:n2), du_dt0(:,:))
        else
            ! add variation of scalar variables as ds = dt · ds/dt to previous computations
            call compAllD_Dt(is, nodeList%u(:,n1:n2), du_dt0(:,:), nodeList%scal(:,n1:n2), dsc_dt0(:,:))
            nodeList%dscal(:,n1:n2) = gVar%dt * dsc_dt0(:,:)
        end if
        nodeList%du(:,n1:n2) = gVar%dt * du_dt0(:,:)
    case(2)             ! 2nd order Runge-Kutta
    case(3)             ! 3rd order Runge-Kutta
    case(4)             ! 4th order Runge-Kutta
        ! dY = f(t, Y)  -->  dY ~ dt/6 * (k1 + 2 k2 + 2 k3 + k4)
        ! k1 = f(t0, Y0)                        k2 = f(t0 + dt/2, Y0 + dt/2 * k1)
        ! k3 = f(t0 + dt/2, Y0 + dt/2 * k2)     k4 = f(t0 + dt, Y0 + dt * k3)
        ! 1st step of RK4
        call computeDu_Dt(is, nodeList%u(:,n1:n2), du_dt0)               ! du_dt0 == k1
        !dsc_dt0 = computeDsc_Dt(is)
        ! partial updated variable              ! up == Y0 + dt/2 * k1
        up = nodeList%u(:,n1:n2) + .5_rp * gVar%dt * du_dt0
        !scp = nodeList%scal(:,n1:n2) + .5_rp * gVar%dt * dsc_dt0
        ! impose boundary nodal values of last time step

        !!! u1(boudofs_r) = dofvalues_r                 ! Should consider t0+dt/2 ?

        ! 2nd step of RK4
        call computeDu_Dt(is, up, du_dt1)           ! du_dt1 == k2       implicit f(Y)?
        !dsc_dt1 = computeDsc_Dt(is, scp)
        ! partial updated variable
        up = nodeList%u(:,n1:n2) + .5_rp * gVar%dt * du_dt1     ! up == Y0 + dt/2 * k2
        !scp = nodeList%scal(:,n1:n2) + .5_rp * gVar%dt * dsc_dt1
        ! impose boundary nodal values of last time step

        !!! u1(boudofs_r) = dofvalues_r                 ! Should consider t0+dt/2 ?

        ! 3rd step of RK4
        call computeDu_Dt(is, up, du_dt2)           ! du_dt2 == k3       implicit f(Y)?
        !dsc_dt2 = computeDsc_Dt(is, scp)
        ! partial updated variable
        up = nodeList%u(:,n1:n2) + gVar%dt * du_dt2              ! up == Y0 + dt * k3
        !scp = nodeList%scal(:,n1:n2) + gVar%dt * dsc_dt2
        ! impose boundary nodal values of last time step

        !!! u1(boudofs_r) = dofvalues_r                 ! Should consider t0+dt ?

        ! 4th step of RK4
        call computeDu_Dt(is, up, du_dt3)           ! du_dt3 == k4       implicit f(Y)?
        !dsc_dt3 = computeDsc_Dt(is, scp)
        ! sum all contributions
        nodeList%du(:,n1:n2) = gVar%dt*(du_dt0 + 2.0_rp*du_dt1 + 2.0_rp*du_dt2 + du_dt3)/6._rp
        !nodeList%dscal(:,n1:n2) = gVar%dt*(dsc_dt0 + 2.0_rp * dsc_dt1 + 2.0_rp * dsc_dt2 + dsc_dt3)/6.0_rp
    case default
        write(fIdx%logfile,*)
        write(fIdx%logfile,*) "ERROR --- Not recognised time stepping order: ", gVar%ordT
        stop
    end select

    ! update unknown values
    nodeList%u(:,n1:n2) = nodeList%u(:,n1:n2) + nodeList%du(:,n1:n2)
    ! update scalar values
    if (gVar%nadv .ne. 0) nodeList%scal(:,n1:n2) = nodeList%scal(:,n1:n2) + nodeList%dscal(:,n1:n2)

end subroutine doIter
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Computations to be performed each time step to advance the simulation to the next one.
!   1st order FW Euler or different orders of RK are used to advance the values of the variables
!   to the next step.
!
!   System to solve (unknowns):    {dU_dt} + {dF_dx} = {B}
!       {th}·[NN]·{dU_dt} + {th}·[NdN]·{F} = {th}·[NN]·{B}
subroutine computeDu_Dt(is, u, du_dt)

    integer(ip),                                    intent(in)  :: is
    real(rp),     dimension(:,:),                   intent(in)  :: u
    real(rp),     dimension(gVar%dof, dom(is)%nod), intent(out) :: du_dt
    real(rp),     dimension(gVar%dof, dom(is)%nod)              :: conv, stab, uSrc, F
    real(rp),     dimension(gVar%dof, gVar%dof, dom(is)%nod)    :: H

    ! Unknowns: {B} == uSrc     {F} --> [NdN]·{F} = conv      {H} --> stab
    call getVectors(is, u, uSrc, F, H)

    ! Int{th(x) · dF(x)} = [th(x) · F(x)]_{bound} - Int{dth(x) · F(x)} ~ - {th} · [dNN] · {F}
    call multEleMatVec(segList%elMat(is)%NdN, F, conv)      !   [dNN] · {F} = conv

    ! Get diffusive term (Unknowns: Stabilization added term)
    call getDiff(is, uSrc, F, H, stab)

    ! {lumpedNN} ~ [NN]
    du_dt = ( - conv - stab ) / dom(is)%lumpedNN + uSrc

    ! G-S solve
    !call gsSolver()

contains
! Solve a system [NN] · {du} = {B} by LU decomposion in a Gauss - Seidel scheme
subroutine gsSolver()
    real(rp), dimension(gVar%dof, dom(is)%nod), target :: du_dt0, du_dt1
    real(rp), dimension(gVar%dof, dom(is)%nod)         :: invD, B, LUdu
    real(rp), dimension(gVar%dof)                      :: maxim
    real(rp), dimension(gVar%n_el, gVar%n_el)          :: LU
    integer(ip)                                        :: i, j, iter
    real(rp)                                           :: er
    real(rp), dimension(:,:), pointer                  :: duPrev, duNew
    logical                                            :: swap

    ! Get {B} = [NN] · {uSrc} - {conv} - {stab}
    call multEleMatVec(segList%elMat(is)%NN, uSrc, B)
    B = B - (conv + stab)
    ! Decompose [NN] matrix in [LU] and [D] == {invD}
    LU = segList%elMat(is)%NN
    do i = 1, gVar%n_el
        LU(i,i) = 0.0_rp
    end do
    invD = 0.0_rp
    do i = 1, dom(is)%nod - 1, gVar%n_el - 1
        do j = 1, gVar%n_el
            invD(:,i-1+j) = invD(:,i-1+j) + segList%elMat(is)%NN(j,j)
        end do
    end do

    ! Diagonal matrix invD is inverted component by component to solve system iteratively
    !   {du}^n = {invD} · ({B} - [LU] · {du}^(n-1))
    invD = 1.0_rp / invD

    er = 1.0_rp
    du_dt0 = 0.0_rp
    swap = .false.
    iter = 0
    do while (er .gt. 1.0e-2)
        if (iter .eq. 1000) then
            write(fIdx%logfile,*) "GS not converging"
            stop
        end if
        select case(swap)
        case (.false.)
            duPrev => du_dt0
            duNew  => du_dt1
        case (.true.)
            duPrev => du_dt1
            duNew  => du_dt0
        end select
        call multEleMatVec(LU, duPrev, LUdu)
        duNew = invD * (B - LUdu)
        maxim(:) = maxval( abs(duNew), dim=2 )
        er = maxval( maxval( abs( duNew - duPrev ), dim=2) / maxim )
        swap = .not. swap
        iter = iter + 1
    end do
    du_dt = duNew
end subroutine gsSolver

end subroutine computeDu_Dt
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get Vectors used to compute src (= B), conv (= dF/dx) and diff terms
!   B   = {  0  ,  -kr . Q/A - dBeta/dx . A/(Ad . rho) . (2/3 A^0.5 - Ad^0.5)
!                + Beta . dA/dx . A/(rho . Ad^2) . (2/3 A^0.5 - 0.5 Ad^0.5)  }
!   F   = {  Q  ,  alpha . Q^2/A + beta/(3 . rho . Ad) . A^{3/2}  }
!   H   = [dF/du] = {                     0                               ,        1           }
!                   {  -alpha · (Q/A)^2 + beta/(2 · rho · Ad) · A^{1/2}   ,   2 · alpha · Q/A  }
subroutine getVectors(is, u, B, F, H)

    integer(ip),  intent(in)                          :: is
    real(rp),     dimension(:,:),     intent(in)      :: u
    real(rp),     dimension(:,:),     intent(out)     :: B, F
    real(rp),     dimension(:,:,:),   intent(out)     :: H
    integer(ip)                                       :: i, j, n1, n2

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

    j = 1           ! Local nodal indexing counter

    do i = n1, n2   ! Global indices
        ! B vector
        B(1,j) = 0.0_rp
        B(2,j) = -FlPar%kr/FlPar%rho * u(2,j) / u(1,j) &
               & - nodeList%dB_dx(i) * u(1,j) / (FlPar%rho * nodeList%dyArea(i)) * &
               &   (2.0_rp / 3.0_rp * sqrt(u(1,j)) - sqrt(nodeList%dyArea(i))) &
               & + nodeList%beta(i) * nodeList%dA_dx(i) * u(1,j) / &
               &   (FlPar%rho * nodeList%dyArea(i)**2.0_rp) * (2.0_rp/3.0_rp * &
               &   sqrt(u(1,j)) - 0.5_rp * sqrt(nodeList%dyArea(i)))
        ! F vector
        F(1,j) = u(2,j)
        F(2,j) = FlPar%alpha * u(2,j)**2 / u(1,j) &
               & + nodeList%beta(i) / (3.0_rp * FlPar%rho * nodeList%dyArea(i)) * &
               &   u(1,j)**1.5_rp
        ! Matrix components
        H(1,1,j) = 0.0_rp
        H(2,1,j) = -FlPar%alpha * (u(2,j) / u(1,j))**2.0_rp + &
                 & nodeList%beta(i) / (2.0_rp * FlPar%rho * nodeList%dyArea(i)) * &
                 & u(1,j)**0.5_rp
        H(1,2,j) = 1.0_rp
        H(2,2,j) = 2.0_rp * FlPar%alpha * u(2,j) / u(1,j)
        ! Update local indexing counter
        j = j + 1
    end do

end subroutine getVectors
!----------------------------------------------------------------------------------------------



!----------------------------------------------------------------------------------------------
!
!   Get diffusive (stabilization) term for the system to solve
!   R = [tau] · ( [H] x ( [dNN] · ({Ut} - {B}) + [dNdN] · {F} ) )
subroutine getDiff(is, B, F, H, R)

    integer(ip),  intent(in)                          :: is
    real(rp),     dimension(:,:),   intent(in)        :: B, F
    real(rp),     dimension(:,:,:), intent(in)        :: H
    real(rp),     dimension(gVar%dof, dom(is)%nod)    :: tmp1, tmp2, tmp3
    real(rp),     dimension(:,:),   intent(out)       :: R
    real(rp),     dimension(dom(is)%nod)              :: tau
    integer(ip)                                       :: i, n1, n2

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

    call getTau(is, tau)

    ! tmp1 = [dNdN] · {F}
    ! tmp2 = [dNN] · {Ut}
    ! tmp3 = [dNN] · {B}
    call multEleMatVec(segList%elMat(is)%dNdN, F, tmp1)
    call multEleMatVec(segList%elMat(is)%dNN, nodeList%du(:,n1:n2), tmp2)
    call multEleMatVec(segList%elMat(is)%dNN, B, tmp3)

!    ! tmp1 = [dNdN] · {F}
!    call multEleMatVec(segList%elMat(is)%dNdN, F, tmp1)
!
!    ! tmp2 = [dNN] · ({Ut} - {B})
!    call multEleMatVec(segList%elMat(is)%dNN, (nodeList%du(:,n1:n2) - B), tmp2)

    ! Use tau stabilization only for the Q variable, not for A (over dissipation)
    !   -> Set first component of first array to 0
    do i = 1, dom(is)%nod
        R(:,i) = [ 0.0_rp , tau(i) * ( H(2,1,i)*(tmp1(1,i)+tmp2(1,i)-tmp3(1,i)) &
                                   & + H(2,2,i)*(tmp1(2,i)+tmp2(2,i)-tmp3(2,i)) ) ]
    end do

!    ! Use tau stabilization only for the Q variable, not for A (over dissipation)
!    !   -> Set first component of first array to 0
!    do i = 1, dom(is)%nod
!        R(:,i) = [ 0.0_rp , &
!                 & tau(i) * ( H(2,1,i)*(tmp1(1,i)+tmp2(1,i)) + H(2,2,i)*(tmp1(2,i)+tmp2(2,i)) ) ]
!        R(:,i) = tau(i) * &
!             & [ H(1,1,i)*(tmp1(1,i)+tmp2(1,i)) + H(1,2,i)*(tmp1(2,i)+tmp2(2,i)), &
!             &   H(2,1,i)*(tmp1(1,i)+tmp2(1,i)) + H(2,2,i)*(tmp1(2,i)+tmp2(2,i)) ]
!    end do

end subroutine getDiff
!----------------------------------------------------------------------------------------------


!----------------------------------------------------------------------------------------------
!
!   Get Tau value for each node in segment [is]
subroutine getTau(is, tau)

    integer(ip),      intent(in)                      :: is
    real(rp),         dimension(:),   intent(out)     :: tau
    integer(ip)                                       :: i, j, n1, n2
    real(rp)                                          :: lamb1, lamb2, lamb, invtau

    n1 = dom(is)%nLim(1)        ! Get limiting nodes index
    n2 = dom(is)%nLim(2)

    tau(:) = 0.0_rp
    j = 1
    do i = n1, n2
        lamb1 = eigenvals(nodeList%u(1,i), nodeList%u(2,i), nodeList%beta(i), &
              & nodeList%dyArea(i), 1)
        lamb2 = eigenvals(nodeList%u(1,i), nodeList%u(2,i), nodeList%beta(i), &
              & nodeList%dyArea(i), 2)
        lamb = maxval( abs( [lamb1, lamb2] ) )
        invtau = 2.0_rp * lamb / segList%dx(is) + FlPar%kr / nodeList%u(1,i)
        if (invtau .gt. 1.0e-10) then
            tau(j) = gVar%asupg / invtau
        end if
        j = j + 1
    end do

end subroutine getTau
!----------------------------------------------------------------------------------------------

end module galtub
