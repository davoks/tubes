# Tubes

Authors:
* David Oks
* Albert Pérez Madrigal

## Description:

A **Fortran 1D finite element code** for studying wave-propagation in deformable and tubular-like network topologies such as the **blood flow in arteries** or **airflow in the bronchial tree**.

## Compile:

To compile with `ifort` set `makefile` flags to:
```
FMOD = -module
OMP = -qopenmp
CFLAGS = -c -g $(OMP) -O3
```

To compile with `gfortran` set `makefile` flags to:
```
FMOD = -J
OMP = -fopenmp
CFLAGS = -c -g $(OMP) -O3
```

## Set-up case:

2 input files must be given for a problem:
* `<casename>.conf.txt`: Configuration file.
* `<casename>.geom.txt`: Geometry file.

## Run case:

`$ ./tubes <casename>`

## Post-process:

2 types of output files are given in the output folder set in `*.conf.in`:
* `<casename>.seg$$$step$$$$$.txt`
* `<casename>.witpoint.$$$.txt`
