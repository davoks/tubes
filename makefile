#Source files
SRCF = \
boundcon.f90 \
tubes_files.f90 \
clases.f90 \
femodule.f90 \
galtub.f90 \
checks.f90 \
inicon.f90 \
io.f90 \
linalg.f90 \
main.f90 \
mesher.f90 \
nrtype.f90 \
physfuncs.f90 \
scgaltub.f90 \
winkes.f90 \
initialize_tubes.f90 \
advance_timestep_tubes.f90 \
get_timestep_tubes.f90

#Object files
OBJF = $(SRCF:.f90=.o)

#Directories
SRC_DIR = src/
OBJ_DIR = obj/
OBJECTS = $(addprefix $(OBJ_DIR), $(OBJF))

PROGRAM = libtubes1d.a
EXE = tubes

##########################################################################################
# COMPILER OPTIONS
##########################################################################################

OPT   = -O3
TYPES = -DI4 -DR8

#ifort
FMOD = -module 
OMP = -qopenmp
CFLAGS = -c -g $(OMP) $(TYPES) $(OPT) -std08 -cpp

#gfortran
#FMOD = -J 
#OMP = -fopenmp
#CFLAGS = -c -g $(OMP) $(TYPES) $(OPT) -std=f2008 -cpp

##########################################################################################

#Compiler 
LDFLAGS = $(OMP)
FC = mpif90
LD = mpif90

#Virtual PATH
VPATH = $(SRC_DIR):$(OBJ_DIR)

all: $(EXE) $(PROGRAM)

$(EXE) : $(OBJF)
	$(LD) -o $@ $(LDFLAGS) $(OBJECTS)

$(PROGRAM) : $(OBJF)
	ar cr $@ $(OBJECTS)

$(OBJF) :
	$(FC) $(CFLAGS) $(SRC_DIR)$(@:.o=.f90) -o $(OBJ_DIR)$@ $(FMOD) $(OBJ_DIR)

clean:
	rm -r $(OBJ_DIR)*.o $(OBJ_DIR)*.mod $(EXE) *.a *.exe

#Dependencies
main.o		:	main.f90 femodule.o galtub.o inicon.o io.o mesher.o
tubes_files.o	:	tubes_files.f90 clases.o
boundcon.o	:	boundcon.f90 clases.o nrtype.o femodule.o linalg.o physfuncs.o winkes.o
clases.o	:	clases.f90 nrtype.o
femodule.o	:	femodule.f90 clases.o nrtype.o physfuncs.o
galtub.o	:	galtub.f90 clases.o boundcon.o femodule.o io.o nrtype.o physfuncs.o scgaltub.o
checks.o	:	checks.f90 clases.o nrtype.o
inicon.o	:	inicon.f90 clases.o femodule.o nrtype.o physfuncs.o
io.o		:	io.f90 nrtype.o clases.o femodule.o
linalg.o	:	linalg.f90 nrtype.o
mesher.o	:	mesher.f90 io.o nrtype.o physfuncs.o
nrtype.o	:	nrtype.f90
physfuncs.o	:	physfuncs.f90 clases.o nrtype.o
scgaltub.o      :	scgaltub.f90 clases.o nrtype.o femodule.o
winkes.o	:	winkes.f90 clases.o nrtype.o
initialize_tubes.o : initialize_tubes.f90 nrtype.o clases.o mesher.o inicon.o femodule.o galtub.o io.o
advance_timestep_tubes.o : advance_timestep_tubes.f90 nrtype.o clases.o galtub.o io.o
get_timestep_tubes.o : get_timestep_tubes.f90 nrtype.o
